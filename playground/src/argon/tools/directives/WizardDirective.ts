import { ContentChildren, Directive, QueryList } from '@angular/core';
import { WizardStepComponent } from '../components/WizardStep/WizardStepComponent';
import { Wizard } from '../components/Wizard/Wizard';
import { FocusableOption } from '@angular/cdk/a11y';
import { WizardHeaderComponent } from '../components/WizardHeader/WizardHeaderComponent';

@Directive({
  selector: '[arWizard]'
})
export class WizardDirective extends Wizard {

  @ContentChildren(WizardStepComponent) _steps: QueryList<WizardStepComponent>;

  @ContentChildren(WizardHeaderComponent) _stepHeader: QueryList<WizardStepComponent>;

}
