import { Component, ElementRef, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'ar-wizard-header',
  templateUrl: 'WizardHeaderComponent.html',
  styleUrls: ['../Wizard/WizardComponent.scss']
})
export class WizardHeaderComponent {

  @Input() state: 'number' | 'edit' | 'done';

  @Input() active: boolean;

  @HostBinding('class.step-header-component') className = true;

  public get icon(): string {
    if (String(this.state) === 'edit') {
      return 'checkbox-marked-circle-outline';
    }

    return this.active
      ? 'checkbox-blank-circle'
      : 'checkbox-blank-circle-outline';
  }

  constructor(
    private element: ElementRef
  ) { }

  public focus() {
    this.element.nativeElement.focus();
  }
}
