import { Pipe, PipeTransform } from '@angular/core';
import { trim, trimStart, trimEnd } from 'lodash';

@Pipe({
  name: 'arTrim'
})
export class TrimPipe implements PipeTransform {

  static MODE_START = 'start';

  static MODE_END = 'end';

  transform(value: any, chars?: string, mode?: string): any {
    switch (mode) {
      case TrimPipe.MODE_START:
        return trimStart(String(value), chars);
      case TrimPipe.MODE_END:
        return trimEnd(String(value), chars);
      default:
        return trim(String(value), chars);
    }
  }
}
