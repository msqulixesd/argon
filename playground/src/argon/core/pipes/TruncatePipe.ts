import { Pipe, PipeTransform } from '@angular/core';
import { truncate } from 'lodash';

@Pipe({
  name: 'arTruncate'
})
export class TruncatePipe implements PipeTransform {

  transform(value: any, length?: number): any {
    if (!length) {
      return value;
    }

    return truncate(String(value), {
      separator: ' ',
      length
    });
  }
}
