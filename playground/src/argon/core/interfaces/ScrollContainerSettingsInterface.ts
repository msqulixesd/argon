import { ElementRef, TemplateRef, ViewContainerRef } from '@angular/core';

export interface ScrollContainerSettingsInterface {

  /** Count elements to display */
  viewElementCount?: number;

  /** Count elements to render on scroll */
  offsetSize?: number;

  /** Container to render collection */
  itemsContainer: ViewContainerRef;

  /** Template for render single item */
  template: TemplateRef<any>;

  /** Root element (which will be trigger scroll event)*/
  containerElement: ElementRef;

  /** Collection for rendering after initialize*/
  initialCollection?: Array<any>;

}
