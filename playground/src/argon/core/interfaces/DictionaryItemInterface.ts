
export interface DictionaryItemInterface {

  label?: string;

  value: any;

  disabled?: boolean;

  extendedData?: any;

}
