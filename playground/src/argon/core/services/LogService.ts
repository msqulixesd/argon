import { Injectable, isDevMode } from '@angular/core';

@Injectable()
export class LogService {

  log(...args) {
    if (isDevMode()) {
      console.log.apply(null, args);
    }
  }

  warn(...args) {
    if (isDevMode()) {
      console.warn.apply(null, args);
    }
  }

}
