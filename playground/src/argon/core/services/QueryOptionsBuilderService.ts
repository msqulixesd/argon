import { Injectable } from '@angular/core';
import { HttpParamsOptions } from '@angular/common/http/src/params';
import { HttpParams } from '@angular/common/http';
import { parse, stringify, IParseOptions, IStringifyOptions } from 'qs';

@Injectable()
export class QueryOptionsBuilderService {

  public createHttpParams<T>(queryParams?: T, options?: IStringifyOptions): HttpParams {
    return new HttpParams({
      fromString: this.stringify(queryParams, options)
    } as HttpParamsOptions);
  }

  public parse<T>(source: string, options?: IParseOptions): T {
    return parse(source, options);
  }

  public stringify<T>(object: T, options?: IStringifyOptions): string {
    return stringify(object, options);
  }

}
