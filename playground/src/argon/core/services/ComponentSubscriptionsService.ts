import { get, set, has, each } from 'lodash';
import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs';

@Injectable()
export class ComponentSubscriptionsService {

  private subscriptions = {};

  public add(key: string, subscription: Subscription) {
    this.remove(key);
    set(this.subscriptions, key, subscription);
    return this;
  }

  public remove(key: string) {
    const item = this.get(key);
    if (item) {
      item.unsubscribe();
    }
    this.unset(key);
    return this;
  }

  public get(key: string): Subscription {
    return get(this.subscriptions, key);
  }

  public unset(key: string) {
    if (has(this.subscriptions, key)) {
      this.subscriptions[key] = null;
    }
    return this;
  }

  public unsubscribeAll() {
    each(this.subscriptions, (i: Subscription, key: string) => { this.remove(key); });
    return this;
  }
}
