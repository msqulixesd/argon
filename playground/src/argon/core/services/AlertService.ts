import { Injectable } from '@angular/core';
import { MessageBusService } from './MessageBusService';
import { BsContextEnum } from '../../ui/types/BsContextEnumType';
import { AlertPropsInterface } from '../interfaces/AlertPropsInterface';
import { DisplayAlertMessage } from '../messages/DisplayAlertMessage';

@Injectable()
export class AlertService {

  static TYPES = BsContextEnum;

  constructor(
    private messageService: MessageBusService
  ) { }

  public showAlert(props: AlertPropsInterface) {
    this.messageService.send(this.createMessage(props));
  }

  private createMessage(props: AlertPropsInterface): DisplayAlertMessage {
    const message = new DisplayAlertMessage(props.message, props.bsContext, props.closeable, props.displayTime);
    if (props.template) {
      message.setTemplate(props.template, props.templateContext);
    }
    return message;
  }

}
