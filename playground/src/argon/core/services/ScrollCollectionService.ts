import { ElementRef, Injectable, NgZone, TemplateRef, ViewContainerRef } from '@angular/core';
import { map, size, each, slice, reverse, concat} from 'lodash';
import { ScrollItemInterface } from '../interfaces/ScrollItemInterface';
import { ScrollContainerSettingsInterface } from '../interfaces/ScrollContainerSettingsInterface';

@Injectable()
export class ScrollCollectionService {

  // <editor-fold desc="[Options]">
  static DEFAULT_VIEW_ELEMENT_COUNT = 20;

  static DEFAULT_OFFSET_SIZE = 5;

  public get viewElementCount(): number {
    return this.options.viewElementCount || ScrollCollectionService.DEFAULT_VIEW_ELEMENT_COUNT;
  }

  public get offsetSize(): number {
    return this.options.offsetSize || ScrollCollectionService.DEFAULT_OFFSET_SIZE;
  }

  public get itemsContainer(): ViewContainerRef {
    return this.options.itemsContainer;
  }

  public get template(): TemplateRef<any> {
    return this.options.template;
  }

  public get containerElement(): ElementRef {
    return this.options.containerElement;
  }

  // </editor-fold desc="[Options]">

  public set collection(val: Array<any>) {
    this.destroyCollection(this._collection);
    this._collection = this.createCollection(val);
    if (this.initialized) {
      this.itemsContainer.clear();
      this._middleIndex = this.minIndex;
      const collectionToRender = slice(this._collection, 0, this.viewElementCount);
      this.renderCollection(collectionToRender);
    }
  }

  public get collection(): Array<any> {
    return map(this._collection, (item: ScrollItemInterface) => item.data);
  }

  public set scrollRatio(val: number) {
    if (val > 0.99) {
      this.ngZone.run(this.renderToBottom);
    } else if (val < 0.01) {
      this.ngZone.run(this.renderToTop);
    }
  }

  public get minIndex(): number {
    return Math.ceil(this.viewElementCount / 2);
  }

  public get maxIndex(): number {
    return this.size - this.minIndex;
  }

  public get size(): number {
    return size(this._collection);
  }

  public get hasItems(): boolean {
    return this.size > 0;
  }

  public get initialized(): boolean {
    return Boolean(this.options);
  }

  public get topScrollSize(): number {
    const nativeElement = this.containerElement.nativeElement;
    const scrollHeight = nativeElement.scrollHeight - nativeElement.clientHeight;
    return scrollHeight * (this.offsetSize / this.viewElementCount);
  }

  private _collection: Array<ScrollItemInterface> = [];

  private _middleIndex: number;

  private options: ScrollContainerSettingsInterface;

  constructor(
    private ngZone: NgZone
  ) { }

  public initialize(options: ScrollContainerSettingsInterface) {
    this.options = options;
    this._middleIndex = this.minIndex;
    this.collection = this.options.initialCollection || this.collection;
    this.options.initialCollection = [];
  }

  public destroy() {
    this.destroyCollection(this._collection);
  }

  private renderToTop = () => {
    if (this._middleIndex <= this.minIndex || size(this._collection) === 0) {
      return;
    }

    const startIndex = this._middleIndex - this.minIndex - this.offsetSize;

    const itemsToAdd = this._collection.slice(startIndex < 0 ? 0 : startIndex, this._middleIndex - this.minIndex);
    const renderedItems = this._collection.slice(this._middleIndex - this.minIndex, this._middleIndex + this.minIndex);

    const collectionToChange = concat(itemsToAdd, renderedItems);
    const collectionSize = size(collectionToChange);
    for (let i = collectionSize - 1; i >= this.viewElementCount; i--) {
      collectionToChange[i].shouldDetach = true;
    }
    this._middleIndex -= this.offsetSize;
    if (this._middleIndex < this.minIndex) {
      this._middleIndex = this.minIndex;
    }

    this.renderCollection(collectionToChange, true);
    this.containerElement.nativeElement.scrollTop = this.topScrollSize;
  }

  private renderToBottom = () => {
    if (this._middleIndex >= this.maxIndex || size(this._collection) === 0) {
      return;
    }
    const itemsToAdd = this._collection.slice(this._middleIndex + this.minIndex, this._middleIndex + this.minIndex + this.offsetSize);
    const renderedItems = this._collection.slice(this._middleIndex - this.minIndex, this._middleIndex + this.minIndex);
    const collectionToChange = concat(renderedItems, itemsToAdd);
    const collectionSize = size(collectionToChange);
    for (let i = 0; i < collectionSize - this.viewElementCount; i++) {
      collectionToChange[i].shouldDetach = true;
    }

    this._middleIndex += this.offsetSize;
    if (this._middleIndex > this.maxIndex) {
      this._middleIndex = this.maxIndex;
    }

    this.renderCollection(collectionToChange);
  }

  private renderCollection(collection: Array<ScrollItemInterface>, reverseRender: boolean = false) {
    const collectionToRender = reverseRender ? reverse(collection) : collection;

    each(collectionToRender, (item: ScrollItemInterface) => {
      const index = item.viewRef ? this.itemsContainer.indexOf(item.viewRef) : -1;
      const self = this;

      if (item.shouldDetach && index >= 0) {
        this.removeElement(item, index);
      } else if (index < 0) {
        this.renderElement(item, reverseRender ? 0 : null);
      }
    });
  }

  private renderElement(item: ScrollItemInterface, index?: number) {
    if (item.viewRef) {
      this.itemsContainer.insert(item.viewRef, index);
    } else {
      item.viewRef = this.itemsContainer.createEmbeddedView(
        this.template, { $implicit: item.data }, index
      );
    }
  }

  private removeElement(item: ScrollItemInterface, index: number) {
    item.shouldDetach = false;
    this.itemsContainer.detach(index);
  }

  private destroyCollection(collection: Array<ScrollItemInterface>) {
    each(collection, (item: ScrollItemInterface) => {
      if (item.viewRef && !item.viewRef.destroyed) {
        item.viewRef.destroy();
      }
    });
  }

  private createCollection(options: Array<any>): Array<ScrollItemInterface> {
    return map(options, (data: any) => ({ data }));
  }

}
