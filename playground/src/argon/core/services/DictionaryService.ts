import { Injectable } from '@angular/core';
import { isArray } from 'lodash';
import { DictionaryItemInterface } from '../interfaces/DictionaryItemInterface';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { LogService } from './LogService';
import { shareReplay, mergeMap } from 'rxjs/operators';

export type DictionaryType = Array<DictionaryItemInterface>;
export type ObservableDictionaryType = Observable<Array<DictionaryItemInterface>>;
export type DictionaryInputType = DictionaryType | ObservableDictionaryType;

@Injectable()
export class DictionaryService {

  public dictionaries = new Map<string, BehaviorSubject<ObservableDictionaryType>>();

  constructor(
    protected logService: LogService
  ) { }

  public add(key: string, dictionary: DictionaryInputType) {
    const hasDictionary = this.dictionaries.has(key);
    const source = isArray(dictionary)
      ? of(<DictionaryType>dictionary).pipe(shareReplay(1))
      : (<ObservableDictionaryType>dictionary).pipe(shareReplay(1));

    if (hasDictionary) {
      const dictionarySubject = this.dictionaries.get(key);
      dictionarySubject.next(source);
    } else {
      const dictionarySubject = new BehaviorSubject<ObservableDictionaryType>(source);
      this.dictionaries.set(key, dictionarySubject);
    }
  }

  public get(key: string): ObservableDictionaryType {
    if (!this.dictionaries.has(key)) {
      this.logService.warn(`DictionaryService: Dictionary ${key} does not exist. Return raw dictionary`);
      this.add(key, []);
    }

    return this.dictionaries.get(key).asObservable()
      .pipe(mergeMap((data: ObservableDictionaryType) => data));
  }

  public remove(key: string) {
    if (!this.dictionaries.has(key)) {
      this.logService.warn(`DictionaryService: Dictionary ${key} does not exist. Nothing removed.`);
    } else {
      this.dictionaries.get(key).next(of([]).pipe(shareReplay(1)));
    }
  }
}
