import { InfinityScrollDirective } from './InfinityScrollDirective';
import { Directive, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { ScrollCollectionService } from '../services/ScrollCollectionService';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[arScrollContainer]',
  providers: [ScrollCollectionService]
})
export class ScrollContainerDirective implements OnInit, OnDestroy {

  @Input() set arScrollContainer(val: Array<any>) {
    this.scrollService.collection = val;
  }

  protected scrollSubscription: Subscription;

  constructor(
    protected container: ViewContainerRef,
    protected template: TemplateRef<any>,
    protected scrollService: ScrollCollectionService,
    protected scrollDirective: InfinityScrollDirective
  ) { }

  ngOnInit(): void {
    if (!this.scrollDirective) {
      throw Error('InfinityScrollDirective in parent node required!');
    }

    this.scrollService.initialize({
      template: this.template,
      itemsContainer: this.container,
      containerElement: this.scrollDirective.element,
      viewElementCount: 50,
      offsetSize: 10
    });

    this.scrollSubscription = this.scrollDirective.scrollEvent.subscribe((ratio: number) => {
      this.scrollService.scrollRatio = ratio;
    });
  }

  ngOnDestroy(): void {
    this.scrollService.destroy();
    if (this.scrollSubscription) {
      this.scrollSubscription.unsubscribe();
    }
  }
}
