import { AfterViewInit, Directive, ElementRef, EventEmitter, NgZone, Output } from '@angular/core';
import { fromEvent, Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Directive({
  selector: '[arInfinityScroll]'
})
export class InfinityScrollDirective implements AfterViewInit {

  @Output() infinityScroll = new EventEmitter<number>();

  public scrollEvent = new Subject<number>();

  constructor(
    public element: ElementRef,
    protected zone: NgZone
  ) { }

  ngAfterViewInit() {
    this.zone.runOutsideAngular(() => {
      fromEvent(this.element.nativeElement, 'scroll')
        .pipe(debounceTime(50))
        .subscribe((event: any) => {
          const nativeElement = this.element.nativeElement;
          const scrollHeight = nativeElement.scrollHeight - nativeElement.clientHeight;
          const scrollTop = nativeElement.scrollTop;
          const scrollRatio = scrollTop / scrollHeight;
          this.infinityScroll.emit(scrollRatio);
          this.scrollEvent.next(scrollRatio);
        });
    });
  }

}
