import { LogService } from '../services/LogService';
import { DictionaryService } from '../services/DictionaryService';

let dictionaryService: DictionaryService;

export const dictionaryServiceFactory = (logService: LogService) => {
  if (!dictionaryService) {
    dictionaryService = new DictionaryService(logService);
  }

  return dictionaryService;
};
