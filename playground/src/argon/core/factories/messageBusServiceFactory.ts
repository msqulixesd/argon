import { MessageBusService } from '../services/MessageBusService';
import { LogService } from '../services/LogService';

let messageBusService: MessageBusService;

export const messageBusServiceFactory = (logService: LogService) => {
  if (!messageBusService) {
    messageBusService = new MessageBusService(logService);
  }

  return messageBusService;
};
