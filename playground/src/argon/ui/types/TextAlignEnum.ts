import { values } from 'lodash';

const RIGHT = 'right';

const LEFT = 'left';

const CENTER = 'center';

const SM_LEFT = 'sm-left';

const SM_RIGHT = 'sm-right';

const SM_CENTER = 'sm-center';

const MD_LEFT = 'md-left';

const MD_RIGHT = 'md-right';

const MD_CENTER = 'md-center';

const LG_LEFT = 'lg-left';

const LG_RIGHT = 'lg-right';

const LG_CENTER = 'lg-center';

const XL_LEFT = 'xl-left';

const XL_RIGHT = 'xl-right';

const XL_CENTER = 'xl-center';

export const TextAlignEnum = {
  RIGHT, LEFT, CENTER,
  SM_LEFT, SM_RIGHT, SM_CENTER,
  MD_LEFT, MD_RIGHT, MD_CENTER,
  LG_LEFT, LG_RIGHT, LG_CENTER,
  XL_LEFT, XL_RIGHT, XL_CENTER,
};

export type TextAlignEnumType = keyof typeof TextAlignEnum;

export const TextAlignArray = values(TextAlignEnum);
