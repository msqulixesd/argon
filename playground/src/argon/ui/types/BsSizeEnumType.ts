import { values } from 'lodash';

const XL = 'xl';

const LG = 'lg';

const MD = 'md';

const SM = 'sm';

const XS = 'xs';

const DEFAULT = '';

export const BsSizeEnum = { XL, LG, MD, XS, SM, DEFAULT };

export type BsSizeType = keyof typeof BsSizeEnum;

export const BsSizeArray = values(BsSizeEnum);
