/**
 * Modal dialog configuration interface
 */
export interface ModalDialogConfigInterface {

  // (setter) add modal-lg class
  large: boolean;

  // (setter) add modal-sm class
  small: boolean;

  // (setter) add custom class name
  customClass: string;

  // should modal dialog close on backdrop click (true by default)
  backdropClickClose: boolean;

  centered: boolean;

}
