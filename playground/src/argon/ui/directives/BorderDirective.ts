import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[arBorder]'
})
export class BorderDirective implements OnInit {

  static BORDER_PREFIX = 'border';

  static ROUNDED_PREFIX = 'rounded';

  private currentContext: string;

  private currentContextClass: string;

  private rounded: boolean;

  @Input() set arBorder(context: string) {
    if (this.currentContext !== context) {
      this.decorate(context);
    }
  }

  @Input() set arBorderRounded(isRounded: boolean) {
    if (this.rounded === isRounded) {
      return;
    }

    this.setRounded(isRounded);
  }

  constructor(
    protected renderer2: Renderer2,
    protected elementRef: ElementRef,
  ) { }

  ngOnInit(): void {
    this.renderer2.addClass(this.elementRef.nativeElement, BorderDirective.BORDER_PREFIX);
  }

  protected decorate(context: string) {
    this.renderer2.removeClass(this.elementRef.nativeElement, this.currentContextClass);
    this.currentContext = context;
    this.currentContextClass = context ? `${BorderDirective.BORDER_PREFIX}-${this.currentContext}` : '';

    if (this.currentContextClass) {
      this.renderer2.addClass(this.elementRef.nativeElement, this.currentContextClass);
    }
  }

  protected setRounded(isRounded: boolean) {
    if (isRounded) {
      this.renderer2.addClass(this.elementRef.nativeElement, BorderDirective.ROUNDED_PREFIX);
    } else {
      this.renderer2.removeClass(this.elementRef.nativeElement, BorderDirective.ROUNDED_PREFIX);
    }

    this.rounded = isRounded;
  }
}
