import { Directive, ElementRef, Input, Optional, Renderer2, SkipSelf } from '@angular/core';
import { BsSizeType } from '../types/BsSizeEnumType';
import { ClassNameDirective } from './ClassNameDirective';
import { BsGroupComponent } from '../components/BsGroup/BsGroupComponent';

@Directive({
  selector: '[arBsSize]'
})
export class BsSizeDirective extends ClassNameDirective<BsSizeType, string> {

  @Input() set arBsSizePrefix(prefix: string) {
    this.prefix = prefix || '';
  }
  get arBsSizePrefix(): string {
    return this.prefix;
  }

  @Input() set arBsSize(context: BsSizeType) {
    let nextContext = context;

    if (!nextContext && this.bsSizeGroup) {
      nextContext = this.bsSizeGroup.bsSize as any;
      this.unsubscribeFromParent();
    } else if (!nextContext && this.parentDirective) {
      nextContext = this.parentDirective.arBsSize
        ? this.parentDirective.arBsSize
        : '' as BsSizeType;
      this.subscribeToParent();
    } else {
      this.unsubscribeFromParent();
    }

    this.decorate(nextContext);
  }
  get arBsSize(): BsSizeType {
    return this.originalContext;
  }

  protected prefix = '';

  constructor(
    renderer2: Renderer2,
    elementRef: ElementRef,
    @Optional() protected bsSizeGroup: BsGroupComponent,
    @Optional() @SkipSelf() protected parentDirective: BsSizeDirective
  ) {
    super(renderer2, elementRef, parentDirective);
  }

  protected createContextClassName(originalClass: string, contextValue: string) {
    if (!originalClass) {
      return '';
    }

    return this.prefix
      ? `${originalClass}-${this.prefix}-${contextValue}`
      : `${originalClass}-${contextValue}`;
  }

  protected createEnumTypeInstance(context: any): string {
    return String(context);
  }

}
