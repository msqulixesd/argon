import { Directive, ElementRef, Input, Optional, Renderer2 } from '@angular/core';
import { BsGroupComponent } from '../components/BsGroup/BsGroupComponent';
import { TextAlignEnumType } from '../types/TextAlignEnum';

@Directive({
  selector: '[arTextAlign]'
})
export class TextAlignDirective {

  protected currentClass: string;
  protected currentValue: string;

  @Input() set arTextAlign(direction: TextAlignEnumType) {
    let nextDirection = direction as string;

    if (!nextDirection && this.bsGroup) {
      nextDirection = this.bsGroup.textAlign;
    }

    if (this.currentValue === nextDirection) {
      return;
    }

    this.decorate(nextDirection);
  }

  constructor(
    protected renderer2: Renderer2,
    protected elementRef: ElementRef,
    @Optional() protected bsGroup: BsGroupComponent,
  ) { }

  protected decorate(nextDirection: string) {
    if (this.currentClass) {
      this.renderer2.removeClass(this.elementRef.nativeElement, this.currentClass);
    }

    this.currentClass = nextDirection ? `text-${nextDirection}` : '';
    this.currentValue = nextDirection;

    if (this.currentClass) {
      this.renderer2.addClass(this.elementRef.nativeElement, this.currentClass);
    }
  }

}
