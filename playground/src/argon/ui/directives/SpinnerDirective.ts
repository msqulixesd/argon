import {
  ComponentFactoryResolver, ComponentRef, Directive, Input, TemplateRef,
  ViewContainerRef
} from '@angular/core';
import { SpinnerComponent } from '../components/Spinner/SpinnerComponent';

@Directive({
  selector: '[arSpinner]'
})
export class SpinnerDirective {

  @Input() set arSpinner(condition: boolean) {
    this.component.instance.state = condition;
  }

  private component: ComponentRef<SpinnerComponent>;

  constructor(
    factoryResolver: ComponentFactoryResolver,
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) {
    const factory = factoryResolver.resolveComponentFactory(SpinnerComponent);
    this.component = factory.create(this.viewContainer.parentInjector);
    this.component.instance.templateRef = this.templateRef;
    this.viewContainer.insert(this.component.hostView);
  }

}
