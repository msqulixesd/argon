import { Directive, ElementRef, Input, Optional, Renderer2 } from '@angular/core';
import { BsContextEnum, BsContextType } from '../types/BsContextEnumType';
import { BsGroupComponent } from '../components/BsGroup/BsGroupComponent';
import { get } from 'lodash';
import { BsTextContextDirective } from './BsTextContextDirective';
import { BsTextContextEnum, BsTextContextType } from '../types/BsTextEnumType';

@Directive({
  selector: '[arBsBgContext]'
})
export class BsBgContextDirective {

  static BG_PREFIX = 'bg';

  static COLOR_MAP = {
    danger: BsTextContextEnum.WHITE,
    dark: BsTextContextEnum.WHITE,
    'default': BsTextContextEnum.DARK,
    light: BsTextContextEnum.DARK,
    info: BsTextContextEnum.WHITE,
    link: BsTextContextEnum.DARK,
    primary: BsTextContextEnum.WHITE,
    secondary: BsTextContextEnum.WHITE,
    warning: BsTextContextEnum.DARK,
    success: BsTextContextEnum.WHITE,
  };

  static getTextColor(bsContext: string, defaultValue = BsTextContextEnum.DARK): string {
    return get(BsBgContextDirective.COLOR_MAP, bsContext, defaultValue);
  }

  private currentContext: BsContextType;

  private currentBgClass: string;

  private currentTextClass: string;

  @Input() set arBsBgContext(context: BsContextType) {
    let nextContext = context;

    if (this.bsContextGroup && !nextContext) {
      nextContext = this.bsContextGroup.bsBgContext as BsContextType;
    }

    if (nextContext !== this.currentContext) {
      this.decorate(nextContext);
    }
  }

  constructor(
    protected renderer2: Renderer2,
    protected elementRef: ElementRef,
    @Optional() protected bsContextGroup: BsGroupComponent,
  ) { }

  protected decorate(nextContext: BsContextType) {
    this.renderer2.removeClass(this.elementRef.nativeElement, this.currentBgClass);
    this.renderer2.removeClass(this.elementRef.nativeElement, this.currentTextClass);
    this.currentContext = nextContext;
    this.currentBgClass = `${BsBgContextDirective.BG_PREFIX}-${this.currentContext}`;
    this.currentTextClass = this.getTextClass(nextContext);
    this.renderer2.addClass(this.elementRef.nativeElement, this.currentBgClass);
    this.renderer2.addClass(this.elementRef.nativeElement, this.currentTextClass);
  }

  protected getTextClass(bgContext: BsContextType): string {
    const context = BsBgContextDirective.getTextColor(bgContext);
    return context
      ? BsTextContextDirective.getClassName(context as BsTextContextType)
      : '';
  }

}
