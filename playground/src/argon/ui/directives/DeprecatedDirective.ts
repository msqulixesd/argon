import { Directive, Input, OnInit } from '@angular/core';
import { LogService } from '../../core/services/LogService';

@Directive({
  // tslint:disable-next-line
  selector: '[bsSize]:not(ar-bs-group), [bsContext]:not(ar-bs-group)'
})
export class DeprecatedDirective implements OnInit {

  @Input() bsSize: any;

  @Input() bsContext: any;

  constructor(
    private logService: LogService
  ) { }

  ngOnInit(): void {
    this.logService.warn('bsContext, bsSize properties were deprecated. Use arBsContext, arBsSize directives instead');
  }
}
