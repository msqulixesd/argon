import { Directive, ElementRef, Input, Optional, Renderer2 } from '@angular/core';
import { BsGroupComponent } from '../components/BsGroup/BsGroupComponent';
import { BsTextContextType } from '../types/BsTextEnumType';

@Directive({
  selector: '[arBsTextContext]'
})
export class BsTextContextDirective {

  static TEXT_PREFIX = 'text';

  static getClassName(context: BsTextContextType): string {
    return `${BsTextContextDirective.TEXT_PREFIX}-${context}`;
  }

  private currentContext: BsTextContextType;

  private currentClass: string;

  @Input() set arBsTextContext(context: BsTextContextType) {
    let nextContext = context;

    if (this.bsContextGroup && !nextContext) {
      nextContext = this.bsContextGroup.bsTextContext as BsTextContextType;
    }

    if (nextContext !== this.currentContext) {
      this.decorate(nextContext);
    }
  }

  constructor(
    protected renderer2: Renderer2,
    protected elementRef: ElementRef,
    @Optional() protected bsContextGroup: BsGroupComponent,
  ) { }

  protected decorate(nextContext: BsTextContextType) {
    this.renderer2.removeClass(this.elementRef.nativeElement, this.currentClass);
    this.currentContext = nextContext;
    this.currentClass = BsTextContextDirective.getClassName(this.currentContext);
    this.renderer2.addClass(this.elementRef.nativeElement, this.currentClass);
  }

}
