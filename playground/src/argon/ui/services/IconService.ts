import { ElementRef, Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { each, keys } from 'lodash';

@Injectable()
export class IconService {

  private renderer: Renderer2;

  private sprite: ElementRef;

  private icons = {};

  constructor (
    private rendererFactory2: RendererFactory2
  ) {
    this.renderer = this.rendererFactory2.createRenderer(null, null);
  }

  public initialize(element: ElementRef) {
    this.sprite = element;
    this.addMany(this.icons);
  }

  public add = (path: string, id: string, viewBox = '0 0 24 24') => {
    if (!this.sprite) {
      this.icons[id] = path;
      return;
    }

    const symbol = this.renderer.createElement('symbol', 'svg');
    this.renderer.setAttribute(symbol, 'id', id);
    this.renderer.setAttribute(symbol, 'viewBox', viewBox);
    const pathElement = this.renderer.createElement('path', 'svg');
    this.renderer.setAttribute(pathElement, 'd', path);
    this.renderer.appendChild(symbol, pathElement);
    this.renderer.appendChild(this.sprite.nativeElement, symbol);
  }

  public addMany(data: { [key: string]: string }) {
    this.icons = {
      ...this.icons,
      ...data,
    };

    if (!this.sprite) {
      return;
    }

    each(data, (path: string, id: string) => { this.add(path, id); });
  }

  public getIconsList(): Array<string> {
    return keys(this.icons);
  }
}
