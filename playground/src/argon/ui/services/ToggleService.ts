import { Injectable } from '@angular/core';
import { animate, AnimationTriggerMetadata, state, style, transition, trigger } from '@angular/animations';

@Injectable()
export class ToggleService {

  static ACTIVE_STATE = 'active';
  static INACTIVE_STATE = 'inactive';

  static ANIMATION_HEIGHT = trigger('toggleServiceState', [
    state('active', style({ height: '*', padding: '*' })),
    state('inactive', style({ height: 0, overflow: 'hidden', padding: 0 })),
    transition('active <=> inactive', animate('100ms ease-out')),
    transition(':enter', [
      style({ height: '*' }),
      animate('1000ms ease-out')
    ]),
    transition(':leave', [
      style({ height: 0, overflow: 'hidden' }),
      animate('1000ms ease-out')
    ])
  ]) as AnimationTriggerMetadata;

  static ANIMATION_OPACITY = trigger('toggleServiceState', [
    state('active', style({ opacity: 1 })),
    state('inactive', style({ opacity: 0 })),
    transition('active <=> inactive', animate('200ms ease-out')),
    transition(':enter', [
      style({ opacity: 0 }),
      animate('200ms ease-in', style({ opacity: 1 }))
    ]),
    transition(':leave', [
      style({ opacity: 1 }),
      animate('200ms ease-out', style({ opacity: 0 }))
    ])
  ]) as AnimationTriggerMetadata;

  public state: string = ToggleService.INACTIVE_STATE;

  public get boolState(): boolean {
    return this.state === ToggleService.ACTIVE_STATE;
  }

  public toggle() {
    this.state = this.state === ToggleService.ACTIVE_STATE
      ? ToggleService.INACTIVE_STATE : ToggleService.ACTIVE_STATE;
  }

  public open() {
    this.state = ToggleService.ACTIVE_STATE;
  }

  public close() {
    this.state = ToggleService.INACTIVE_STATE;
  }
}
