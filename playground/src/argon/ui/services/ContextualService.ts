import { Host, Injectable, Optional, Self } from '@angular/core';
import { BsContextDirective } from '../directives/BsContextDirective';
import { BsSizeDirective } from '../directives/BsSizeDirective';
import { ClassNameDirective } from '../directives/ClassNameDirective';

@Injectable()
export class ContextualService {

  set baseClass(val: string) {
    this.setBaseClassName(this.bsContextDirective, val);
    this.setBaseClassName(this.bsSizeDirective, val);
  }

  get bsContext(): any {
    return this.bsContextDirective
      ? this.bsContextDirective.arBsContext
      : '';
  }

  set bsContext(val: any) {
    if (this.bsContextDirective) {
      this.bsContextDirective.arBsContext = val;
    }
  }

  get bsContextPrefix(): string {
    return this.bsContextDirective
      ? this.bsContextDirective.arBsContextPrefix
      : '';
  }

  set bsContextPrefix(val: string) {
    if (this.bsContextDirective) {
      this.bsContextDirective.arBsContextPrefix = val;
    }
  }

  get bsSizePrefix(): string {
    return this.bsSizeDirective
      ? this.bsSizeDirective.arBsSizePrefix
      : '';
  }

  set bsSizePrefix(val: string) {
    if (this.bsSizeDirective) {
      this.bsSizeDirective.arBsSizePrefix = val;
    }
  }

  get bsSize(): any {
    return this.bsSizeDirective
      ? this.bsSizeDirective.arBsSize
      : '';
  }

  set bsSize(val: any) {
    if (this.bsSizeDirective) {
      this.bsSizeDirective.arBsSize = val;
    }
  }

  constructor(
    @Host() @Self() @Optional() protected  bsContextDirective: BsContextDirective,
    @Host() @Self() @Optional() protected  bsSizeDirective: BsSizeDirective
  ) { }

  private setBaseClassName(contextualDirective: ClassNameDirective<any, any>, className: string) {
    if (contextualDirective) {
      contextualDirective.baseClassName = className;
    }
  }
}
