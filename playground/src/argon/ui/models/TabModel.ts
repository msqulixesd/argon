import { EmbeddedViewRef } from '@angular/core';

export class TabModel {

  public changed: boolean;

  public active: boolean;

  public viewRef?: EmbeddedViewRef<any>;

  constructor(
    public title: string = 'Tab title',
    public closeable: boolean = false
  ) { }

}
