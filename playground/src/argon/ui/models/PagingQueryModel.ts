import { PagingModel } from './PagingModel';
import { PagingControlChangeInterface } from '../interfaces/PagingControlChangeInterface';
import { PagingInterface } from '../interfaces/PagingInterface';

/**
 * This is sample of PagingInterface implementation
 */
export class PagingQueryModel<T> implements PagingInterface {

  public pageSize = 50;

  public page = 1;

  public offset = 0;

  public orderBy: string;

  public direction: 'asc' | 'desc';

  public lastPagingResult?: PagingModel;

  constructor(
    public filter: T
  ) { }

  public getPagesCount() {
    return Math.ceil(this.getTotalCount() / this.pageSize);
  }

  public getStartRecord(): number {
    return (this.page - 1) * this.pageSize + 1;
  }

  public getEndRecord(): number {
    const recordIndex = this.page * this.pageSize;
    const totalCount = this.getTotalCount();
    return totalCount < recordIndex ? totalCount : recordIndex;
  }

  public getTotalCount(): number {
    if (!this.lastPagingResult) {
      return 0;
    }

    return this.lastPagingResult.totalCount;
  }

  public getPageSize(): number {
    return this.pageSize;
  }

  public getPage(): number {
    return this.page;
  }

  public setOrder(orderBy: string, orderDirection: 'asc' | 'desc' = 'asc') {
    this.orderBy = orderBy;
    this.direction = orderDirection;
  }

  public setPageSize(size: number) {
    this.pageSize = size;
    this.page = 1;
  }

  public applyChanges(changes: PagingControlChangeInterface) {
    this.page = changes.page;
    if (changes.pageSize !== this.pageSize) {
      this.setPageSize(changes.pageSize);
    }
  }

  public setPagingResult(model: PagingModel) {
    this.lastPagingResult = model;
    this.pageSize = model.pageSize;
    this.page = model.page;
    this.offset = model.offset;
  }

  public updateFilter(filter: T) {
    this.page = 1;
    this.filter = filter;
  }

  public toJSON() {
    return {
      pageSize: this.pageSize,
      page: this.page,
      filter: this.filter,
      orderBy: this.orderBy || '',
      direction: this.direction || 'asc'
    };
  }

}
