import { ModalService } from '../services/ModalService';
import { LogService } from '../../core/services/LogService';

let modalService: ModalService;

export const modalServiceFactory = (logService: LogService) => {
  if (!modalService) {
    modalService = new ModalService(logService);
  }

  return modalService;
};
