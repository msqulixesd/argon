import { Component, ElementRef, Input, OnInit, Optional, Renderer2 } from '@angular/core';
import { BsTextContextEnum, BsTextContextType } from '../../types/BsTextEnumType';
import { AlertComponent } from '../Alert/AlertComponent';
import { CardComponent } from '../Card/CardComponent';

@Component({
  selector: 'ar-inner-link',
  template: `
    <a [routerLink]="url"
       [class.disabled]="disabled"
       [class.alert-link]="isAlertLink"
       [arBsTextContext]="textContext"
    >
      <ng-content></ng-content>
    </a>
  `,
  styleUrls: ['InnerLinkComponent.scss']
})
export class InnerLinkComponent implements OnInit {

  @Input() url: any;

  @Input() disabled: boolean;

  @Input() isAlert: boolean;

  @Input() isCard: boolean;

  public get textContext(): BsTextContextType {
    return (this.disabled ? BsTextContextEnum.MUTED : BsTextContextEnum.DEFAULT) as BsTextContextType;
  }

  public get isAlertLink(): boolean {
    return Boolean(this.alertComponent) || this.isAlert;
  }

  constructor(
    private element: ElementRef,
    private renderer: Renderer2,
    @Optional() private card: CardComponent,
    @Optional() private alertComponent: AlertComponent
  ) { }

  ngOnInit(): void {
    if (this.card) {
      this.renderer.addClass(this.element.nativeElement, 'card-link');
    }
  }
}
