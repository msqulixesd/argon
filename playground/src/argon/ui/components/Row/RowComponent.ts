import { Component, HostBinding, OnInit, Optional} from '@angular/core';
import { FormComponent } from '../../../form/components/From/FormComponent';

@Component({
  selector: 'ar-row, [arRow]',
  template: '<ng-content></ng-content>'
})
export class RowComponent implements OnInit {

  static ROW_CLASS = 'row';
  static FORM_ROW_CLASS = 'form-row';

  @HostBinding('class.row') baseClass = true;

  @HostBinding('class.form-row') formRow = false;

  constructor(
    @Optional() public formComponent: FormComponent
  ) { }

  ngOnInit(): void {
    this.formRow = Boolean(this.formComponent);
    this.baseClass = !this.formRow;
  }

}
