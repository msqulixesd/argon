import { Component, ElementRef, HostListener, OnInit, ViewChild, ViewContainerRef, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { IconService } from '../../services/IconService';
import { animate, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'ar-layout',
  template: `
    <section class="argon-layout" [@inOutState]>
      <!-- App content -->
      <ng-content></ng-content>
      <!-- Modal layout -->
      <ar-modal-layout></ar-modal-layout>
      <!-- Alerts -->
      <ar-alert-area></ar-alert-area>
      <!-- Icons -->
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" #iconsSprite style="display: none;"></svg>
    </section>
  `,
  styleUrls: ['../../../assets/common.scss', 'LayoutComponent.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [trigger('inOutState', [
    transition(':enter', [
      style({ opacity: 0 }),
      animate('1000ms ease-in', style({ opacity: 1 }))
    ]),
    transition(':leave', [
      style({ opacity: 1 }),
      animate('1000ms ease-out', style({ opacity: 0 }))
    ])
  ])
  ],
})
export class LayoutComponent implements OnInit {

  @ViewChild('iconsSprite') iconsSprite: ElementRef;

  public documentClick = new Subject<any>();

  constructor(
    private iconService: IconService,
    private element: ElementRef
  ) { }

  ngOnInit(): void {
    this.iconService.initialize(this.iconsSprite);
  }

  @HostListener('document:click', ['$event'])
  public closeHandler(event) {
    this.documentClick.next(event);
  }

}
