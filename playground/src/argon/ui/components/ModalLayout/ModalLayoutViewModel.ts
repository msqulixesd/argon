import { TemplateRef, ViewContainerRef, ViewRef } from '@angular/core';
import { ModalDialogConfigModel } from '../../models/ModalDialogConfigModel';

export class ModalLayoutViewModel {

  public container: ViewContainerRef;

  public template: TemplateRef<any>;

  public viewRef: ViewRef;

  public opened: boolean;

  public config: ModalDialogConfigModel;

  constructor(viewContainer: ViewContainerRef) {
    this.container = viewContainer;
  }

}
