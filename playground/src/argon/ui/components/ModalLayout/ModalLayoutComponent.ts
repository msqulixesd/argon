import { Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ModalService } from '../../services/ModalService';
import { ModalLayoutViewModel } from './ModalLayoutViewModel';
import { modalServiceFactory } from '../../factories/modalServiceFactory';
import { LogService } from '../../../core/services/LogService';

@Component({
  selector: 'ar-modal-layout',
  template: `
      <div class="modal fade" [class.show]="viewModel.opened" (click)="backdropClick($event)">
          <div [class]="getDialogClassName()" #dialog>
            <ng-container #modalContent></ng-container>
          </div>
      </div>
      <div class="modal-backdrop fade show" *ngIf="viewModel.opened"></div>
  `,
  styles: ['.show { display: block; }'],
  providers: [{
    provide: ModalService,
    useFactory: modalServiceFactory,
    deps: [ LogService ]
  }]
})
export class ModalLayoutComponent implements OnInit, OnDestroy {

  @ViewChild('modalContent', {read: ViewContainerRef}) modalContent: ViewContainerRef;
  @ViewChild('dialog', {read: ElementRef}) dialog: ElementRef;

  public viewModel: ModalLayoutViewModel;

  constructor(
    private modalService: ModalService
  ) { }

  ngOnInit() {
    this.viewModel = this.modalService.registerLayout(this.modalContent);
  }

  ngOnDestroy() {
    this.modalService.unregisterLayout();
  }

  public backdropClick(event: Event) {
    if (!this.viewModel.config) {
      return;
    }

    if (!this.viewModel.config.backdropClickClose) {
      return;
    }

    if (this.dialog.nativeElement.contains(event.target)) {
      return;
    }

    this.modalService.hideModal();
  }

  public getDialogClassName() {
    let className = this.viewModel.config
      ? `modal-dialog ${this.viewModel.config.className}`
      : 'modal-dialog';

    className = this.viewModel.config && this.viewModel.config.centered
      ? `${className} modal-dialog-centered`
      : className;

    return className;
  }
}
