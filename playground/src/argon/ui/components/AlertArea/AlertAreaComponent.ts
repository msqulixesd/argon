import { Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { AlertsAreaService } from '../../services/AlertsAreaService';
import { DisplayAlertMessage } from '../../../core/messages/DisplayAlertMessage';
import { MessageBusService } from '../../../core/services/MessageBusService';

@Component({
  selector: 'ar-alert-area',
  template: `
      <ng-container #alertContainer></ng-container>
      <ng-template #alertTemplate let-message>
          <ar-alert [arBsContext]="message.bsContext"
                   [closeable]="message.closeable"
                   (close)="alertService.removeAlert(message)"
                   class="inner-alert"
          >
              <ng-container *ngIf="message.isSimple; else contentTemplate">{{ message.message }}</ng-container>
              <ng-template #contentTemplate>
                  <ng-container *ngTemplateOutlet="message.template; context: { $implicit: message.templateContext }">
                  </ng-container>
              </ng-template>
          </ar-alert>
      </ng-template>
  `,
  providers: [AlertsAreaService],
  styleUrls: ['AlertsArea.component.scss']
})
export class AlertAreaComponent implements OnInit, OnDestroy {

  @ViewChild('alertContainer', { read: ViewContainerRef }) alertContainer: ViewContainerRef;
  @ViewChild('alertTemplate') alertTemplate: TemplateRef<any>;

  private alertSubscription: Subscription;

  constructor(
    private messageService: MessageBusService,
    public alertService: AlertsAreaService
  ) { }

  ngOnInit(): void {
    this.alertService.registerContainer(this.alertContainer);
    this.alertService.registerTemplate(this.alertTemplate);
    this.alertSubscription = this.messageService.of(DisplayAlertMessage).subscribe(this.alertService.addAlert);
  }

  ngOnDestroy(): void {
    this.alertSubscription.unsubscribe();
  }

}
