import { Component, EventEmitter, HostBinding, Input, Output } from '@angular/core';
import { ContextualService } from '../../services/ContextualService';

@Component({
  selector: 'ar-alert',
  template: `
    <button *ngIf="closeable" type="button" class="close" (click)="close.emit()">
      <span aria-hidden="true">&times;</span>
    </button>
    <ng-content></ng-content>
  `,
  styles: [`
    :host { display: block; }
  `],
  providers: [
    ContextualService
  ]
})
export class AlertComponent {

  static BASE_CLASS = 'alert';

  @Input() closeable: boolean;

  @Output() close = new EventEmitter<void>();

  @HostBinding('class.alert') className = true;

  @HostBinding('attr.role') role = 'alert';

  constructor(
    contextualService: ContextualService
  ) {
    contextualService.baseClass = AlertComponent.BASE_CLASS;
  }
}
