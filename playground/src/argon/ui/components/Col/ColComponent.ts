import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'ar-col',
  template: '<ng-content></ng-content>',
  styles: [`
    :host { display: block; }
  `]
})
export class ColComponent {

  @HostBinding('class.col') className = true;

}
