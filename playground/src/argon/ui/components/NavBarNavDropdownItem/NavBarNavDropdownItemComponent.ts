import { Component, ViewChild } from '@angular/core';
import { DropdownComponent } from '../Dropdown/DropdownComponent';

@Component({
  selector: 'ar-nav-bar-nav-dropdown-item',
  template: `
    <li class="nav-item">
      <ar-dropdown #dropdown>
        <ng-container title>
          <span class="nav-link dropdown-toggle">
            <ng-content select="[title]"></ng-content>
          </span>    
        </ng-container>
        <ng-content></ng-content>
      </ar-dropdown>
    </li>
  `
})
export class NavBarNavDropdownItemComponent {

  @ViewChild('dropdown') dropdown: DropdownComponent;

  public close() {
    this.dropdown.close();
  }

}
