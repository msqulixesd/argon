import {
  ChangeDetectorRef, Component, ElementRef, Input, OnDestroy,
  OnInit} from '@angular/core';
import { Subscription } from 'rxjs';
import { LayoutComponent } from '../Layout/LayoutComponent';
import { ToggleService } from '../../services/ToggleService';

@Component({
  selector: 'ar-dropdown',
  template: `
    <div class="dropdown ar-ui-dropdown" [class.dropup]="dropUp" [class.full-width]="fullWidth">
      <div class="dropdown-menu-selector" (click)="toggle()">
        <ng-content select="[title]"></ng-content>
      </div>
      <div
        *ngIf="opened"
        class="dropdown-menu"
        [@toggleServiceState]="toggleService.state"
      >
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styleUrls: ['Dropdown.component.scss'],
  animations: [ ToggleService.ANIMATION_OPACITY ],
  providers: [ ToggleService ]
})
export class DropdownComponent implements OnInit, OnDestroy {

  @Input() disabled: boolean;

  @Input() fullWidth: boolean;

  @Input() dropUp: boolean;

  public get opened(): boolean {
    return this.toggleService.boolState;
  }

  public clickSubscription: Subscription;

  constructor(
    protected element: ElementRef,
    protected layout: LayoutComponent,
    protected ref: ChangeDetectorRef,
    public toggleService: ToggleService
  ) { }

  ngOnInit(): void {
    this.clickSubscription = this.layout.documentClick.subscribe(this.closeHandler);
  }

  ngOnDestroy(): void {
    if (this.clickSubscription) {
      this.clickSubscription.unsubscribe();
      this.clickSubscription = null;
    }
  }

  public closeHandler = (event) => {
    if (this.element.nativeElement.contains(event.target)) {
      return;
    }

    if (this.opened) {
      this.close();
      this.ref.markForCheck();
    }
  }

  public close() {
    this.toggleService.close();
  }

  public toggle() {
    if (this.disabled) {
      this.close();
    } else {
      this.toggleService.toggle();
    }
  }
}
