import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ar-switch',
  template: `
      <div class="onoffswitch" [class.disabled]="disabled" (click)="clickHandler()">
          <span class="onoffswitch-label" [class.checked]="active">
              <span class="onoffswitch-inner"></span>
              <span class="onoffswitch-switch"></span>
          </span>
      </div>
  `,
  styleUrls: ['Switch.component.scss']
})
export class SwitchComponent {

  @Input() active: boolean;

  @Input() disabled: boolean;

  @Output() toggle = new EventEmitter<void>();

  clickHandler() {
    if (this.disabled) {
      return;
    }

    this.toggle.emit();
  }

}
