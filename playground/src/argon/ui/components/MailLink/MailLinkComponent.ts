import { Component, Input } from '@angular/core';

@Component({
  selector: 'ar-mail-link',
  template: `
    <a *ngIf="email" href="mailto:{{email}}">{{email}}</a>
  `
})
export class MailLinkComponent {

  @Input() email: string;

}
