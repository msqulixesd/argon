import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { InnerLinkComponent } from '../InnerLink/InnerLinkComponent';
// import * as URL from 'url-parse';

@Component({
  selector: 'ar-outer-link',
  template: `
    <a target="_blank" [href]="urlString" *ngIf="url"
       [class.disabled]="disabled"
       [class.alert-link]="isAlertLink"
       [arBsTextContext]="textContext"
    >
        <ng-content></ng-content>
    </a>
  `,
  styleUrls: ['../InnerLink/InnerLinkComponent.scss', 'OuterLinkComponent.scss']
})
export class OuterLinkComponent extends InnerLinkComponent implements OnChanges {

  @Input() url: any;

  @Input() disabled: boolean;

  @Input() isAlert: boolean;

  @Input() isCard: boolean;

  public parsedUrl: any;

  public urlString: string;

  ngOnChanges(changes: SimpleChanges): void {
    const { url, href } = changes;

    if (!url || (url.currentValue === url.previousValue && !url.firstChange)) {
      return;
    }

    if (!url.currentValue) {
      this.urlString = '';
      this.parsedUrl = null;
      return;
    }

    this.urlString = url.currentValue;

    // let value = url.currentValue;
    //
    // if (!/^https?:\/\//i.test(value)) {
    //   value = 'http://' + value;
    // }
    //
    // this.parsedUrl = URL(value, {});
    // this.urlString = this.parsedUrl.toString();
  }

}
