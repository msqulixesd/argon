import { Component } from '@angular/core';

@Component({
  selector: 'ar-nav-bar-nav-item',
  template: `
    <li class="nav-item">
      <span class="nav-link">
        <ng-content></ng-content>
      </span>
    </li>
  `
})
export class NavBarNavItemComponent {

}
