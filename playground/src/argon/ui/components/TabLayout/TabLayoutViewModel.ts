import { find, size, without, findIndex, first } from 'lodash';
import { TabModel } from '../../models/TabModel';

export class TabLayoutViewModel {

  public get hasOneTab() {
    return size(this.tabs) === 1;
  }

  public get hasTabs(): boolean {
    return size(this.tabs) > 1;
  }

  public tabs: Array<TabModel>;

  public tabToClose: TabModel;

  public selectedTab: TabModel;

  public addTab(tab: TabModel) {
    if (!this.tabs) {
      this.tabs = [tab];
      this.selectTab(tab);
    } else {
      this.tabs.push(tab);
    }
  }

  public removeTab(tab: TabModel) {
    if (this.selectedTab === tab) {
      const nextTab = this.getNextTab(tab);
      this.selectTab(nextTab);
    }
    this.tabs = without(this.tabs, tab);
  }

  public selectTab(tab?: TabModel) {
    if (!tab) {
      return;
    }

    if (this.selectedTab) {
      this.selectedTab.active = false;
    }

    tab.active = true;
    this.selectedTab = tab;
  }

  public exist(tab: TabModel): boolean {
    return Boolean(find(this.tabs, (item) => item === tab));
  }

  private getNextTab(tabToDelete: TabModel): TabModel {
    const currentTabIndex = findIndex(this.tabs, tabToDelete);

    if (currentTabIndex < 0) {
      return first(this.tabs);
    }

    if (currentTabIndex === 0 && size(this.tabs) > 1) {
      return this.tabs[1];
    }

    if (currentTabIndex > 0) {
      return this.tabs[currentTabIndex - 1];
    }

    return null;
  }
}
