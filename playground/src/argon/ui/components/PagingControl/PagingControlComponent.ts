import { Component, EventEmitter, Input, Output } from '@angular/core';
import { PagingControlChangeInterface } from '../../interfaces/PagingControlChangeInterface';
import { PagingInterface } from '../../interfaces/PagingInterface';
import { BsSizeEnum } from '../../types/BsSizeEnumType';
import { ContextualService } from '../../services/ContextualService';

@Component({
  selector: 'ar-paging-control',
  template: `
    <ng-container *ngIf="paging">
      <small>Records per page</small>
      <ar-paging-list-size
        [size]="paging.getPageSize()"
        (onSizeChange)="onChange.emit({ pageSize: $event, page: paging.getPage() })"
        [arBsSize]="contextualService.bsSize"
      >
      </ar-paging-list-size>
      <ar-pagination
        [total]="paging.getPagesCount()"
        [current]="paging.getPage()"
        (onPageSelect)="onChange.emit({ pageSize: paging.getPageSize(), page: $event })"
        [arBsSize]="contextualService.bsSize"
      >
      </ar-pagination>
      <ar-paging-text [paging]="paging"></ar-paging-text>
    </ng-container>
  `,
  styles: [
    ':host { display: flex; align-items: center; } '
  ],
  providers: [ContextualService]
})
export class PagingControlComponent {

  @Input() paging: PagingInterface;

  @Input() bsSize = BsSizeEnum.SM;

  @Output() onChange = new EventEmitter<PagingControlChangeInterface>();

  constructor(
    public contextualService: ContextualService
  ) { }
}
