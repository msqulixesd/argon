import { Component, Input, TemplateRef } from '@angular/core';
@Component({
  selector: 'ar-spinner',
  template: `
    <div class="spinner" *ngIf="state"></div>
    <div [class.layout]="state">
      <ng-template *ngTemplateOutlet="templateRef"></ng-template>
      <ng-content></ng-content>
    </div>
  `,
  styleUrls: ['Spinner.component.scss']
})
export class SpinnerComponent {

  @Input() set visible(state: boolean) { this.state = state; }
  get visible(): boolean { return this.state; }

  @Input() set invisible(state: boolean) {
    this.state = !state;
  }

  public templateRef: TemplateRef<any>;

  public state: boolean;

  public show() {
    this.state = true;
  }

  public hide() {
    this.state = false;
  }
}
