import { Component, HostBinding, Input } from '@angular/core';
import { ContextualService } from '../../services/ContextualService';

@Component({
  selector: 'ar-icon',
  template: `
    <svg xmlns="http://www.w3.org/2000/svg" #svg [ngStyle]="svgStyle">
      <use [attr.xlink:href]="link" *ngIf="name" />
    </svg>
  `,
  styleUrls: ['Icon.component.scss'],
  providers: [ ContextualService ]
})
export class IconComponent {

  static BASE_CLASS = 'ar-icon';

  public get svgStyle(): any {
    const style = {};
    if (this.size) {
      style['width.px'] = this.size;
      style['height.px'] = this.size;
    }

    return style;
  }

  @Input() name: string;

  @Input() size: number;

  @HostBinding('class.ar-icon') className = true;

  public get link(): string { return `#${this.name}`; }

  constructor(
    protected contextualService: ContextualService,
  ) {
    contextualService.baseClass = IconComponent.BASE_CLASS;
  }
}
