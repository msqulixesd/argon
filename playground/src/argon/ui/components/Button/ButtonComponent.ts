import {
  ChangeDetectionStrategy, Component, EventEmitter, HostBinding, Input, OnInit, Optional, Output
} from '@angular/core';
import { ContextualService } from '../../services/ContextualService';
import { ButtonGroupComponent } from '../ButtonGroup/ButtonGroupComponent';

@Component({
  selector: 'ar-button, [arButton]',
  template: `
    <ng-content></ng-content>
  `,
  styleUrls: ['./ButtonComponent.scss'],
  providers: [ ContextualService ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonComponent implements OnInit {

  static BASE_CLASS = 'btn';

  @Input() set disabled(val: boolean) {
    this.attrDisabled = val;
    this.disabledClassName = val;
  }

  @Input() set block(val: boolean) {
    this.blockClassName = val;
  }

  @Output() click = new EventEmitter<void>();

  @HostBinding('class.btn') className = true;

  @HostBinding('class.btn-block') blockClassName = false;

  @HostBinding('class.disabled') disabledClassName = false;

  @HostBinding('attr.role') role = 'button';

  @HostBinding('attr.disabled') attrDisabled = false;

  constructor(
    protected contextualService: ContextualService,
    @Optional() protected buttonGroup: ButtonGroupComponent
  ) {
    contextualService.baseClass = ButtonComponent.BASE_CLASS;
  }

  ngOnInit(): void {
    if (this.buttonGroup) {
      this.contextualService.bsContext = this.buttonGroup.bsContext;
      this.contextualService.bsSize = this.buttonGroup.bsSize;
      this.contextualService.bsContextPrefix = this.buttonGroup.bsContextPrefix;
    }
  }
}
