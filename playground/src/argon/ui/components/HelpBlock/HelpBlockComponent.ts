import { Component, Input } from '@angular/core';

@Component({
  selector: 'ar-help-block, ar-form-text',
  template: `
    <span class="form-text" [class.form-text--mini]="mini" arBsTextContext="muted">
      <ng-content></ng-content>
    </span>
  `,
  styles: [`
    .form-text--mini {
      font-size: 80%;
    }
  `]
})
export class HelpBlockComponent {

  @Input() mini = true;

}
