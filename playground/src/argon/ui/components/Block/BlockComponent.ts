import { Component, Input } from '@angular/core';

@Component({
  selector: 'ar-block',
  template: `
    <p *ngIf="title" class="h6 border-bottom border-muted" arBsTextContext="muted">{{ title }}</p>
    <ng-content></ng-content>
  `,
  styleUrls: ['BlockComponent.scss']
})
export class BlockComponent {

  @Input() title: string;

}
