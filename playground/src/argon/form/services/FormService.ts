import { Injectable} from '@angular/core';

@Injectable()
export class FormService {

  static FORM_ID_PREFIX = 'Form';

  static CONTROL_PREFIX = 'Input';

  public formId: string;

  public bsBgContext: string;

  private id = 0;

  public generateControlId(): string {
    return this.generateId(`${this.formId}_${FormService.CONTROL_PREFIX}`);
  }

  public generateSubControlId(control_id: string, value: any): string {
    return `${control_id}_${value}`;
  }

  public generateId(prefix: string): string {
    this.id++;
    return `${prefix}_${this.id}`;
  }

}
