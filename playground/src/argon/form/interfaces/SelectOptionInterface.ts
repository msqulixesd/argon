import { DictionaryItemInterface } from '../../core/interfaces/DictionaryItemInterface';

export interface SelectOptionInterface extends DictionaryItemInterface {

  label?: string;

  value: any;

  disabled?: boolean;

  extendedData?: any;

}
