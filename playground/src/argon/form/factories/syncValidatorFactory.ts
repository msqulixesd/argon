import { has, first, last } from 'lodash';
import { FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

const emailPattern = new RegExp('^.*@.*\\..*$', 'i');
const alphaDashPattern = new RegExp('^[a-z0-9_\\-]+$', 'i');

export const syncValidatorFactory = (
  name: string, validatorArguments: Array<string>,
  allValidators: object
): ValidatorFn | Array<ValidatorFn> => {

  switch (name) {
    case 'required':
      return has(allValidators, 'nullable') ? null : Validators.required;
    case 'max': return Validators.max.apply(null, validatorArguments);
    case 'min': return Validators.min.apply(null, validatorArguments);
    case 'size': {
      const size = Number(first(validatorArguments));
      return isFinite(size) ? createSizeValidator(size) : null;
    }
    case 'between':
      return [
        Validators.minLength(Number(first(validatorArguments))),
        Validators.maxLength(Number(last(validatorArguments)))
      ];
    case 'email': return emailValidator;
    case 'alpha_dash': return alphaDashValidator;
    default: return null;
  }

};

export const emailValidator = (c: FormControl) => (
  (!c.value || emailPattern.test(c.value)) ? null : { email: null } as ValidationErrors
);

export function createSizeValidator(size: any) {
  return (c: FormControl) => {
    const valueSize = c.value ? String(c.value).length : 0;
    return valueSize !== size && valueSize > 0
      ? {
        size: {
          requiredSize: size,
          value: c.value
        }
      } as ValidationErrors
      : null;
  };
}

export function createConfirmValidator(fieldName: string, confirmationName: string, message: string) {
  return (c: FormGroup) => {
    const fieldValue = c.get(fieldName).value;
    const confirmation = c.get(confirmationName);
    const currentErrors = confirmation.errors;
    if (fieldValue !== confirmation.value) {
      confirmation.setErrors({ confirmation: message });
    } else {
      confirmation.setErrors(null);
    }
    
    return null;
  };
}

export const alphaDashValidator = (c: FormControl) => (
  (!c.value || alphaDashPattern.test(c.value)) ? null : { alpha_dash: null } as ValidationErrors
);
