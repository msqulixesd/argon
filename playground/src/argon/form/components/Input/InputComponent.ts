import { AbstractControl, FormControl } from '@angular/forms';
import { Component, HostBinding, Input, OnInit, Optional, Renderer2 } from '@angular/core';
import { FormGroupComponent } from '../FormGroup/FormGroupComponent';
import { FormService } from '../../services/FormService';
import { InputGroupComponent } from '../InputGroup/InputGroupComponent';

@Component({
  selector: 'ar-input',
  template: `
    <input
      [type]="type"
      class="form-control"
      [class.is-invalid]="hasError"
      [placeholder]="placeholder"
      arBsSize
      [arBsBgContext]="formService?.bsBgContext"
      [formControl]="control"
      [attr.id]="controlId"
      [autocomplete]="autocomplete"
      [name]="inputName"
    >
  `,
  styleUrls: ['InputComponent.scss']
})
export class InputComponent implements OnInit {

  @Input() type = 'text';

  @Input() placeholder = '';

  @Input() control: AbstractControl;

  @Input() name: string;

  @HostBinding('class.form-control') baseClass = false;

  public get hasError(): boolean {
    return this.control && this.control.invalid && (this.control.dirty || this.control.touched);
  }

  public get pending() {
    return this.control && this.control.pending;
  }

  public get inputName(): string {
    return this.name || this.controlId;
  }

  public get autocomplete(): string {
    return this.type === 'password' ? 'nope' : 'off';
  }

  public controlId = null;

  constructor(
    @Optional() public formGroupComponent: FormGroupComponent,
    @Optional() public formService: FormService,
    @Optional() protected inputGroup: InputGroupComponent,
    protected renderer: Renderer2
  ) { }

  ngOnInit(): void {
    if (!this.control) {
      this.control = new FormControl();
    }

    if (this.formService) {
      this.controlId = this.formService.generateControlId();
    }

    if (this.inputGroup) {
      this.baseClass = true;
    }

    this.attachToFormGroup();
  }

  protected attachToFormGroup() {
    if (this.formGroupComponent && this.control) {
      this.formGroupComponent.control = this.control;
      this.formGroupComponent.controlId = this.controlId;
    }
  }
}
