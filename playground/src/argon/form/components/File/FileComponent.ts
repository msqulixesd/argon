import { InputComponent } from '../Input/InputComponent';
import { size, first } from 'lodash';
import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'ar-file',
  template: `
    <ar-input-group [prepend]="hasValue" [append]="true" arBsSize>
      <ng-container prepend>
        <ar-input-group-text class="remove-addon" (click)="removeFile()">
          <ar-icon name="close-circle" arBsContext arBsSize></ar-icon>
        </ar-input-group-text>
      </ng-container>
      <div class="custom-file" arBsSize *ngIf="control">

        <input type="file"
               class="custom-file-input"
               [class.is-invalid]="hasError"
               [id]="controlId"
               (change)="onFileChange($event)"
               [accept]="accept"
               [name]="inputName"
               #input
        >
        <label
          class="custom-file-label"
          [for]="controlId"
          [arBsBgContext]="formService?.bsBgContext"
        >{{ fileName }}</label>
      </div>

      <ng-container append>
        <ar-input-group-text>
          Browse
        </ar-input-group-text>
      </ng-container>
    </ar-input-group>
  `,
  styleUrls: ['FileComponent.scss']
})
export class FileComponent extends InputComponent {

  @Input() placeholder = 'Choose file...';

  @Input() control: AbstractControl;

  @Input() name: string;

  @Input() accept = '*';

  @ViewChild('input') input: ElementRef;

  public get fileName(): string {
    if (!this.control.value) {
      return this.placeholder;
    }

    const file: File = this.control.value;
    return file.name;
  }

  public get hasValue(): boolean {
    return Boolean(this.control.value);
  }

  public onFileChange(event: any) {
    const value = size(event.target.files) > 0 ? first(event.target.files) : null;
    this.control.setValue(value);
  }

  public removeFile() {
    this.control.setValue(null);
    this.input.nativeElement.value = null;
  }

}
