import { InputComponent } from '../Input/InputComponent';
import { Component, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'ar-checkbox',
  template: `
    <div class="custom-checkbox form-check"
         [class.custom-control-inline]="inline"
         *ngIf="control"
         arBsContext
         arBsSize
    >
      <input class="custom-control-input"
             [class.is-invalid]="hasError" 
             type="checkbox" 
             [id]="controlId" 
             [formControl]="control"
             [name]="inputName"
      >
      <label class="custom-control-label" [for]="controlId">
        <ng-content></ng-content>
      </label>
    </div>
  `,
  styleUrls: ['CheckboxComponent.scss']
})
export class CheckboxComponent extends InputComponent {

  @Input() inline: boolean;

  @Input() control: AbstractControl;

  @Input() name: string;

  protected attachToFormGroup() {
    //
  }
}
