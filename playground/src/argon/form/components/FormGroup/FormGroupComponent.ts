import { AfterViewInit, Component, ElementRef, HostBinding, Input, Optional, Renderer2, Self } from '@angular/core';
import { ContextualService } from '../../../ui/services/ContextualService';
import { RowComponent } from '../../../ui/components/Row/RowComponent';
import { get, find, startsWith, first } from 'lodash';
import { AbstractControl } from '@angular/forms';
import { ErrorMessageService } from '../../services/ErrorMessageService';

@Component({
  selector: 'ar-form-group',
  template: `
    <div class="form-control-content">
      <label *ngIf="label" class="control-label" arBsSize [for]="controlId">{{label}}</label>
      <ng-content></ng-content>
      <div *ngIf="hasError" [class.invalid-feedback]="!useTooltip" [class.invalid-tooltip]="useTooltip">
        <span *ngFor="let message of messages">{{message}}</span>
      </div>
    </div>
    <ar-form-text [hidden]="isHelpBlockHidden">
      <ng-content select="[hint]"></ng-content>
    </ar-form-text>
  `,
  providers: [ContextualService],
  styles: [
    ':host { display: block; }',
    '.form-control-content { position: relative; }',
    '.invalid-feedback { display: block }',
    '.invalid-tooltip  { display: block }',
    '.control-label-sm { font-size: 90%; margin-bottom: .2rem; }'
  ]
})
export class FormGroupComponent implements AfterViewInit {

  static BASE_CLASS = 'form-group';

  @Input() label: string;

  @Input() showFirstError = true;

  @Input() useTooltip = true;

  @HostBinding('class.form-group') baseClass = true;

  @HostBinding('class.form-check') isCheckbox = false;

  public get hasError(): boolean {
    return this.control && this.control.invalid && (this.control.dirty || this.control.touched);
  }

  public get pending() {
    return this.control && this.control.pending;
  }

  public get isHelpBlockHidden(): boolean {
    return this.hasError && !this.useTooltip;
  }

  public get messages(): Array<string> {
    if (!this.control) {
      return [];
    }

    return this.showFirstError
      ? [this.errorMessageService.getFirstErrorMessage(this.control)]
      : this.errorMessageService.getErrorMessageList(this.control);
  }

  public set control(val: AbstractControl) {
    if (!find(this._control, val)) {
      this._control.push(val);
    }
  }

  public get control(): AbstractControl {
    return first(this._control);
  }

  public controlId: string;

  protected _control = [];

  constructor(
    contextualService: ContextualService,
    private elementRef: ElementRef,
    private renderer: Renderer2,
    private errorMessageService: ErrorMessageService,
    @Self() @Optional() private row: RowComponent
  ) {
    contextualService.baseClass = FormGroupComponent.BASE_CLASS;
  }

  ngAfterViewInit(): void {
    if (this.hasClassName(this.elementRef.nativeElement, 'col')) {
      return;
    }

    const parentNode = this.renderer.parentNode(this.elementRef.nativeElement);
    if (this.hasClassName(parentNode, RowComponent.ROW_CLASS)
      || this.hasClassName(parentNode, RowComponent.FORM_ROW_CLASS)) {
      this.renderer.addClass(this.elementRef.nativeElement, 'col');
    }
  }

  private hasClassName(element: any, search: string): boolean {
    if (!element) {
      return false;
    }

    const classList = get(element, 'classList', []);
    return Boolean(find(classList, (className: string) => startsWith(className, search)));
  }
}
