import { InputComponent } from '../Input/InputComponent';
import { size } from 'lodash';
import { Component, Input, OnInit, Optional, Renderer2, TemplateRef, ViewChild, OnDestroy } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import { SelectOptionInterface } from '../../interfaces/SelectOptionInterface';
import { Observable, Subject, of, Subscription } from 'rxjs';
import { debounceTime, map, mergeMap, distinctUntilChanged } from 'rxjs/operators';
import { InputGroupComponent } from '../InputGroup/InputGroupComponent';
import { FormService } from '../../services/FormService';
import { FormGroupComponent } from '../FormGroup/FormGroupComponent';

@Component({
  selector: 'ar-autocomplete',
  templateUrl: 'AutocompleteComponent.html',
  styleUrls: ['AutocompleteComponent.scss']
})
export class AutocompleteComponent extends InputComponent implements OnInit, OnDestroy {

  @Input() placeholder = '';

  @Input() control: AbstractControl;

  @Input() name: string;

  @Input() noResultText = 'Nothing found';

  @Input() keyUpDelay = 500;

  @Input() searchFunction: (q: string) => Observable<Array<SelectOptionInterface>>;

  @Input() optionTemplate: TemplateRef<any>;

  @Input() defaultOption: SelectOptionInterface;

  @Input() dropUp: boolean;

  @ViewChild('defaultOptionTemplate') defaultOptionTemplate: TemplateRef<any>;

  public get delay(): number {
    return this.keyUpDelay;
  }

  public get noResult(): boolean {
    return size(this.collection) === 0;
  }

  public get optionTemplateToRender(): TemplateRef<any> {
    return this.optionTemplate ? this.optionTemplate : this.defaultOptionTemplate;
  }

  public searchControl = new FormControl('');

  public searchPending: boolean;

  public selectedOption: SelectOptionInterface;

  public displayText: string;

  public optionChange = new Subject<SelectOptionInterface>();

  public collection: Array<SelectOptionInterface> = [];

  protected controlChangeSubscription: Subscription;

  constructor(
    @Optional() formGroupComponent: FormGroupComponent,
    @Optional() formService: FormService,
    @Optional() inputGroup: InputGroupComponent,
    renderer: Renderer2
  ) {
    super(formGroupComponent, formService, inputGroup, renderer);
  }

  ngOnInit() {
    super.ngOnInit();

    if (this.control.value) {
      this.selectedOption = this.defaultOption || {
        label: this.control.value,
        value: this.control.value,
      };

      this.displayText = this.selectedOption.label;
    }

    this.controlChangeSubscription = this.searchControl.valueChanges.pipe(
      debounceTime(this.delay),
      distinctUntilChanged(),
      map(this.beforeSearch),
      mergeMap(this.runSearch),
      map(this.afterSearch)
    ).subscribe(this.handleResponse, this.handleError);
  }

  ngOnDestroy() {
    if (this.controlChangeSubscription) {
      this.controlChangeSubscription.unsubscribe();
    }
  }

  public handleChange(item: SelectOptionInterface) {
    this.control.setValue(item.value);
    this.selectedOption = item;
    this.displayText = item.label;
    this.optionChange.next(item);
  }

  public handleRemove() {
    this.control.setValue('');
    this.searchControl.reset(null, { emitEvent: false });
    this.selectedOption = null;
    this.displayText = '';
    this.optionChange.next(null);
  }

  public handleFocus() {
    if (!this.searchControl.touched) {
      this.searchControl.setValue('');
    }
  }

  public handleBlur() {
    this.control.markAsTouched();
  }

  public handlePlaceholderClick(input: any) {
    if (this.control.disabled) {
      return;
    }

    input.focus();
  }

  protected runSearch = (query: string): Observable<Array<SelectOptionInterface>> => {
    return this.search(query);
  }

  protected beforeSearch = (query: string): string => {
    this.searchPending = true;
    return query;
  }

  protected afterSearch = (response: Array<SelectOptionInterface>): Array<SelectOptionInterface> => {
    this.searchPending = false;
    return response;
  }

  protected handleResponse = (response: Array<SelectOptionInterface>) => {
    this.collection = response;
  }

  protected handleError = () => {
    return [];
  }

  protected search(query: string): Observable<Array<SelectOptionInterface>> {
    if (!this.searchFunction) {
      return of([]);
    }

    return this.searchFunction(query);
  }
}
