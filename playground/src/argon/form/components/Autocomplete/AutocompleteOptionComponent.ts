import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Optional, Output,
  TemplateRef
} from '@angular/core';
import { SelectOptionInterface } from '../../interfaces/SelectOptionInterface';
import { AutocompleteComponent } from './AutocompleteComponent';
import { Subscription } from 'rxjs';
import { AutocompleteListComponent } from '../AutocompleteList/AutocompleteListComponent';
import { AutocompleteDictionaryComponent } from '../AutocompleteDictionary/AutocompleteDictionaryComponent';

@Component({
  selector: 'ar-autocomplete-option-component',
  template: `    
    <ar-dropdown-menu-item
      [closeOnSelect]="active"
      [disabled]="!active"
      (click)="handleClick()"
    >
      <ng-container *ngTemplateOutlet="optionTemplate; context: { $implicit: option }"></ng-container>
    </ar-dropdown-menu-item>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutocompleteOptionComponent implements OnInit, OnDestroy {

  @Input() optionTemplate: TemplateRef<any>;

  @Input() option: SelectOptionInterface;

  @Input() readonly: boolean;

  @Output() selectOption = new EventEmitter<SelectOptionInterface>();

  public get active(): boolean {
    return !this.isSelectedOption && !this.readonly;
  }

  public get autocomplete(): AutocompleteComponent {
    if (this.autocompleteComponent) {
      return this.autocompleteComponent;
    }

    if (this.autocompleteListComponent) {
      return this.autocompleteListComponent;
    }

    if (this.autocompleteDictionaryComponent) {
      return this.autocompleteDictionaryComponent;
    }

    throw new Error('AutocompleteOptionComponent: Autocomplete component required!' +
      '(Suggested components: AutocompleteComponent, AutocompleteListComponent)');
  }

  protected changeValueSubscription: Subscription;

  protected isSelectedOption = false;

  constructor(
    @Optional() protected autocompleteComponent: AutocompleteComponent,
    @Optional() protected autocompleteListComponent: AutocompleteListComponent,
    @Optional() protected autocompleteDictionaryComponent: AutocompleteDictionaryComponent,
    private ref: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.changeValueSubscription = this.autocomplete.optionChange.subscribe(this.handleOptionChange);
  }

  ngOnDestroy(): void {
    if (this.changeValueSubscription) {
      this.changeValueSubscription.unsubscribe();
    }
  }

  public handleClick() {
    if (this.active) {
      this.selectOption.emit(this.option);
    }
  }

  public handleOptionChange = (option: SelectOptionInterface) => {
    const nextState = Boolean(option) && option.value === this.option.value;
    if (nextState !== this.isSelectedOption) {
      this.ref.markForCheck();
      this.isSelectedOption = nextState;
    }
  }
}
