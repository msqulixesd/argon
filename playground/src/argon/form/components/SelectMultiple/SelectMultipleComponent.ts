import { SelectComponent } from '../Select/SelectComponent';
import { Component, Input } from '@angular/core';
import { SelectOptionInterface } from '../../interfaces/SelectOptionInterface';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'ar-select-multiple',
  template: `
    <select
      class="custom-select"
      [arBsBgContext]="formService?.bsBgContext"
      [class.is-invalid]="hasError"
      arBsSize
      [formControl]="control"
      multiple
      [size]="size"
      [name]="inputName"
    >
      <option *ngFor="let option of options" [ngValue]="option.value" [disabled]="option.disabled">
        {{ option.label || option.value }}
      </option>
    </select>
  `,
  styleUrls: ['../Input/InputComponent.scss']
})
export class SelectMultipleComponent extends SelectComponent {

  @Input() size = 4;

  @Input() options: Array<SelectOptionInterface> = [];

  @Input() control: AbstractControl;

  @Input() name: string;

}
