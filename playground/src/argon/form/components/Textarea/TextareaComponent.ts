import { InputComponent } from '../Input/InputComponent';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { size } from 'lodash';

@Component({
  selector: 'ar-textarea',
  template: `
    <div *ngIf="multiLine" class="form-control hidden-content" arBsSize>{{ content }}</div>
    <textarea
      [arBsBgContext]="formService?.bsBgContext"
      class="form-control"
      [class.is-invalid]="hasError"
      [class.resize]="resize"
      [class.multiline]="multiLine"
      [placeholder]="placeholder"
      arBsSize
      [formControl]="control"
      [attr.id]="controlId"
      autocomplete="nope"
      [name]="inputName"
      [attr.rows]="size"
    ></textarea>
  `,
  styleUrls: ['../Input/InputComponent.scss', 'TextareaComponent.scss']
})
export class TextareaComponent extends InputComponent implements OnInit, OnDestroy {

  @Input() placeholder = '';

  @Input() control: AbstractControl;

  @Input() name: string;

  @Input() resize: boolean;

  @Input() size  = 4;

  @Input() multiLine: boolean;

  protected changeSubscription: Subscription;

  protected content: string;

  ngOnInit(): void {
    super.ngOnInit();

    if (this.multiLine) {
      this.changeSubscription = this.control.valueChanges.subscribe(this.resizeTextarea);
    }
  }

  ngOnDestroy(): void {
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }

  public resizeTextarea = (value: string) => {
    this.content = value;
  }
}
