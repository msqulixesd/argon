import { AutocompleteComponent } from '../Autocomplete/AutocompleteComponent';
import { filter, isArray } from 'lodash';
import { Component, Input, TemplateRef } from '@angular/core';
import { SelectOptionInterface } from '../../interfaces/SelectOptionInterface';
import { AbstractControl } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'ar-autocomplete-list',
  templateUrl: '../Autocomplete/AutocompleteComponent.html',
  styleUrls: ['../Autocomplete/AutocompleteComponent.scss']
})
export class AutocompleteListComponent extends AutocompleteComponent {

  @Input() placeholder = '';

  @Input() control: AbstractControl;

  @Input() name: string;

  @Input() noResultText = 'Nothing found';

  @Input() keyUpDelay = 500;

  @Input() set options(val: Array<SelectOptionInterface> | Observable<Array<SelectOptionInterface>>) {
    if (!val) {
      this._options = of([]);
    } else if (isArray(val)) {
      this._options = of(val);
    } else {
      this._options = val;
    }
  }

  @Input() optionTemplate: TemplateRef<any>;

  @Input() defaultOption: SelectOptionInterface;

  @Input() dropUp: boolean;

  protected _options: Observable<Array<SelectOptionInterface>> = of([]);

  protected search(query: string): Observable<Array<SelectOptionInterface>> {
    return this._options.pipe(
      map(
        (options: Array<SelectOptionInterface>) => {
          if (!query) {
            return options;
          }

          return filter(options,
            (option: SelectOptionInterface) => String(option.label).indexOf(query) >= 0
          );
        }
      )
    );
  }

}
