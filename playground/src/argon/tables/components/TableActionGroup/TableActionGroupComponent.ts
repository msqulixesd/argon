import { Component } from '@angular/core';

@Component({
  selector: 'ar-table-action-group',
  template: `
    <div class="d-inline-flex ar-table__action-group">
      <ng-content></ng-content>
    </div>
  `
})
export class TableActionGroupComponent {

}
