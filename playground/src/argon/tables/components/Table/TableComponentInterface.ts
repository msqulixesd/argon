import { EventEmitter } from '@angular/core';
import { TableSortOrderInterface } from '../../interfaces/TableSortOrderInterface';

export interface TableComponentInterface {
  // default order key
  defaultOrderBy: string;
  // default order direction
  defaultOrderDirection: 'asc' | 'desc';
  // bs table property
  striped: boolean;

  dark: boolean;
  // bs table property
  bordered: boolean;
  // bs table property
  hover: boolean;
  // bs table property
  responsive: boolean;
  // Header column click emitter
  order: EventEmitter<TableSortOrderInterface>;
  // Simulate header click event
  headerClick(key: string);
  // Set current order key and direction
  setOrder(key: string, direction: 'asc' | 'desc');
  // is key equal current sort order key
  isOrderKey(key: string): boolean;
  // sort direction flag
  isAscDirection(): boolean;
  // sort direction flag
  isDescDirection(): boolean;

}
