import { find, size, each } from 'lodash';
import { Injectable } from '@angular/core';
import { SwitchableCellInterface } from '../../interfaces/SwitchableCellInterface';

@Injectable()
export class SwitchableCellService {

  public get cells(): Array<SwitchableCellInterface> { return this._cells; }

  public get hasCells(): boolean { return size(this.cells) > 0; }

  public get cellsMap(): { [key: string]: boolean } {
    return this._map;
  }

  private _cells: Array<SwitchableCellInterface> = [];

  private _map: { [key: string]: boolean } = {};

  public registerCell(cell: SwitchableCellInterface) {
    if (this.find(cell.key)) {
      return;
    }

    this._cells.push(cell);
    this._map = this.createCellsMap();
  }

  public toggleCellsVisibility(keys: Array<string>) {
    each(keys, this.toggleCell);
  }

  public toggleAllCellsVisibility(isVisible: boolean) {
    each(this.cells, (cell: SwitchableCellInterface) => { cell.hidden = !isVisible; });
    this._map = this.createCellsMap();
  }

  public isCellHidden(key: string): boolean {
    const cell = this.find(key);
    return cell ? cell.hidden : false;
  }

  public toggleCell = (key) => {
    const cell = this.find(key);
    if (cell) {
      cell.hidden = !cell.hidden;
      this._map = this.createCellsMap();
    }
  }

  private find(key: string): SwitchableCellInterface {
    return find(this._cells, { key });
  }

  private createCellsMap(): { [key: string]: boolean } {
    const cellsMap = {};
    each(this.cells, (cell: SwitchableCellInterface) => { cellsMap[cell.key] = !cell.hidden; });
    return cellsMap;
  }
}
