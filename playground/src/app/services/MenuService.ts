import { Injectable } from '@angular/core';
import { MenuItemInterface } from '../interfaces/MenuItemInterface';
import { menu } from '../data/menu';

@Injectable()
export class MenuService {

  public getMenu(): Array<MenuItemInterface> {
    return menu;
  }

}
