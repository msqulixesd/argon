import { Injectable } from '@angular/core';
import { range, map } from 'lodash';
import { DictionaryItemInterface } from '../../argon/core/interfaces/DictionaryItemInterface';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable()
export class FakeDictionaryService {

  public createDictionaryArray(count: number = 5): Array<DictionaryItemInterface> {
    return this.createDictionary(count, (value: number) => `From Array Option ${value}`);
  }

  public createDictionaryObservable(count: number = 5): Observable<Array<DictionaryItemInterface>> {
    return of(
      this.createDictionary(count, (value: number) => `From Observable Option ${value}`)
    ).pipe(delay(2000));
  }

  private createDictionary(count: number, createLabel: (id: number) => string): Array<DictionaryItemInterface> {
    return map(
      range(1, count + 1),
      (value: number) => ({
        value,
        label: createLabel(value),
        extendedData: { extendedDataValue: 'Extended Data Value' }
      } as DictionaryItemInterface)
    );
  }
}
