import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { slice, filter, size } from 'lodash';
import { IconService } from '../../../../argon/ui/services/IconService';

@Component({
  selector: 'pg-icons-list-component',
  template: `    
    <ar-block>
      <ar-block *ngIf="hasIcons">
        <ar-button [block]="true" arBsContext="dark" *ngIf="showMoreVisible" (click)="isFullMode = true">Show All</ar-button>
        <ar-button [block]="true" arBsContext="dark" *ngIf="hideVisible" (click)="isFullMode = false">Hide</ar-button>
      </ar-block>
      <section class="icons-layout">
        <ng-container *ngFor="let icon of icons" textAlign="center">
          <ar-figure (click)="selectIcon.emit(icon)">
            <div class="icon-container">
              <div class="icon-content" arBorder="info" [arBorderRounded]="true">
                <ar-icon [name]="icon" arBsSize="xl"></ar-icon>
              </div>
            </div>
            <ng-container caption>
              {{icon}}
            </ng-container>
          </ar-figure>
        </ng-container>
      </section>
      <ar-block *ngIf="hasIcons">
        <ar-button [block]="true" arBsContext="dark" *ngIf="showMoreVisible" (click)="isFullMode = true">Show All</ar-button>
        <ar-button [block]="true" arBsContext="dark" *ngIf="hideVisible" (click)="isFullMode = false">Hide</ar-button>
      </ar-block>
    </ar-block>
  `,
  styles: [`
    .icons-layout {
      display: flex;
      justify-content: space-between;
      flex-wrap: wrap;
    }
    ar-figure {
      margin: 20px 20px 40px 20px;
      display: block;
      width: 100px;
      height: 100px;
      text-align: center;
    }
    .icon-container {
      display: flex;
      justify-content: center;
    }
    .icon-content {
      padding: 20px;
    }
  `],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconsListComponent implements OnInit {

  static SHORT_MODE_COUNT = 30;

  @Input() filter: string;

  @Output() selectIcon = new EventEmitter<string>();

  public get icons(): Array<string> {
    let icons = this._icons;
    if (this.filter) {
      icons = filter(icons, this.isVisible);
    }

    if (this.isFullMode) {
      return icons;
    }

    return slice(icons, 0, IconsListComponent.SHORT_MODE_COUNT);
  }

  public get iconsCount(): number {
    return size(filter(this._icons, this.isVisible));
  }

  public get hasIcons(): boolean {
    return this.iconsCount > 0;
  }

  public isFullMode: boolean;

  private _icons: Array<string>;

  public get showMoreVisible(): boolean { return !this.isFullMode && this.iconsCount > IconsListComponent.SHORT_MODE_COUNT; }
  public get hideVisible(): boolean { return this.isFullMode && this.iconsCount > IconsListComponent.SHORT_MODE_COUNT; }

  constructor(
    private iconService: IconService
  ) { }

  ngOnInit(): void {
    this._icons = this.iconService.getIconsList();
  }

  public isVisible = (icon: string): boolean => {
    if (!this.filter) {
      return true;
    }

    return icon.indexOf(this.filter) >= 0;
  }
}
