import { Component, Input } from '@angular/core';

@Component({
  selector: 'pg-example',
  template: `
    <ar-block>
      <ar-tab-layout>
        <ar-tab title="Example">
          <ng-content></ng-content>
        </ar-tab>
        <ar-tab *ngIf="template" title="Template Source">
          <pg-source-code [source]="template"></pg-source-code>
        </ar-tab>
        <ar-tab *ngIf="code" title="Code Source">
          <pg-source-code [source]="code" lang="typescript"></pg-source-code>
        </ar-tab>
      </ar-tab-layout>
    </ar-block>
  `
})
export class ExampleComponent {

  @Input() template: string;

  @Input() code: string;

}
