import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route } from '@angular/router';
import { RouteLinkInterface } from '../../../interfaces/RouteLinkInterface';
import { map, get } from 'lodash';

@Component({
  selector: 'pg-router-tree',
  template: `
    <ar-block title="List of Contents">
      <ul class="list-inline">
        <li class="list-inline-item" *ngFor="let link of links">
          <ar-inner-link [url]="link.url">{{link.title}}</ar-inner-link>
        </li>
      </ul>
    </ar-block>
  `
})
export class RouterTreeComponent implements OnInit {

  get links(): Array<RouteLinkInterface> {
    return map(this.children, (route: Route) => ({
      url: [this.basePath, route.path],
      title: get(route, 'data.title', route.path)
    } as RouteLinkInterface));
  }

  private basePath: any;
  private children: Array<Route>;

  constructor(
    public activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const parent = this.activatedRoute.parent;

    if (!parent) {
      return;
    }

    const config = parent.routeConfig;
    this.basePath = parent.snapshot.url;
    this.children = config.children;
  }
}
