import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'pg-radio-page-component',
  templateUrl: './RadioPageComponent.html'
})
export class RadioPageComponent {

  public templateSource = require('./RadioPageComponent.html');

  public codeSource = `
export class RadioComponent extends CheckboxComponent {

  @Input() value: any;

  @Input() inline: boolean;

  @Input() control: AbstractControl;

  @Input() name: string;

}  
`;

  public control = new FormControl(null, [Validators.required]);

}
