import { Component } from '@angular/core';

@Component({
  selector: 'pg-input-group-page-component',
  templateUrl: './InputGroupPageComponent.html'
})
export class InputGroupPageComponent {

  public templateSource = require('./InputGroupPageComponent.html');

  public codeSource = `
export class InputGroupComponent {

  @Input() prepend: boolean;

  @Input() append: boolean;

}

export class InputGroupTextComponent {

}  
`;

}
