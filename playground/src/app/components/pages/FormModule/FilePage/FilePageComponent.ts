import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'pg-file-page-component',
  templateUrl: './FilePageComponent.html'
})
export class FilePageComponent {

  public templateSource = require('./FilePageComponent.html');

  public codeSource = `
export class FileComponent extends InputComponent {

  @Input() placeholder = 'Choose file...';

  @Input() control: AbstractControl;

  @Input() name: string;

  @Input() accept: string;
  
}
  `;

  public control = new FormControl(null, [Validators.required]);

}
