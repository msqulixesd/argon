import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'pg-ar-form-page-component',
  templateUrl: './ArFormPageComponent.html'
})
export class ArFormPageComponent {

  public templateSource = require('./ArFormPageComponent.html');

  public codeSource = `
export class FormComponent {

  @Input() inline: boolean;

  @Input() id: string;
  
  @Input() bgContext: string;
}  

export class FormGroupComponent {

  @Input() label: string;

  @Input() showFirstError = true;

  @Input() useTooltip = true;
  
}
`;

  public form = new FormGroup({
    control1: new FormControl(null, [Validators.required]),
    control2: new FormControl(null, [Validators.required]),
    control3: new FormControl(null, [Validators.required])
  });
}
