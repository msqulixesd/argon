import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { SelectOptionInterface } from '../../../../../argon/form/interfaces/SelectOptionInterface';

import { range, map, random } from 'lodash';

@Component({
  selector: 'pg-autocomplete-page-component',
  templateUrl: './AutocompletePageComponent.html'
})
export class AutocompletePageComponent implements OnInit {

  public templateSource = require('./AutocompletePageComponent.html');

  public codeSource = `
export class AutocompleteComponent extends InputComponent {

  @Input() placeholder = '';

  @Input() control: AbstractControl;

  @Input() name: string;

  @Input() noResultText = 'Nothing found';

  @Input() keyUpDelay = 500;

  @Input() searchFunction: (q: string) => Observable<Array<SelectOptionInterface>>;

  @Input() optionTemplate: TemplateRef<any>;

  @Input() defaultOption: SelectOptionInterface;

  @Input() dropUp: boolean;

}


export interface SelectOptionInterface {

  label?: string;

  value: any;

  disabled?: boolean;

  extendedData?: any;
}
  `;

  public control = new FormControl(null, [Validators.required]);

  public lastQuery: string;

  public initialOption: SelectOptionInterface;

  protected options = [];

  ngOnInit(): void {
    this.options = map(range(1, 5000), (i: number): SelectOptionInterface => ({
        label: `Options ${i}`,
        value: i,
        extendedData: {
          message: 'Extended data message'
        }
      })
    );
    const initialIndex = random(0, 49);
    this.initialOption = this.options[initialIndex];
    this.control.setValue(this.initialOption.value);
  }

  public searchFn = (query: string): Observable<Array<SelectOptionInterface>> => {
    this.lastQuery = query;

    return of(this.options).pipe(delay(1000));
  }

}
