import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { SelectOptionInterface } from '../../../../../argon/form/interfaces/SelectOptionInterface';

@Component({
  selector: 'pg-select-multiple-page-component',
  templateUrl: './SelectMultiplePageComponent.html'
})
export class SelectMultiplePageComponent {

  public templateSource = require('./SelectMultiplePageComponent.html');

  public codeSource = `
export class SelectMultipleComponent extends SelectComponent {

  @Input() size = 4;

  @Input() options: Array<SelectOptionInterface> = [];

  @Input() public list: Array<string>;

  @Input() control: AbstractControl;

  @Input() name: string;

}

export interface SelectOptionInterface {

  label?: string;

  value: any;

  disabled?: boolean;

  extendedData?: any;
}
`;

  public control = new FormControl(null, [Validators.required]);

  public options: Array<SelectOptionInterface> = [
    { label: 'Option 1', value: 1 },
    { label: 'Option 2', value: 2 },
    { label: 'Option 3', value: 3, disabled: true },
    { label: 'Option 4', value: 4 },
  ];
}
