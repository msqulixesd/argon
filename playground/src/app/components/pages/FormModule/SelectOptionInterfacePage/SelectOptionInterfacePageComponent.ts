import { Component } from '@angular/core';

@Component({
  selector: 'pg-select-option-interface-page-component',
  templateUrl: './SelectOptionInterfacePageComponent.html'
})
export class SelectOptionInterfacePageComponent {

  public templateSource = require('./SelectOptionInterfacePageComponent.html');

  public codeSource = require('!!raw-loader!../../../../../argon/form/interfaces/SelectOptionInterface');

}
