import { Component } from '@angular/core';
import { range } from 'lodash';

@Component({
  selector: 'pg-table-appearance-page-component',
  templateUrl: './TableAppearancePageComponent.html'
})
export class TableAppearancePageComponent {

  public templateSource = require('./TableAppearancePageComponent.html');

  public codeSource = `
export class TableComponent {

  static ASC = 'asc';

  static DESC = 'desc';

  @Input() defaultOrderBy: string;

  @Input() defaultOrderDirection: 'asc' | 'desc' = 'asc';

  @Input() striped: boolean;

  @Input() dark: boolean;

  @Input() bordered: boolean;

  @Input() hover: boolean;

  @Input() responsive: boolean;
  
  @Input() scrollable: boolean;

  @Output() order = new EventEmitter<TableSortOrderInterface>();
  
}

export interface TableSortOrderInterface {

  orderBy: string;

  orderDirection: 'asc' | 'desc';

}

  `;

  public tableValues = range(1, 5);

  public tableValuesLarge = range(1, 25);
}
