import { Component } from '@angular/core';

@Component({
  selector: 'pg-table-actions-page-component',
  templateUrl: './TableActionsPageComponent.html'
})
export class TableActionsPageComponent {

  public templateSource = require('./TableActionsPageComponent.html');

  public codeSource = ``;

}
