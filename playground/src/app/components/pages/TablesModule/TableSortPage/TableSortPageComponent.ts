import { Component } from '@angular/core';
import { TableSortOrderInterface } from '../../../../../argon/tables/interfaces/TableSortOrderInterface';

@Component({
  selector: 'pg-table-sort-page-component',
  templateUrl: './TableSortPageComponent.html'
})
export class TableSortPageComponent {

  public templateSource = require('./TableSortPageComponent.html');

  public codeSource = `
export class TableComponent {

  static ASC = 'asc';

  static DESC = 'desc';

  @Input() defaultOrderBy: string;

  @Input() defaultOrderDirection: 'asc' | 'desc' = 'asc';

  @Input() striped: boolean;

  @Input() dark: boolean;

  @Input() bordered: boolean;

  @Input() hover: boolean;

  @Input() responsive: boolean;

  @Input() scrollable: boolean;

  @Output() order = new EventEmitter<TableSortOrderInterface>();
  
}

export interface TableSortOrderInterface {

  orderBy: string;

  orderDirection: 'asc' | 'desc';

}
  `;

  public currentOrder = {
    orderDirection: 'desc',
    orderBy: 'header3'
  } as TableSortOrderInterface;
}
