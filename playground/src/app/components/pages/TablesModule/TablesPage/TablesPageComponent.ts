import { Component } from '@angular/core';

@Component({
  selector: 'pg-tables-page-component',
  templateUrl: './TablesPageComponent.html'
})
export class TablesPageComponent {

  public templateSource = `
<!-- To Start Working with UI module don't forget wrap content wit ar-layout component -->
<ar-layout>
  <!-- Any app content there -->
</ar-layout>
`;

}
