import { LargeTablePageComponent } from './LargeTablePage/LargeTablePageComponent';
import { TableActionsPageComponent } from './TableActionsPage/TableActionsPageComponent';
import { TableSortPageComponent } from './TableSortPage/TableSortPageComponent';
import { TableAppearancePageComponent } from './TableAppearancePage/TableAppearancePageComponent';
import { TablesPageComponent } from './TablesPage/TablesPageComponent';

export const TABLES_MODULE = [
  TablesPageComponent,
  TableAppearancePageComponent,
  TableSortPageComponent,
  TableActionsPageComponent,
  LargeTablePageComponent,
];
