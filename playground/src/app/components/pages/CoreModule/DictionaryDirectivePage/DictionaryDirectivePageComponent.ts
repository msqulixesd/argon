import { Component, OnDestroy, OnInit } from '@angular/core';
import { FakeDictionaryService } from '../../../../services/FakeDictionaryService';
import { DictionaryService } from '../../../../../argon/core/services/DictionaryService';
import { Subscription } from 'rxjs/index';
import { DictionaryItemInterface } from '../../../../../argon/core/interfaces/DictionaryItemInterface';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'pg-dictionary-directive-page-component',
  templateUrl: './DictionaryDirectivePageComponent.html'
})
export class DictionaryDirectivePageComponent implements OnInit, OnDestroy {

  public templateSource = require('./DictionaryDirectivePageComponent.html');

  public codeSource = ``;

  public dictionary: Array<DictionaryItemInterface>;

  public subscription: Subscription;

  public valueControl = new FormControl();

  constructor(
    public dictionaryService: DictionaryService,
    private fakeDictionaryService: FakeDictionaryService
  ) { }

  ngOnInit(): void {
    this.dictionaryService.add('TEST', this.fakeDictionaryService.createDictionaryArray(7));
    this.subscription = this.dictionaryService.get('TEST').subscribe((dic: Array<DictionaryItemInterface>) => {
      this.dictionary = dic;
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
