import { Component, OnInit } from '@angular/core';
import { FakeDictionaryService } from '../../../../services/FakeDictionaryService';
import { DictionaryService } from '../../../../../argon/core/services/DictionaryService';

@Component({
  selector: 'pg-pipes-page-component',
  templateUrl: './PipesPageComponent.html'
})
export class PipesPageComponent implements OnInit {

  public templateSource = require('./PipesPageComponent.html');

  public codeSource = ``;

  public dictionaryPipeValue = 6;

  public dictionaryName = 'FakeDictionary';

  public trimValue = '--__Value__--  ';

  public truncateValue = 'This very long string which will be used for demonstrate arTuncate Pipe.';

  constructor(
    private fakeDictionaryService: FakeDictionaryService,
    private dictionaryService: DictionaryService
  ) { }

  ngOnInit(): void {
    this.dictionaryService.add(this.dictionaryName, this.fakeDictionaryService.createDictionaryArray(9));
  }
}
