import { Component } from '@angular/core';
import { BsSizeArray } from '../../../../../argon/ui/types/BsSizeEnumType';
import { BsContextArray } from '../../../../../argon/ui/types/BsContextEnumType';
import { BsTextContextArray } from '../../../../../argon/ui/types/BsTextEnumType';
import { TextAlignArray } from '../../../../../argon/ui/types/TextAlignEnum';

@Component({
  selector: 'pg-bsgroup-page-component',
  templateUrl: './BsGroupPageComponent.html'
})
export class BsGroupPageComponent {

  public templateSource = require('./BsGroupPageComponent.html');

  public codeSource = `
export class BsGroupComponent {

  @Input() bsSize: string;

  @Input() bsContext: string;

  @Input() bsBgContext: string;

  @Input() bsTextContext: string;

  @Input() textAlign: string;

}
  `;

  public bsSize = BsSizeArray;

  public bsContext = BsContextArray;

  public bsTextContext = BsTextContextArray;

  public bsTextAlign = TextAlignArray;

}
