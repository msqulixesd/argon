import { Component} from '@angular/core';
import { BsSizeArray } from '../../../../../argon/ui/types/BsSizeEnumType';
import { BsContextArray } from '../../../../../argon/ui/types/BsContextEnumType';

@Component({
  selector: 'pg-dropdownpagecomponent',
  templateUrl: './DropdownPageComponent.html'
})
export class DropdownPageComponent {

  public templateSource = require('./DropdownPageComponent.html');

  public codeSource = `
export class DropdownComponent {

  @Input() disabled: boolean;

  @Input() fullWidth: boolean;
  
}

export class DropdownMenuComponent extends DropdownComponent {

  @Input() disabled: boolean;

  @Input() fullWidth: boolean;

}

export class DropdownMenuItemComponent {

  @Input() disabled: boolean;

  @Input() closeOnSelect = true;
  
}
`;

  public bsSize = BsSizeArray;

  public bsContext = BsContextArray;

  public dropdownMenuContent = 'Event Status';

}
