import { Component } from '@angular/core';

@Component({
  selector: 'pg-ui-page-component',
  templateUrl: './UiPageComponent.html'
})
export class UiPageComponent {

  public templateSource = `
<!-- To Start Working with UI module don't forget wrap content wit ar-layout component -->
<ar-layout>
  <!-- Any app content there -->
</ar-layout>
`;

}
