import { Component } from '@angular/core';

@Component({
  selector: 'pg-grid-page-component',
  templateUrl: './GridPageComponent.html'
})
export class GridPageComponent {

  public templateSource = require('./GridPageComponent.html');

  public codeSource = ``;

}
