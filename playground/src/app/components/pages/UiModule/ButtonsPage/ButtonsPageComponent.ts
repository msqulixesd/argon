import { Component } from '@angular/core';
import { BsSizeArray } from '../../../../../argon/ui/types/BsSizeEnumType';
import { BsContextArray } from '../../../../../argon/ui/types/BsContextEnumType';

@Component({
  selector: 'pg-buttons-page-component',
  templateUrl: './ButtonsPageComponent.html'
})
export class ButtonsPageComponent {

  public templateSource = require('./ButtonsPageComponent.html');

  public codeSource = `
export class ButtonComponent {

  @Input() disabled: boolean;

  @Input() block: boolean;

  @Output() click = new EventEmitter<void>();
  
}

export class ButtonGroupComponent {

  @Input() vertical: boolean;
  
}
  `;

  public bsSize = BsSizeArray;

  public bsContext = BsContextArray;

  public buttonClickState = 'Click Me';

  public buttonClickState2 = 'Not Clickable';
}
