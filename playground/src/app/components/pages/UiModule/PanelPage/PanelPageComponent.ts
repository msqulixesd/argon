import { Component } from '@angular/core';
import { BsSizeArray } from '../../../../../argon/ui/types/BsSizeEnumType';
import { BsContextArray } from '../../../../../argon/ui/types/BsContextEnumType';

@Component({
  selector: 'pg-panel-page-component',
  templateUrl: './PanelPageComponent.html'
})
export class PanelPageComponent {

  public templateSource = require('./PanelPageComponent.html');

  public codeSource = ``;

  public bsSize = BsSizeArray;

  public bsContext = BsContextArray;

}
