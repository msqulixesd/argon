import { Component } from '@angular/core';
import { BsSizeArray } from '../../../../../argon/ui/types/BsSizeEnumType';
import { BsContextArray } from '../../../../../argon/ui/types/BsContextEnumType';

@Component({
  selector: 'pg-navbar-page-component',
  templateUrl: './NavBarPageComponent.html'
})
export class NavBarPageComponent {

  public templateSource = require('./NavBarPageComponent.html');

  public codeSource = `
export class NavBarComponent {

}
`;

  public bsSize = BsSizeArray;

  public bsContext = BsContextArray;

}
