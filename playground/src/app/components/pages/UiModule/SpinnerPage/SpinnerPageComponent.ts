import { Component } from '@angular/core';
import { BsSizeArray } from '../../../../../argon/ui/types/BsSizeEnumType';
import { BsContextArray } from '../../../../../argon/ui/types/BsContextEnumType';

@Component({
  selector: 'pg-spinner-page-component',
  templateUrl: './SpinnerPageComponent.html'
})
export class SpinnerPageComponent {

  public templateSource = require('./SpinnerPageComponent.html');

  public codeSource = `
export class SpinnerComponent {

  @Input() visible: boolean;

  @Input() invisible: boolean;
  
} 
`;

  public bsSize = BsSizeArray;

  public bsContext = BsContextArray;

  public state: boolean;

}
