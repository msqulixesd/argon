import { Route, RouterModule } from '@angular/router';
import { HomePageComponent } from './components/pages/HomePage/HomePageComponent';
import { IconsPageComponent } from './components/pages/UiModule/IconsPage/IconsPageComponent';
import { ButtonsPageComponent } from './components/pages/UiModule/ButtonsPage/ButtonsPageComponent';
import { DropdownPageComponent } from './components/pages/UiModule/DropdownPage/DropdownPageComponent';
import { TabPageComponent } from './components/pages/UiModule/TabPage/TabPageComponent';
import { BsGroupPageComponent } from './components/pages/UiModule/BsGroupPage/BsGroupPageComponent';
import { NavBarPageComponent } from './components/pages/UiModule/NavBarPage/NavBarPageComponent';
import { OtherPageComponent } from './components/pages/UiModule/OtherPage/OtherPageComponent';
import { UiPageComponent } from './components/pages/UiModule/UiPage/UiPageComponent';
import { AlertPageComponent } from './components/pages/UiModule/AlertPage/AlertPageComponent';
import { PageLayoutComponent } from './components/layouts/PageLayout/PageLayoutComponent';
import { PagingPageComponent } from './components/pages/UiModule/PagingPage/PagingPageComponent';
import { LinksPageComponent } from './components/pages/UiModule/LinksPage/LinksPageComponent';
import { ModalPageComponent } from './components/pages/UiModule/ModalPage/ModalPageComponent';
import { PanelPageComponent } from './components/pages/UiModule/PanelPage/PanelPageComponent';
import { DirectivesPageComponent } from './components/pages/UiModule/DirectivesPage/DirectivesPageComponent';
import { CardPageComponent } from './components/pages/UiModule/CardPage/CardPageComponent';
import { ProgressBarPageComponent } from './components/pages/UiModule/ProgressBarPage/ProgressBarPageComponent';
import { SpinnerPageComponent } from './components/pages/UiModule/SpinnerPage/SpinnerPageComponent';
import { TablesPageComponent } from './components/pages/TablesModule/TablesPage/TablesPageComponent';
import { TableAppearancePageComponent } from './components/pages/TablesModule/TableAppearancePage/TableAppearancePageComponent';
import { TableSortPageComponent } from './components/pages/TablesModule/TableSortPage/TableSortPageComponent';
import { TableActionsPageComponent } from './components/pages/TablesModule/TableActionsPage/TableActionsPageComponent';
import { FormPageComponent } from './components/pages/FormModule/FormPage/FormPageComponent';
import { FormOverviewComponent } from './components/pages/FormModule/FormOverview/FormOverviewComponent';
import { FormPageLayoutComponent } from './components/layouts/FormPageLayout/FormPageLayoutComponent';
import { InputPageComponent } from './components/pages/FormModule/InputPage/InputPageComponent';
import { GridPageComponent } from './components/pages/UiModule/GridPage/GridPageComponent';
import { CheckboxPageComponent } from './components/pages/FormModule/CheckboxPage/CheckboxPageComponent';
import { FilePageComponent } from './components/pages/FormModule/FilePage/FilePageComponent';
import { RadioPageComponent } from './components/pages/FormModule/RadioPage/RadioPageComponent';
import { RadioGroupPageComponent } from './components/pages/FormModule/RadioGroupPage/RadioGroupPageComponent';
import {
  SelectOptionInterfacePageComponent
} from './components/pages/FormModule/SelectOptionInterfacePage/SelectOptionInterfacePageComponent';
import { SelectPageComponent } from './components/pages/FormModule/SelectPage/SelectPageComponent';
import { SelectMultiplePageComponent } from './components/pages/FormModule/SelectMultiplePage/SelectMultiplePageComponent';
import { YesNoSelectPageComponent } from './components/pages/FormModule/YesNoSelectPage/YesNoSelectPageComponent';
import { ArFormPageComponent } from './components/pages/FormModule/ArFormPage/ArFormPageComponent';
import { InputGroupPageComponent } from './components/pages/FormModule/InputGroupPage/InputGroupPageComponent';
import { TextareaPageComponent } from './components/pages/FormModule/TextareaPage/TextareaPageComponent';
import { AutocompletePageComponent } from './components/pages/FormModule/AutocompletePage/AutocompletePageComponent';
import { AutocompleteListPageComponent } from './components/pages/FormModule/AutocompleteListPage/AutocompleteListPageComponent';
import { LargeTablePageComponent } from './components/pages/TablesModule/LargeTablePage/LargeTablePageComponent';
import { CorePageComponent } from './components/pages/CoreModule/CorePage/CorePageComponent';
import { DictionaryServicePageComponent } from './components/pages/CoreModule/DictionaryServicePage/DictionaryServicePageComponent';
import { PipesPageComponent } from './components/pages/CoreModule/PipesPage/PipesPageComponent';
import { DictionaryDirectivePageComponent } from './components/pages/CoreModule/DictionaryDirectivePage/DictionaryDirectivePageComponent';
import {
  AutocompleteDictionaryPageComponent
} from './components/pages/FormModule/AutocompleteDictionaryPage/AutocompleteDictionaryPageComponent';
import { MessageBusServicePageComponent } from './components/pages/CoreModule/MessageBusServicePage/MessageBusServicePageComponent';
import { LogServicePageComponent } from './components/pages/CoreModule/LogServicePage/LogServicePageComponent';
import { ScrollDirectivePageComponent } from './components/pages/CoreModule/ScrollDirectivePage/ScrollDirectivePageComponent';
import {
  QueryOptionsBuilderServicePageComponent
} from './components/pages/CoreModule/QueryOptionsBuilderServicePage/QueryOptionsBuilderServicePageComponent';
import {
  ComponentSubscriptionsServiceComponent
} from './components/pages/CoreModule/ComponentSubscriptionsServicePage/ComponentSubscriptionsServiceComponent';
import { ToolsPageComponent } from './components/pages/ToolsModule/ToolsPage/ToolsPageComponent';
import { WizardPageComponent } from './components/pages/ToolsModule/WizardPage/WizardPageComponent';

export const ROUTES = [
  { path: '', component: HomePageComponent } as Route,
  { path: 'core', component: PageLayoutComponent, data: { title: 'Core Module' }, children: [
    { path: '', component: CorePageComponent, pathMatch: 'full' },
    { path: 'dictionary-service', component: DictionaryServicePageComponent, title: 'Dictionary Service' },
    { path: 'pipes', component: PipesPageComponent, title: 'Pipes' },
    { path: 'dictionary-directive', component: DictionaryDirectivePageComponent, title: 'Dictionary Directive' },
    { path: 'message-service', component: MessageBusServicePageComponent, title: 'Message Service' },
    { path: 'log-service', component: LogServicePageComponent, title: 'Message Service' },
    { path: 'query-service', component: QueryOptionsBuilderServicePageComponent, title: 'Query Options Builder Service' },
    { path: 'scroll', component: ScrollDirectivePageComponent, title: 'Scroll' },
    { path: 'subscriptions', component: ComponentSubscriptionsServiceComponent, title: 'Subscription Management' },
  ]} as Route,
  { path: 'ui', component: PageLayoutComponent, data: { title: 'UI Module' }, children: [
    { path: '', component: UiPageComponent, pathMatch: 'full' },
    { path: 'alert', component: AlertPageComponent, data: { title: 'Alerts' } },
    { path: 'icons', component: IconsPageComponent, data: { title: 'Icons' } },
    { path: 'buttons', component: ButtonsPageComponent, data: { title: ' Buttons' } },
    { path: 'dropdowns', component: DropdownPageComponent, data: { title: 'Dropdowns' } },
    { path: 'tabs', component: TabPageComponent, data: { title: 'Tabs' } },
    { path: 'bs-group', component: BsGroupPageComponent, data: { title: 'Groups and Context' } },
    { path: 'navbar', component: NavBarPageComponent, data: { title: 'NavBar' } },
    { path: 'paging', component: PagingPageComponent, data: { title: 'Paging' } },
    { path: 'links', component: LinksPageComponent, data: { title: 'Links' } },
    { path: 'modals', component: ModalPageComponent, data: { title: 'Modals' } },
    { path: 'panel', component: PanelPageComponent, data: { title: 'Panel' } },
    { path: 'card', component: CardPageComponent, data: { title: 'Card' } },
    { path: 'progress-bar', component: ProgressBarPageComponent, data: { title: 'Progress Bar' } },
    { path: 'spinner', component: SpinnerPageComponent, data: { title: 'Spinner' } },
    { path: 'grid', component: GridPageComponent, data: { title: 'Grid' } },
    { path: 'directives', component: DirectivesPageComponent, data: { title: 'Directives' } },
    { path: 'other', component: OtherPageComponent, data: { title: 'Other Components' } },
  ]} as Route,
  { path: 'tables', component: PageLayoutComponent, data: { title: 'Tables Module' }, children: [
    { path: '', component: TablesPageComponent, pathMatch: 'full' },
    { path: 'appearance', component: TableAppearancePageComponent, data: { title: 'Table Appearance'} },
    { path: 'sortable', component: TableSortPageComponent, data: { title: 'Sortable Table'} },
    { path: 'actions', component: TableActionsPageComponent, data: { title: 'Table Actions'} },
    { path: 'large', component: LargeTablePageComponent, data: { title: 'Large Tables'} },
  ]} as Route,
  { path: 'forms', component: FormPageLayoutComponent, data: { title: 'Form Module' }, children: [
    { path: '', component: FormPageComponent, pathMatch: 'full' },
    { path: 'overview', component: FormOverviewComponent, data: { title: 'Form Overview' } },
    { path: 'input', component: InputPageComponent, data: { title: 'Input Control' } },
    { path: 'checkbox', component: CheckboxPageComponent, data: { title: 'Checkbox Control' } },
    { path: 'file', component: FilePageComponent, data: { title: 'File Control' } },
    { path: 'radio', component: RadioPageComponent, data: { title: 'Radio Control' } },
    { path: 'radio-group', component: RadioGroupPageComponent, data: { title: 'Radio Group Control' } },
    { path: 'select-option-interface', component: SelectOptionInterfacePageComponent, data: { title: 'SelectOptionInterface' } },
    { path: 'select', component: SelectPageComponent, data: { title: 'Select Control' } },
    { path: 'select-multiple', component: SelectMultiplePageComponent, data: { title: 'Select Multiple Control' } },
    { path: 'select-yes-no', component: YesNoSelectPageComponent, data: { title: 'Yes/No Select Control' } },
    { path: 'textarea', component: TextareaPageComponent, data: { title: 'Textarea Control' } },
    { path: 'autocomplete', component: AutocompletePageComponent, data: { title: 'Autocomplete Control' } },
    { path: 'autocomplete-list', component: AutocompleteListPageComponent, data: { title: 'Autocomplete List Control' } },
    { path: 'autocomplete-dictionary', component: AutocompleteDictionaryPageComponent, data: { title: 'Autocomplete Dictionary Control' } },
    { path: 'form', component: ArFormPageComponent, data: { title: 'Form Control' } },
    { path: 'input-group', component: InputGroupPageComponent, data: { title: 'Input Group' } },
  ]} as Route,
  { path: 'tools', component: PageLayoutComponent, data: { title: 'Tools Module' }, children: [
    { path: '', component: ToolsPageComponent, pathMatch: 'full' },
    { path: 'wizard', component: WizardPageComponent, data: { title: 'Wizard' } }
  ]} as Route
] as Array<Route>;

export const router = RouterModule.forRoot(ROUTES);
