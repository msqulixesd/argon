import { DictionaryItemInterface } from '../interfaces/DictionaryItemInterface';
import { BehaviorSubject, Observable } from 'rxjs';
import { LogService } from './LogService';
export declare type DictionaryType = Array<DictionaryItemInterface>;
export declare type ObservableDictionaryType = Observable<Array<DictionaryItemInterface>>;
export declare type DictionaryInputType = DictionaryType | ObservableDictionaryType;
export declare class DictionaryService {
    protected logService: LogService;
    dictionaries: Map<string, BehaviorSubject<Observable<DictionaryItemInterface[]>>>;
    constructor(logService: LogService);
    add(key: string, dictionary: DictionaryInputType): void;
    get(key: string): ObservableDictionaryType;
    remove(key: string): void;
}
