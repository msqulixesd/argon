import { MessageBusService } from './MessageBusService';
import { AlertPropsInterface } from '../interfaces/AlertPropsInterface';
export declare class AlertService {
    private messageService;
    static TYPES: {
        INFO: string;
        PRIMARY: string;
        SUCCESS: string;
        WARNING: string;
        DANGER: string;
        DEFAULT: string;
        SECONDARY: string;
        LIGHT: string;
        DARK: string;
        LINK: string;
    };
    constructor(messageService: MessageBusService);
    showAlert(props: AlertPropsInterface): void;
    private createMessage(props);
}
