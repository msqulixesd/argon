/// <reference types="qs" />
import { HttpParams } from '@angular/common/http';
import { IParseOptions, IStringifyOptions } from 'qs';
export declare class QueryOptionsBuilderService {
    createHttpParams<T>(queryParams?: T, options?: IStringifyOptions): HttpParams;
    parse<T>(source: string, options?: IParseOptions): T;
    stringify<T>(object: T, options?: IStringifyOptions): string;
}
