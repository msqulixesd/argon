export declare class LogService {
    log(...args: any[]): void;
    warn(...args: any[]): void;
}
