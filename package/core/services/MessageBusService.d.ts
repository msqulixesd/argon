import { Observable } from 'rxjs';
import { MessageInterface } from '../interfaces/MessageInterface';
import { LogService } from './LogService';
export declare class MessageBusService {
    protected logService: LogService;
    private bus;
    constructor(logService: LogService);
    of<T>(messageClass: any): Observable<T>;
    send(message: MessageInterface): void;
}
