import { LogService } from '../services/LogService';
import { DictionaryService } from '../services/DictionaryService';
export declare const dictionaryServiceFactory: (logService: LogService) => DictionaryService;
