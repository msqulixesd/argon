import { PipeTransform } from '@angular/core';
import { DictionaryService } from '../services/DictionaryService';
import { Observable } from 'rxjs';
export declare class DictionaryPipe implements PipeTransform {
    protected dictionaryService: DictionaryService;
    constructor(dictionaryService: DictionaryService);
    transform(value: any, key: string): Observable<string>;
}
