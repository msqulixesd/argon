import { PipeTransform } from '@angular/core';
export declare class TruncatePipe implements PipeTransform {
    transform(value: any, length?: number): any;
}
