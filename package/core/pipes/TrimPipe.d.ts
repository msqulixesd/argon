import { PipeTransform } from '@angular/core';
export declare class TrimPipe implements PipeTransform {
    static MODE_START: string;
    static MODE_END: string;
    transform(value: any, chars?: string, mode?: string): any;
}
