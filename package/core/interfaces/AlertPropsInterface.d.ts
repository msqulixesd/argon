import { TemplateRef } from '@angular/core';
export interface AlertPropsInterface {
    message?: string;
    bsContext?: string;
    closeable?: boolean;
    displayTime?: number;
    template?: TemplateRef<any>;
    templateContext?: any;
}
