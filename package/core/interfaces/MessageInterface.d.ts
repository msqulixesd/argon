export interface MessageInterface {
    type: string;
    isMessageType(messageClass: any): boolean;
}
