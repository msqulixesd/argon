import { ElementRef, RendererFactory2 } from '@angular/core';
export declare class IconService {
    private rendererFactory2;
    private renderer;
    private sprite;
    private icons;
    constructor(rendererFactory2: RendererFactory2);
    initialize(element: ElementRef): void;
    add: (path: string, id: string, viewBox?: string) => void;
    addMany(data: {
        [key: string]: string;
    }): void;
    getIconsList(): Array<string>;
}
