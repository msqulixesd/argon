import { TemplateRef, ViewContainerRef } from '@angular/core';
import { ModalLayoutViewModel } from '../components/ModalLayout/ModalLayoutViewModel';
import { ModalDialogConfigModel } from '../models/ModalDialogConfigModel';
import { LogService } from '../../core/services/LogService';
export declare class ModalService {
    protected logService: LogService;
    private layoutViewModel;
    constructor(logService: LogService);
    showModal(template: TemplateRef<any>, config: ModalDialogConfigModel, context?: any): void;
    hideModal(): void;
    registerLayout(viewContainer: ViewContainerRef): ModalLayoutViewModel;
    unregisterLayout(): void;
    private createViewModel(viewContainer);
    private checkLayout();
}
