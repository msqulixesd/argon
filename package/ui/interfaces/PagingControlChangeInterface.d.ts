export interface PagingControlChangeInterface {
    pageSize: number;
    page: number;
}
