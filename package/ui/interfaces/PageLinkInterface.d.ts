export interface PageLinkInterface {
    page: number;
    title: string;
    disabled: boolean;
    common: boolean;
}
