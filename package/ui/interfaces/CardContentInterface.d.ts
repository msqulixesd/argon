export interface CardContentInterface {
    title?: string;
    subtitle?: string;
    text?: string;
}
