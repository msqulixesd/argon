import { PagingControlChangeInterface } from './PagingControlChangeInterface';
export interface PagingInterface {
    getPagesCount(): number;
    getStartRecord(): number;
    getEndRecord(): number;
    getTotalCount(): number;
    getPageSize(): number;
    getPage(): number;
    setOrder(orderBy: string, orderDirection: 'asc' | 'desc'): void;
    setPageSize(size: number): void;
    applyChanges(changes: PagingControlChangeInterface): void;
}
