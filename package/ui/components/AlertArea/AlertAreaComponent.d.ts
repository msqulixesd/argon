import { OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { AlertsAreaService } from '../../services/AlertsAreaService';
import { MessageBusService } from '../../../core/services/MessageBusService';
export declare class AlertAreaComponent implements OnInit, OnDestroy {
    private messageService;
    alertService: AlertsAreaService;
    alertContainer: ViewContainerRef;
    alertTemplate: TemplateRef<any>;
    private alertSubscription;
    constructor(messageService: MessageBusService, alertService: AlertsAreaService);
    ngOnInit(): void;
    ngOnDestroy(): void;
}
