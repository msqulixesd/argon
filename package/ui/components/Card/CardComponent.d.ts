import { EventEmitter, OnInit } from '@angular/core';
import { ImageInterface } from '../../interfaces/ImageInterface';
import { CardContentInterface } from '../../interfaces/CardContentInterface';
import { ToggleService } from '../../services/ToggleService';
import { ContextualService } from '../../services/ContextualService';
export declare class CardComponent implements OnInit {
    toggleService: ToggleService;
    static BASE_CLASS: string;
    image: ImageInterface;
    imageBottom: boolean;
    cardContent: CardContentInterface;
    imageOverlay: boolean;
    showHeader: boolean;
    showFooter: boolean;
    height: number;
    collapsible: boolean;
    defaultCollapsed: boolean;
    change: EventEmitter<boolean>;
    className: boolean;
    readonly hasTopImage: boolean;
    readonly hasBottomImage: boolean;
    constructor(toggleService: ToggleService, contextualService: ContextualService);
    ngOnInit(): void;
    toggle(): void;
}
