import { EventEmitter, OnInit } from '@angular/core';
import { ContextualService } from '../../services/ContextualService';
import { ButtonGroupComponent } from '../ButtonGroup/ButtonGroupComponent';
export declare class ButtonComponent implements OnInit {
    protected contextualService: ContextualService;
    protected buttonGroup: ButtonGroupComponent;
    static BASE_CLASS: string;
    disabled: boolean;
    block: boolean;
    click: EventEmitter<void>;
    className: boolean;
    blockClassName: boolean;
    disabledClassName: boolean;
    role: string;
    attrDisabled: boolean;
    constructor(contextualService: ContextualService, buttonGroup: ButtonGroupComponent);
    ngOnInit(): void;
}
