import { EventEmitter } from '@angular/core';
import { BsContextType } from '../../types/BsContextEnumType';
export declare class PanelComponent {
    bsContext: BsContextType;
    collapsible: boolean;
    showHeader: boolean;
    showFooter: boolean;
    defaultCollapsed: boolean;
    height: number;
    change: EventEmitter<boolean>;
}
