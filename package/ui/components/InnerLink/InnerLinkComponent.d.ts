import { ElementRef, OnInit, Renderer2 } from '@angular/core';
import { BsTextContextType } from '../../types/BsTextEnumType';
import { AlertComponent } from '../Alert/AlertComponent';
import { CardComponent } from '../Card/CardComponent';
export declare class InnerLinkComponent implements OnInit {
    private element;
    private renderer;
    private card;
    private alertComponent;
    url: any;
    disabled: boolean;
    isAlert: boolean;
    isCard: boolean;
    readonly textContext: BsTextContextType;
    readonly isAlertLink: boolean;
    constructor(element: ElementRef, renderer: Renderer2, card: CardComponent, alertComponent: AlertComponent);
    ngOnInit(): void;
}
