export declare class BsGroupComponent {
    bsSize: string;
    bsContext: string;
    bsBgContext: string;
    bsTextContext: string;
    textAlign: string;
}
