import { DropdownComponent } from '../Dropdown/DropdownComponent';
import { DropdownMenuComponent } from '../DropdownMenu/DropdownMenuComponent';
import { ContextualService } from '../../services/ContextualService';
export declare class DropdownMenuItemComponent {
    private dropdown;
    private dropdownMenu;
    contextualService: ContextualService;
    disabled: boolean;
    closeOnSelect: boolean;
    className: boolean;
    disabledClassName: boolean;
    readonly parentDropdown: DropdownComponent;
    constructor(dropdown: DropdownComponent, dropdownMenu: DropdownMenuComponent, contextualService: ContextualService);
    handleClick(e: MouseEvent): void;
}
