import { ChangeDetectorRef, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { LayoutComponent } from '../Layout/LayoutComponent';
import { ToggleService } from '../../services/ToggleService';
export declare class DropdownComponent implements OnInit, OnDestroy {
    protected element: ElementRef;
    protected layout: LayoutComponent;
    protected ref: ChangeDetectorRef;
    toggleService: ToggleService;
    disabled: boolean;
    fullWidth: boolean;
    dropUp: boolean;
    readonly opened: boolean;
    clickSubscription: Subscription;
    constructor(element: ElementRef, layout: LayoutComponent, ref: ChangeDetectorRef, toggleService: ToggleService);
    ngOnInit(): void;
    ngOnDestroy(): void;
    closeHandler: (event: any) => void;
    close(): void;
    toggle(): void;
}
