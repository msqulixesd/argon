import { EventEmitter } from '@angular/core';
export declare class SwitchComponent {
    active: boolean;
    disabled: boolean;
    toggle: EventEmitter<void>;
    clickHandler(): void;
}
