import { ContextualService } from '../../services/ContextualService';
export declare class ButtonGroupComponent {
    protected contextualService: ContextualService;
    vertical: boolean;
    readonly bsContext: string;
    readonly bsContextPrefix: string;
    readonly bsSize: string;
    constructor(contextualService: ContextualService);
}
