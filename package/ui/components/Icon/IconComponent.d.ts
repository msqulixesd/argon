import { ContextualService } from '../../services/ContextualService';
export declare class IconComponent {
    protected contextualService: ContextualService;
    static BASE_CLASS: string;
    readonly svgStyle: any;
    name: string;
    size: number;
    className: boolean;
    readonly link: string;
    constructor(contextualService: ContextualService);
}
