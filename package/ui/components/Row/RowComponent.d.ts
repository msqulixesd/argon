import { OnInit } from '@angular/core';
import { FormComponent } from '../../../form/components/From/FormComponent';
export declare class RowComponent implements OnInit {
    formComponent: FormComponent;
    static ROW_CLASS: string;
    static FORM_ROW_CLASS: string;
    baseClass: boolean;
    formRow: boolean;
    constructor(formComponent: FormComponent);
    ngOnInit(): void;
}
