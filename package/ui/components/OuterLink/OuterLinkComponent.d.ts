import { OnChanges, SimpleChanges } from '@angular/core';
import { InnerLinkComponent } from '../InnerLink/InnerLinkComponent';
export declare class OuterLinkComponent extends InnerLinkComponent implements OnChanges {
    url: any;
    disabled: boolean;
    isAlert: boolean;
    isCard: boolean;
    parsedUrl: any;
    urlString: string;
    ngOnChanges(changes: SimpleChanges): void;
}
