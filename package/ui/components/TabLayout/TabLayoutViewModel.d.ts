import { TabModel } from '../../models/TabModel';
export declare class TabLayoutViewModel {
    readonly hasOneTab: boolean;
    readonly hasTabs: boolean;
    tabs: Array<TabModel>;
    tabToClose: TabModel;
    selectedTab: TabModel;
    addTab(tab: TabModel): void;
    removeTab(tab: TabModel): void;
    selectTab(tab?: TabModel): void;
    exist(tab: TabModel): boolean;
    private getNextTab(tabToDelete);
}
