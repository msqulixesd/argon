import { EventEmitter } from '@angular/core';
import { ContextualService } from '../../services/ContextualService';
export declare class AlertComponent {
    static BASE_CLASS: string;
    closeable: boolean;
    close: EventEmitter<void>;
    className: boolean;
    role: string;
    constructor(contextualService: ContextualService);
}
