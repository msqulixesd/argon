import { ElementRef, OnDestroy, OnInit, ViewContainerRef } from '@angular/core';
import { ModalService } from '../../services/ModalService';
import { ModalLayoutViewModel } from './ModalLayoutViewModel';
export declare class ModalLayoutComponent implements OnInit, OnDestroy {
    private modalService;
    modalContent: ViewContainerRef;
    dialog: ElementRef;
    viewModel: ModalLayoutViewModel;
    constructor(modalService: ModalService);
    ngOnInit(): void;
    ngOnDestroy(): void;
    backdropClick(event: Event): void;
    getDialogClassName(): string;
}
