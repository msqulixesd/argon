import { TemplateRef, ViewContainerRef, ViewRef } from '@angular/core';
import { ModalDialogConfigModel } from '../../models/ModalDialogConfigModel';
export declare class ModalLayoutViewModel {
    container: ViewContainerRef;
    template: TemplateRef<any>;
    viewRef: ViewRef;
    opened: boolean;
    config: ModalDialogConfigModel;
    constructor(viewContainer: ViewContainerRef);
}
