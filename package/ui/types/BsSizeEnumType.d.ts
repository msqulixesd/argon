export declare const BsSizeEnum: {
    XL: string;
    LG: string;
    MD: string;
    XS: string;
    SM: string;
    DEFAULT: string;
};
export declare type BsSizeType = keyof typeof BsSizeEnum;
export declare const BsSizeArray: string[];
