export declare const BsTextContextEnum: {
    INFO: string;
    PRIMARY: string;
    SUCCESS: string;
    WARNING: string;
    DANGER: string;
    DEFAULT: string;
    SECONDARY: string;
    LIGHT: string;
    DARK: string;
    LINK: string;
    WHITE: string;
    MUTED: string;
};
export declare type BsTextContextType = keyof typeof BsTextContextEnum;
export declare const BsTextContextArray: string[];
