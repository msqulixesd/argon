export declare const BsContextEnum: {
    INFO: string;
    PRIMARY: string;
    SUCCESS: string;
    WARNING: string;
    DANGER: string;
    DEFAULT: string;
    SECONDARY: string;
    LIGHT: string;
    DARK: string;
    LINK: string;
};
export declare type BsContextType = keyof typeof BsContextEnum;
export declare const BsContextArray: string[];
