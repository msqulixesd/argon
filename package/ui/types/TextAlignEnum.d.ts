export declare const TextAlignEnum: {
    RIGHT: string;
    LEFT: string;
    CENTER: string;
    SM_LEFT: string;
    SM_RIGHT: string;
    SM_CENTER: string;
    MD_LEFT: string;
    MD_RIGHT: string;
    MD_CENTER: string;
    LG_LEFT: string;
    LG_RIGHT: string;
    LG_CENTER: string;
    XL_LEFT: string;
    XL_RIGHT: string;
    XL_CENTER: string;
};
export declare type TextAlignEnumType = keyof typeof TextAlignEnum;
export declare const TextAlignArray: string[];
