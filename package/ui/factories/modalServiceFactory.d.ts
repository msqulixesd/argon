import { ModalService } from '../services/ModalService';
import { LogService } from '../../core/services/LogService';
export declare const modalServiceFactory: (logService: LogService) => ModalService;
