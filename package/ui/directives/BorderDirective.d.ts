import { ElementRef, OnInit, Renderer2 } from '@angular/core';
export declare class BorderDirective implements OnInit {
    protected renderer2: Renderer2;
    protected elementRef: ElementRef;
    static BORDER_PREFIX: string;
    static ROUNDED_PREFIX: string;
    private currentContext;
    private currentContextClass;
    private rounded;
    arBorder: string;
    arBorderRounded: boolean;
    constructor(renderer2: Renderer2, elementRef: ElementRef);
    ngOnInit(): void;
    protected decorate(context: string): void;
    protected setRounded(isRounded: boolean): void;
}
