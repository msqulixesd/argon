import { ElementRef, OnInit, Renderer2 } from '@angular/core';
export declare class ScrollableDirective implements OnInit {
    protected renderer2: Renderer2;
    protected elementRef: ElementRef;
    static CLASS_NAME: string;
    arScrollable: any;
    private currentHeight;
    constructor(renderer2: Renderer2, elementRef: ElementRef);
    ngOnInit(): void;
}
