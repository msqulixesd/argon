import { ElementRef, Renderer2 } from '@angular/core';
import { BsContextType } from '../types/BsContextEnumType';
import { ClassNameDirective } from './ClassNameDirective';
import { BsGroupComponent } from '../components/BsGroup/BsGroupComponent';
export declare class BsContextDirective extends ClassNameDirective<BsContextType, string> {
    protected bsContextGroup: BsGroupComponent;
    protected parentDirective: BsContextDirective;
    protected prefix: string;
    arBsContextPrefix: string;
    arBsContext: BsContextType;
    constructor(renderer2: Renderer2, elementRef: ElementRef, bsContextGroup: BsGroupComponent, parentDirective: BsContextDirective);
    protected createContextClassName(originalClass: string, contextValue: string): string;
    protected createEnumTypeInstance(context: BsContextType): string;
}
