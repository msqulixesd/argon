import { ElementRef, Renderer2 } from '@angular/core';
import { BsSizeType } from '../types/BsSizeEnumType';
import { ClassNameDirective } from './ClassNameDirective';
import { BsGroupComponent } from '../components/BsGroup/BsGroupComponent';
export declare class BsSizeDirective extends ClassNameDirective<BsSizeType, string> {
    protected bsSizeGroup: BsGroupComponent;
    protected parentDirective: BsSizeDirective;
    arBsSizePrefix: string;
    arBsSize: BsSizeType;
    protected prefix: string;
    constructor(renderer2: Renderer2, elementRef: ElementRef, bsSizeGroup: BsGroupComponent, parentDirective: BsSizeDirective);
    protected createContextClassName(originalClass: string, contextValue: string): string;
    protected createEnumTypeInstance(context: any): string;
}
