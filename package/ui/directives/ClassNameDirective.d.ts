import { ElementRef, OnDestroy, Renderer2 } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
export declare abstract class ClassNameDirective<K, T> implements OnDestroy {
    protected renderer2: Renderer2;
    protected elementRef: ElementRef;
    protected parentDirective: ClassNameDirective<K, T>;
    context: T;
    originalContext: K;
    change: Subject<K>;
    protected parentSubscription: Subscription;
    protected originalClassName: string;
    protected contextClassName: string;
    baseClassName: string;
    constructor(renderer2: Renderer2, elementRef: ElementRef, parentDirective: ClassNameDirective<K, T>);
    ngOnDestroy(): void;
    protected subscribeToParent(): void;
    protected unsubscribeFromParent(): void;
    protected decorate: (context: K) => void;
    protected abstract createContextClassName(originalClass: string, contextValue: T): string;
    protected abstract createEnumTypeInstance(context: K): T;
}
