import { ElementRef, Renderer2 } from '@angular/core';
import { BsContextType } from '../types/BsContextEnumType';
import { BsGroupComponent } from '../components/BsGroup/BsGroupComponent';
export declare class BsBgContextDirective {
    protected renderer2: Renderer2;
    protected elementRef: ElementRef;
    protected bsContextGroup: BsGroupComponent;
    static BG_PREFIX: string;
    static COLOR_MAP: {
        danger: string;
        dark: string;
        'default': string;
        light: string;
        info: string;
        link: string;
        primary: string;
        secondary: string;
        warning: string;
        success: string;
    };
    static getTextColor(bsContext: string, defaultValue?: string): string;
    private currentContext;
    private currentBgClass;
    private currentTextClass;
    arBsBgContext: BsContextType;
    constructor(renderer2: Renderer2, elementRef: ElementRef, bsContextGroup: BsGroupComponent);
    protected decorate(nextContext: BsContextType): void;
    protected getTextClass(bgContext: BsContextType): string;
}
