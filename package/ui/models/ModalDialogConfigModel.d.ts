import { ModalDialogConfigInterface } from '../interfaces/ModalDialogConfigInterface';
export declare class ModalDialogConfigModel implements ModalDialogConfigInterface {
    large: boolean;
    small: boolean;
    customClass: string;
    readonly className: string;
    backdropClickClose: boolean;
    centered: boolean;
    private _classes;
    private toggleClass(className, state);
}
