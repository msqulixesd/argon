import { Pipe, Injectable, isDevMode, Directive, ElementRef, EventEmitter, NgZone, Output, Input, TemplateRef, ViewContainerRef, NgModule, Component, Optional, Renderer2, SkipSelf, Host, Self, ChangeDetectionStrategy, HostBinding, RendererFactory2, HostListener, ViewChild, ViewEncapsulation, ChangeDetectorRef, ComponentFactoryResolver } from '@angular/core';
import { trim, trimStart, trimEnd, values, truncate, map, size, each, slice, reverse, concat, isArray, find, first, get, keys, without, findIndex, range, indexOf, join } from 'lodash';
import { Subject, fromEvent, BehaviorSubject, of } from 'rxjs';
import { filter, map as map$1, debounceTime, shareReplay, mergeMap, delay } from 'rxjs/operators';
import { HttpParams } from '@angular/common/http';
import { parse, stringify } from 'qs';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class TrimPipe {
    /**
     * @param {?} value
     * @param {?=} chars
     * @param {?=} mode
     * @return {?}
     */
    transform(value, chars, mode) {
        switch (mode) {
            case TrimPipe.MODE_START:
                return trimStart(String(value), chars);
            case TrimPipe.MODE_END:
                return trimEnd(String(value), chars);
            default:
                return trim(String(value), chars);
        }
    }
}
TrimPipe.MODE_START = 'start';
TrimPipe.MODE_END = 'end';
TrimPipe.decorators = [
    { type: Pipe, args: [{
                name: 'arTrim'
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class LogService {
    /**
     * @param {...?} args
     * @return {?}
     */
    log(...args) {
        if (isDevMode()) {
            console.log.apply(null, args);
        }
    }
    /**
     * @param {...?} args
     * @return {?}
     */
    warn(...args) {
        if (isDevMode()) {
            console.warn.apply(null, args);
        }
    }
}
LogService.decorators = [
    { type: Injectable },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class MessageBusService {
    /**
     * @param {?} logService
     */
    constructor(logService) {
        this.logService = logService;
        this.bus = new Subject();
    }
    /**
     * @template T
     * @param {?} messageClass
     * @return {?}
     */
    of(messageClass) {
        return this.bus.pipe(filter((message) => message.isMessageType(messageClass)), map$1((message) => /** @type {?} */ (message)));
    }
    /**
     * @param {?} message
     * @return {?}
     */
    send(message) {
        this.logService.log('MessageBusService: send', message);
        this.bus.next(message);
    }
}
MessageBusService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
MessageBusService.ctorParameters = () => [
    { type: LogService, },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
const INFO = 'info';
const PRIMARY = 'primary';
const SECONDARY = 'secondary';
const SUCCESS = 'success';
const WARNING = 'warning';
const DANGER = 'danger';
const DEFAULT = 'default';
const LIGHT = 'light';
const DARK = 'dark';
const LINK = 'link';
const BsContextEnum = { INFO, PRIMARY, SUCCESS, WARNING, DANGER, DEFAULT, SECONDARY, LIGHT, DARK, LINK };
const BsContextArray = values(BsContextEnum);

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class DisplayAlertMessage {
    /**
     * @param {?} message
     * @param {?=} bsContext
     * @param {?=} closeable
     * @param {?=} displayTime
     */
    constructor(message, bsContext = 'info', closeable = true, displayTime = DisplayAlertMessage.DEFAULT_DISPLAY_TIME) {
        this.message = message;
        this.bsContext = bsContext;
        this.closeable = closeable;
        this.displayTime = displayTime;
        this.type = DisplayAlertMessage.type;
    }
    /**
     * @return {?}
     */
    get isSimple() { return !this.template; }
    /**
     * @return {?}
     */
    get shouldAutohide() { return this.displayTime > 0; }
    /**
     * @param {?} messageClass
     * @return {?}
     */
    isMessageType(messageClass) {
        return messageClass.type === this.type;
    }
    /**
     * @param {?} template
     * @param {?=} context
     * @return {?}
     */
    setTemplate(template, context) {
        this.template = template;
        this.templateContext = context;
    }
}
DisplayAlertMessage.DEFAULT_DISPLAY_TIME = 5000;
DisplayAlertMessage.type = 'DisplayAlertMessage';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class AlertService {
    /**
     * @param {?} messageService
     */
    constructor(messageService) {
        this.messageService = messageService;
    }
    /**
     * @param {?} props
     * @return {?}
     */
    showAlert(props) {
        this.messageService.send(this.createMessage(props));
    }
    /**
     * @param {?} props
     * @return {?}
     */
    createMessage(props) {
        const /** @type {?} */ message = new DisplayAlertMessage(props.message, props.bsContext, props.closeable, props.displayTime);
        if (props.template) {
            message.setTemplate(props.template, props.templateContext);
        }
        return message;
    }
}
AlertService.TYPES = BsContextEnum;
AlertService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
AlertService.ctorParameters = () => [
    { type: MessageBusService, },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class TruncatePipe {
    /**
     * @param {?} value
     * @param {?=} length
     * @return {?}
     */
    transform(value, length) {
        if (!length) {
            return value;
        }
        return truncate(String(value), {
            separator: ' ',
            length
        });
    }
}
TruncatePipe.decorators = [
    { type: Pipe, args: [{
                name: 'arTruncate'
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class InfinityScrollDirective {
    /**
     * @param {?} element
     * @param {?} zone
     */
    constructor(element, zone) {
        this.element = element;
        this.zone = zone;
        this.infinityScroll = new EventEmitter();
        this.scrollEvent = new Subject();
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        this.zone.runOutsideAngular(() => {
            fromEvent(this.element.nativeElement, 'scroll')
                .pipe(debounceTime(50))
                .subscribe((event) => {
                const /** @type {?} */ nativeElement = this.element.nativeElement;
                const /** @type {?} */ scrollHeight = nativeElement.scrollHeight - nativeElement.clientHeight;
                const /** @type {?} */ scrollTop = nativeElement.scrollTop;
                const /** @type {?} */ scrollRatio = scrollTop / scrollHeight;
                this.infinityScroll.emit(scrollRatio);
                this.scrollEvent.next(scrollRatio);
            });
        });
    }
}
InfinityScrollDirective.decorators = [
    { type: Directive, args: [{
                selector: '[arInfinityScroll]'
            },] },
];
/** @nocollapse */
InfinityScrollDirective.ctorParameters = () => [
    { type: ElementRef, },
    { type: NgZone, },
];
InfinityScrollDirective.propDecorators = {
    "infinityScroll": [{ type: Output },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ScrollCollectionService {
    /**
     * @param {?} ngZone
     */
    constructor(ngZone) {
        this.ngZone = ngZone;
        this._collection = [];
        this.renderToTop = () => {
            if (this._middleIndex <= this.minIndex || size(this._collection) === 0) {
                return;
            }
            const /** @type {?} */ startIndex = this._middleIndex - this.minIndex - this.offsetSize;
            const /** @type {?} */ itemsToAdd = this._collection.slice(startIndex < 0 ? 0 : startIndex, this._middleIndex - this.minIndex);
            const /** @type {?} */ renderedItems = this._collection.slice(this._middleIndex - this.minIndex, this._middleIndex + this.minIndex);
            const /** @type {?} */ collectionToChange = concat(itemsToAdd, renderedItems);
            const /** @type {?} */ collectionSize = size(collectionToChange);
            for (let /** @type {?} */ i = collectionSize - 1; i >= this.viewElementCount; i--) {
                collectionToChange[i].shouldDetach = true;
            }
            this._middleIndex -= this.offsetSize;
            if (this._middleIndex < this.minIndex) {
                this._middleIndex = this.minIndex;
            }
            this.renderCollection(collectionToChange, true);
            this.containerElement.nativeElement.scrollTop = this.topScrollSize;
        };
        this.renderToBottom = () => {
            if (this._middleIndex >= this.maxIndex || size(this._collection) === 0) {
                return;
            }
            const /** @type {?} */ itemsToAdd = this._collection.slice(this._middleIndex + this.minIndex, this._middleIndex + this.minIndex + this.offsetSize);
            const /** @type {?} */ renderedItems = this._collection.slice(this._middleIndex - this.minIndex, this._middleIndex + this.minIndex);
            const /** @type {?} */ collectionToChange = concat(renderedItems, itemsToAdd);
            const /** @type {?} */ collectionSize = size(collectionToChange);
            for (let /** @type {?} */ i = 0; i < collectionSize - this.viewElementCount; i++) {
                collectionToChange[i].shouldDetach = true;
            }
            this._middleIndex += this.offsetSize;
            if (this._middleIndex > this.maxIndex) {
                this._middleIndex = this.maxIndex;
            }
            this.renderCollection(collectionToChange);
        };
    }
    /**
     * @return {?}
     */
    get viewElementCount() {
        return this.options.viewElementCount || ScrollCollectionService.DEFAULT_VIEW_ELEMENT_COUNT;
    }
    /**
     * @return {?}
     */
    get offsetSize() {
        return this.options.offsetSize || ScrollCollectionService.DEFAULT_OFFSET_SIZE;
    }
    /**
     * @return {?}
     */
    get itemsContainer() {
        return this.options.itemsContainer;
    }
    /**
     * @return {?}
     */
    get template() {
        return this.options.template;
    }
    /**
     * @return {?}
     */
    get containerElement() {
        return this.options.containerElement;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set collection(val) {
        this.destroyCollection(this._collection);
        this._collection = this.createCollection(val);
        if (this.initialized) {
            this.itemsContainer.clear();
            this._middleIndex = this.minIndex;
            const /** @type {?} */ collectionToRender = slice(this._collection, 0, this.viewElementCount);
            this.renderCollection(collectionToRender);
        }
    }
    /**
     * @return {?}
     */
    get collection() {
        return map(this._collection, (item) => item.data);
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set scrollRatio(val) {
        if (val > 0.99) {
            this.ngZone.run(this.renderToBottom);
        }
        else if (val < 0.01) {
            this.ngZone.run(this.renderToTop);
        }
    }
    /**
     * @return {?}
     */
    get minIndex() {
        return Math.ceil(this.viewElementCount / 2);
    }
    /**
     * @return {?}
     */
    get maxIndex() {
        return this.size - this.minIndex;
    }
    /**
     * @return {?}
     */
    get size() {
        return size(this._collection);
    }
    /**
     * @return {?}
     */
    get hasItems() {
        return this.size > 0;
    }
    /**
     * @return {?}
     */
    get initialized() {
        return Boolean(this.options);
    }
    /**
     * @return {?}
     */
    get topScrollSize() {
        const /** @type {?} */ nativeElement = this.containerElement.nativeElement;
        const /** @type {?} */ scrollHeight = nativeElement.scrollHeight - nativeElement.clientHeight;
        return scrollHeight * (this.offsetSize / this.viewElementCount);
    }
    /**
     * @param {?} options
     * @return {?}
     */
    initialize(options) {
        this.options = options;
        this._middleIndex = this.minIndex;
        this.collection = this.options.initialCollection || this.collection;
        this.options.initialCollection = [];
    }
    /**
     * @return {?}
     */
    destroy() {
        this.destroyCollection(this._collection);
    }
    /**
     * @param {?} collection
     * @param {?=} reverseRender
     * @return {?}
     */
    renderCollection(collection, reverseRender = false) {
        const /** @type {?} */ collectionToRender = reverseRender ? reverse(collection) : collection;
        each(collectionToRender, (item) => {
            const /** @type {?} */ index = item.viewRef ? this.itemsContainer.indexOf(item.viewRef) : -1;
            if (item.shouldDetach && index >= 0) {
                this.removeElement(item, index);
            }
            else if (index < 0) {
                this.renderElement(item, reverseRender ? 0 : null);
            }
        });
    }
    /**
     * @param {?} item
     * @param {?=} index
     * @return {?}
     */
    renderElement(item, index) {
        if (item.viewRef) {
            this.itemsContainer.insert(item.viewRef, index);
        }
        else {
            item.viewRef = this.itemsContainer.createEmbeddedView(this.template, { $implicit: item.data }, index);
        }
    }
    /**
     * @param {?} item
     * @param {?} index
     * @return {?}
     */
    removeElement(item, index) {
        item.shouldDetach = false;
        this.itemsContainer.detach(index);
    }
    /**
     * @param {?} collection
     * @return {?}
     */
    destroyCollection(collection) {
        each(collection, (item) => {
            if (item.viewRef && !item.viewRef.destroyed) {
                item.viewRef.destroy();
            }
        });
    }
    /**
     * @param {?} options
     * @return {?}
     */
    createCollection(options) {
        return map(options, (data) => ({ data }));
    }
}
// <editor-fold desc="[Options]">
ScrollCollectionService.DEFAULT_VIEW_ELEMENT_COUNT = 20;
ScrollCollectionService.DEFAULT_OFFSET_SIZE = 5;
ScrollCollectionService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
ScrollCollectionService.ctorParameters = () => [
    { type: NgZone, },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ScrollContainerDirective {
    /**
     * @param {?} container
     * @param {?} template
     * @param {?} scrollService
     * @param {?} scrollDirective
     */
    constructor(container, template, scrollService, scrollDirective) {
        this.container = container;
        this.template = template;
        this.scrollService = scrollService;
        this.scrollDirective = scrollDirective;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set arScrollContainer(val) {
        this.scrollService.collection = val;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (!this.scrollDirective) {
            throw Error('InfinityScrollDirective in parent node required!');
        }
        this.scrollService.initialize({
            template: this.template,
            itemsContainer: this.container,
            containerElement: this.scrollDirective.element,
            viewElementCount: 50,
            offsetSize: 10
        });
        this.scrollSubscription = this.scrollDirective.scrollEvent.subscribe((ratio) => {
            this.scrollService.scrollRatio = ratio;
        });
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.scrollService.destroy();
        if (this.scrollSubscription) {
            this.scrollSubscription.unsubscribe();
        }
    }
}
ScrollContainerDirective.decorators = [
    { type: Directive, args: [{
                selector: '[arScrollContainer]',
                providers: [ScrollCollectionService]
            },] },
];
/** @nocollapse */
ScrollContainerDirective.ctorParameters = () => [
    { type: ViewContainerRef, },
    { type: TemplateRef, },
    { type: ScrollCollectionService, },
    { type: InfinityScrollDirective, },
];
ScrollContainerDirective.propDecorators = {
    "arScrollContainer": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
let messageBusService;
const messageBusServiceFactory = (logService) => {
    if (!messageBusService) {
        messageBusService = new MessageBusService(logService);
    }
    return messageBusService;
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class DictionaryService {
    /**
     * @param {?} logService
     */
    constructor(logService) {
        this.logService = logService;
        this.dictionaries = new Map();
    }
    /**
     * @param {?} key
     * @param {?} dictionary
     * @return {?}
     */
    add(key, dictionary) {
        const /** @type {?} */ hasDictionary = this.dictionaries.has(key);
        const /** @type {?} */ source = isArray(dictionary)
            ? of(/** @type {?} */ (dictionary)).pipe(shareReplay(1))
            : (/** @type {?} */ (dictionary)).pipe(shareReplay(1));
        if (hasDictionary) {
            const /** @type {?} */ dictionarySubject = this.dictionaries.get(key);
            dictionarySubject.next(source);
        }
        else {
            const /** @type {?} */ dictionarySubject = new BehaviorSubject(source);
            this.dictionaries.set(key, dictionarySubject);
        }
    }
    /**
     * @param {?} key
     * @return {?}
     */
    get(key) {
        if (!this.dictionaries.has(key)) {
            this.logService.warn(`DictionaryService: Dictionary ${key} does not exist. Return raw dictionary`);
            this.add(key, []);
        }
        return this.dictionaries.get(key).asObservable()
            .pipe(mergeMap((data) => data));
    }
    /**
     * @param {?} key
     * @return {?}
     */
    remove(key) {
        if (!this.dictionaries.has(key)) {
            this.logService.warn(`DictionaryService: Dictionary ${key} does not exist. Nothing removed.`);
        }
        else {
            this.dictionaries.get(key).next(of([]).pipe(shareReplay(1)));
        }
    }
}
DictionaryService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
DictionaryService.ctorParameters = () => [
    { type: LogService, },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class DictionaryPipe {
    /**
     * @param {?} dictionaryService
     */
    constructor(dictionaryService) {
        this.dictionaryService = dictionaryService;
    }
    /**
     * @param {?} value
     * @param {?} key
     * @return {?}
     */
    transform(value, key) {
        return this.dictionaryService.get(key).pipe(map$1((dictionary) => {
            const /** @type {?} */ item = find(dictionary, { value });
            return item ? item.label : '';
        }));
    }
}
DictionaryPipe.decorators = [
    { type: Pipe, args: [{
                name: 'arDictionary'
            },] },
];
/** @nocollapse */
DictionaryPipe.ctorParameters = () => [
    { type: DictionaryService, },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class DictionaryDirective {
    /**
     * @param {?} container
     * @param {?} template
     * @param {?} dictionaryService
     */
    constructor(container, template, dictionaryService) {
        this.container = container;
        this.template = template;
        this.dictionaryService = dictionaryService;
        this._dictionary = [];
        this.setDictionary = (dictionary) => {
            this._dictionary = dictionary;
            this.updateView();
        };
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set arDictionaryName(value) {
        if (this._currentDictionaryKey === value) {
            return;
        }
        this.subscribeToDictionary(value);
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set arDictionary(value) {
        if (value === this._currentValue) {
            return;
        }
        this._currentValue = value;
        this.updateView();
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.unsubscribeFromDictionary();
    }
    /**
     * @param {?} value
     * @return {?}
     */
    subscribeToDictionary(value) {
        this.unsubscribeFromDictionary();
        this._currentDictionaryKey = value;
        if (!this._currentDictionaryKey) {
            return;
        }
        this._dictionarySubscription = this.dictionaryService.get(this._currentDictionaryKey).subscribe(this.setDictionary);
    }
    /**
     * @return {?}
     */
    unsubscribeFromDictionary() {
        if (this._dictionarySubscription) {
            this._dictionarySubscription.unsubscribe();
        }
        this._dictionarySubscription = null;
    }
    /**
     * @return {?}
     */
    updateView() {
        const /** @type {?} */ nextItem = this.getDictionaryItem();
        // TODO: Implement compare values function
        // if (nextItem && this._currentDictionaryItem && nextItem.value === this._currentDictionaryItem.value) {
        //   return;
        // }
        if (nextItem === this._currentDictionaryItem) {
            return;
        }
        this._currentDictionaryItem = nextItem;
        this.container.clear();
        if (nextItem) {
            this.container.createEmbeddedView(this.template, { $implicit: nextItem });
        }
    }
    /**
     * @return {?}
     */
    getDictionaryItem() {
        return find(this._dictionary, (item) => item.value === this._currentValue);
    }
}
DictionaryDirective.decorators = [
    { type: Directive, args: [{
                selector: '[arDictionary]'
            },] },
];
/** @nocollapse */
DictionaryDirective.ctorParameters = () => [
    { type: ViewContainerRef, },
    { type: TemplateRef, },
    { type: DictionaryService, },
];
DictionaryDirective.propDecorators = {
    "arDictionaryName": [{ type: Input },],
    "arDictionary": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
let dictionaryService;
const dictionaryServiceFactory = (logService) => {
    if (!dictionaryService) {
        dictionaryService = new DictionaryService(logService);
    }
    return dictionaryService;
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class QueryOptionsBuilderService {
    /**
     * @template T
     * @param {?=} queryParams
     * @param {?=} options
     * @return {?}
     */
    createHttpParams(queryParams, options) {
        return new HttpParams(/** @type {?} */ ({
            fromString: this.stringify(queryParams, options)
        }));
    }
    /**
     * @template T
     * @param {?} source
     * @param {?=} options
     * @return {?}
     */
    parse(source, options) {
        return parse(source, options);
    }
    /**
     * @template T
     * @param {?} object
     * @param {?=} options
     * @return {?}
     */
    stringify(object, options) {
        return stringify(object, options);
    }
}
QueryOptionsBuilderService.decorators = [
    { type: Injectable },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
const ɵ0 = messageBusServiceFactory;
const ɵ1 = dictionaryServiceFactory;
class CoreModule {
}
CoreModule.decorators = [
    { type: NgModule, args: [{
                imports: [],
                exports: [
                    TrimPipe,
                    TruncatePipe,
                    DictionaryPipe,
                    InfinityScrollDirective,
                    ScrollContainerDirective,
                    DictionaryDirective,
                ],
                declarations: [
                    TrimPipe,
                    TruncatePipe,
                    DictionaryPipe,
                    InfinityScrollDirective,
                    ScrollContainerDirective,
                    DictionaryDirective,
                ],
                providers: [
                    { provide: MessageBusService, useFactory: ɵ0, deps: [LogService] },
                    { provide: DictionaryService, useFactory: ɵ1, deps: [LogService] },
                    AlertService,
                    LogService,
                    QueryOptionsBuilderService,
                ]
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 * @template K, T
 */
class ClassNameDirective {
    /**
     * @param {?} renderer2
     * @param {?} elementRef
     * @param {?} parentDirective
     */
    constructor(renderer2, elementRef, parentDirective) {
        this.renderer2 = renderer2;
        this.elementRef = elementRef;
        this.parentDirective = parentDirective;
        this.change = new Subject();
        this.decorate = (context) => {
            this.change.next(context);
            this.context = this.createEnumTypeInstance(context);
            this.originalContext = context;
            const /** @type {?} */ nextContextClassName = this.createContextClassName(this.originalClassName, this.context);
            if (String(this.contextClassName) === String(nextContextClassName)) {
                return;
            }
            if (this.contextClassName) {
                this.renderer2.removeClass(this.elementRef.nativeElement, this.contextClassName);
            }
            if (nextContextClassName) {
                this.contextClassName = nextContextClassName;
                this.renderer2.addClass(this.elementRef.nativeElement, this.contextClassName);
            }
        };
        const /** @type {?} */ classList = this.elementRef.nativeElement.classList;
        this.originalClassName = classList ? /** @type {?} */ (first(classList)) : '';
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set baseClassName(val) {
        this.originalClassName = val;
        this.decorate(this.originalContext);
    }
    /**
     * @return {?}
     */
    get baseClassName() {
        return this.originalClassName;
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.unsubscribeFromParent();
    }
    /**
     * @return {?}
     */
    subscribeToParent() {
        if (!this.parentSubscription && this.parentDirective) {
            this.parentSubscription = this.parentDirective.change.subscribe(this.decorate);
        }
    }
    /**
     * @return {?}
     */
    unsubscribeFromParent() {
        if (this.parentSubscription) {
            this.parentSubscription.unsubscribe();
            this.parentSubscription = null;
        }
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class BsGroupComponent {
}
BsGroupComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-bs-group, [arBsGroup]',
                template: `    
    <ng-content></ng-content>`,
            },] },
];
/** @nocollapse */
BsGroupComponent.propDecorators = {
    "bsSize": [{ type: Input },],
    "bsContext": [{ type: Input },],
    "bsBgContext": [{ type: Input },],
    "bsTextContext": [{ type: Input },],
    "textAlign": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class BsContextDirective extends ClassNameDirective {
    /**
     * @param {?} renderer2
     * @param {?} elementRef
     * @param {?} bsContextGroup
     * @param {?} parentDirective
     */
    constructor(renderer2, elementRef, bsContextGroup, parentDirective) {
        super(renderer2, elementRef, parentDirective);
        this.bsContextGroup = bsContextGroup;
        this.parentDirective = parentDirective;
        this.prefix = '';
    }
    /**
     * @param {?} prefix
     * @return {?}
     */
    set arBsContextPrefix(prefix) {
        this.prefix = prefix || '';
        this.decorate(this.originalContext);
    }
    /**
     * @return {?}
     */
    get arBsContextPrefix() {
        return this.prefix;
    }
    /**
     * @param {?} context
     * @return {?}
     */
    set arBsContext(context) {
        let /** @type {?} */ nextContext = context;
        if (!nextContext && this.bsContextGroup) {
            nextContext = /** @type {?} */ (this.bsContextGroup.bsContext);
            this.unsubscribeFromParent();
        }
        else if (!nextContext && this.parentDirective && this.parentDirective.arBsContext) {
            nextContext = this.parentDirective.arBsContext
                ? this.parentDirective.arBsContext
                : /** @type {?} */ ('');
            this.subscribeToParent();
        }
        else {
            this.unsubscribeFromParent();
        }
        this.decorate(nextContext);
    }
    /**
     * @return {?}
     */
    get arBsContext() {
        return this.originalContext;
    }
    /**
     * @param {?} originalClass
     * @param {?} contextValue
     * @return {?}
     */
    createContextClassName(originalClass, contextValue) {
        if (!originalClass) {
            return '';
        }
        return this.prefix
            ? `${originalClass}-${this.prefix}-${contextValue}`
            : `${originalClass}-${contextValue}`;
    }
    /**
     * @param {?} context
     * @return {?}
     */
    createEnumTypeInstance(context) {
        return String(context);
    }
}
BsContextDirective.decorators = [
    { type: Directive, args: [{
                selector: '[arBsContext]'
            },] },
];
/** @nocollapse */
BsContextDirective.ctorParameters = () => [
    { type: Renderer2, },
    { type: ElementRef, },
    { type: BsGroupComponent, decorators: [{ type: Optional },] },
    { type: BsContextDirective, decorators: [{ type: Optional }, { type: SkipSelf },] },
];
BsContextDirective.propDecorators = {
    "arBsContextPrefix": [{ type: Input },],
    "arBsContext": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class BsSizeDirective extends ClassNameDirective {
    /**
     * @param {?} renderer2
     * @param {?} elementRef
     * @param {?} bsSizeGroup
     * @param {?} parentDirective
     */
    constructor(renderer2, elementRef, bsSizeGroup, parentDirective) {
        super(renderer2, elementRef, parentDirective);
        this.bsSizeGroup = bsSizeGroup;
        this.parentDirective = parentDirective;
        this.prefix = '';
    }
    /**
     * @param {?} prefix
     * @return {?}
     */
    set arBsSizePrefix(prefix) {
        this.prefix = prefix || '';
    }
    /**
     * @return {?}
     */
    get arBsSizePrefix() {
        return this.prefix;
    }
    /**
     * @param {?} context
     * @return {?}
     */
    set arBsSize(context) {
        let /** @type {?} */ nextContext = context;
        if (!nextContext && this.bsSizeGroup) {
            nextContext = /** @type {?} */ (this.bsSizeGroup.bsSize);
            this.unsubscribeFromParent();
        }
        else if (!nextContext && this.parentDirective) {
            nextContext = this.parentDirective.arBsSize
                ? this.parentDirective.arBsSize
                : /** @type {?} */ ('');
            this.subscribeToParent();
        }
        else {
            this.unsubscribeFromParent();
        }
        this.decorate(nextContext);
    }
    /**
     * @return {?}
     */
    get arBsSize() {
        return this.originalContext;
    }
    /**
     * @param {?} originalClass
     * @param {?} contextValue
     * @return {?}
     */
    createContextClassName(originalClass, contextValue) {
        if (!originalClass) {
            return '';
        }
        return this.prefix
            ? `${originalClass}-${this.prefix}-${contextValue}`
            : `${originalClass}-${contextValue}`;
    }
    /**
     * @param {?} context
     * @return {?}
     */
    createEnumTypeInstance(context) {
        return String(context);
    }
}
BsSizeDirective.decorators = [
    { type: Directive, args: [{
                selector: '[arBsSize]'
            },] },
];
/** @nocollapse */
BsSizeDirective.ctorParameters = () => [
    { type: Renderer2, },
    { type: ElementRef, },
    { type: BsGroupComponent, decorators: [{ type: Optional },] },
    { type: BsSizeDirective, decorators: [{ type: Optional }, { type: SkipSelf },] },
];
BsSizeDirective.propDecorators = {
    "arBsSizePrefix": [{ type: Input },],
    "arBsSize": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ContextualService {
    /**
     * @param {?} bsContextDirective
     * @param {?} bsSizeDirective
     */
    constructor(bsContextDirective, bsSizeDirective) {
        this.bsContextDirective = bsContextDirective;
        this.bsSizeDirective = bsSizeDirective;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set baseClass(val) {
        this.setBaseClassName(this.bsContextDirective, val);
        this.setBaseClassName(this.bsSizeDirective, val);
    }
    /**
     * @return {?}
     */
    get bsContext() {
        return this.bsContextDirective
            ? this.bsContextDirective.arBsContext
            : '';
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set bsContext(val) {
        if (this.bsContextDirective) {
            this.bsContextDirective.arBsContext = val;
        }
    }
    /**
     * @return {?}
     */
    get bsContextPrefix() {
        return this.bsContextDirective
            ? this.bsContextDirective.arBsContextPrefix
            : '';
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set bsContextPrefix(val) {
        if (this.bsContextDirective) {
            this.bsContextDirective.arBsContextPrefix = val;
        }
    }
    /**
     * @return {?}
     */
    get bsSizePrefix() {
        return this.bsSizeDirective
            ? this.bsSizeDirective.arBsSizePrefix
            : '';
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set bsSizePrefix(val) {
        if (this.bsSizeDirective) {
            this.bsSizeDirective.arBsSizePrefix = val;
        }
    }
    /**
     * @return {?}
     */
    get bsSize() {
        return this.bsSizeDirective
            ? this.bsSizeDirective.arBsSize
            : '';
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set bsSize(val) {
        if (this.bsSizeDirective) {
            this.bsSizeDirective.arBsSize = val;
        }
    }
    /**
     * @param {?} contextualDirective
     * @param {?} className
     * @return {?}
     */
    setBaseClassName(contextualDirective, className) {
        if (contextualDirective) {
            contextualDirective.baseClassName = className;
        }
    }
}
ContextualService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
ContextualService.ctorParameters = () => [
    { type: BsContextDirective, decorators: [{ type: Host }, { type: Self }, { type: Optional },] },
    { type: BsSizeDirective, decorators: [{ type: Host }, { type: Self }, { type: Optional },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ButtonGroupComponent {
    /**
     * @param {?} contextualService
     */
    constructor(contextualService) {
        this.contextualService = contextualService;
    }
    /**
     * @return {?}
     */
    get bsContext() { return this.contextualService.bsContext; }
    /**
     * @return {?}
     */
    get bsContextPrefix() { return this.contextualService.bsContextPrefix; }
    /**
     * @return {?}
     */
    get bsSize() { return this.contextualService.bsSize; }
}
ButtonGroupComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-button-group',
                template: `    
    <div class="btn-toolbar"
         [class.btn-group-vertical]="vertical"
         [class.btn-group]="!vertical"
    >
      <ng-content></ng-content>
    </div>
  `,
                changeDetection: ChangeDetectionStrategy.OnPush,
                providers: [ContextualService],
            },] },
];
/** @nocollapse */
ButtonGroupComponent.ctorParameters = () => [
    { type: ContextualService, },
];
ButtonGroupComponent.propDecorators = {
    "vertical": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ButtonComponent {
    /**
     * @param {?} contextualService
     * @param {?} buttonGroup
     */
    constructor(contextualService, buttonGroup) {
        this.contextualService = contextualService;
        this.buttonGroup = buttonGroup;
        this.click = new EventEmitter();
        this.className = true;
        this.blockClassName = false;
        this.disabledClassName = false;
        this.role = 'button';
        this.attrDisabled = false;
        contextualService.baseClass = ButtonComponent.BASE_CLASS;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set disabled(val) {
        this.attrDisabled = val;
        this.disabledClassName = val;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set block(val) {
        this.blockClassName = val;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.buttonGroup) {
            this.contextualService.bsContext = this.buttonGroup.bsContext;
            this.contextualService.bsSize = this.buttonGroup.bsSize;
            this.contextualService.bsContextPrefix = this.buttonGroup.bsContextPrefix;
        }
    }
}
ButtonComponent.BASE_CLASS = 'btn';
ButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-button, [arButton]',
                template: `
    <ng-content></ng-content>
  `,
                styles: [`:host{pointer-events:auto}:host.disabled{pointer-events:none}`],
                providers: [ContextualService],
                changeDetection: ChangeDetectionStrategy.OnPush
            },] },
];
/** @nocollapse */
ButtonComponent.ctorParameters = () => [
    { type: ContextualService, },
    { type: ButtonGroupComponent, decorators: [{ type: Optional },] },
];
ButtonComponent.propDecorators = {
    "disabled": [{ type: Input },],
    "block": [{ type: Input },],
    "click": [{ type: Output },],
    "className": [{ type: HostBinding, args: ['class.btn',] },],
    "blockClassName": [{ type: HostBinding, args: ['class.btn-block',] },],
    "disabledClassName": [{ type: HostBinding, args: ['class.disabled',] },],
    "role": [{ type: HostBinding, args: ['attr.role',] },],
    "attrDisabled": [{ type: HostBinding, args: ['attr.disabled',] },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class SimpleLayoutComponent {
}
SimpleLayoutComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-simple-layout',
                template: `
    <header>
      <ng-content select="[header]"></ng-content>
    </header>
    <section>
      <ng-content></ng-content>
    </section>
    <footer>
      <ng-content select="[footer]"></ng-content>
    </footer>
  `,
                styles: [`:host{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;min-height:100vh}:host section{padding:10px}:host footer{margin-top:auto}`]
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class BsTextContextDirective {
    /**
     * @param {?} renderer2
     * @param {?} elementRef
     * @param {?} bsContextGroup
     */
    constructor(renderer2, elementRef, bsContextGroup) {
        this.renderer2 = renderer2;
        this.elementRef = elementRef;
        this.bsContextGroup = bsContextGroup;
    }
    /**
     * @param {?} context
     * @return {?}
     */
    static getClassName(context) {
        return `${BsTextContextDirective.TEXT_PREFIX}-${context}`;
    }
    /**
     * @param {?} context
     * @return {?}
     */
    set arBsTextContext(context) {
        let /** @type {?} */ nextContext = context;
        if (this.bsContextGroup && !nextContext) {
            nextContext = /** @type {?} */ (this.bsContextGroup.bsTextContext);
        }
        if (nextContext !== this.currentContext) {
            this.decorate(nextContext);
        }
    }
    /**
     * @param {?} nextContext
     * @return {?}
     */
    decorate(nextContext) {
        this.renderer2.removeClass(this.elementRef.nativeElement, this.currentClass);
        this.currentContext = nextContext;
        this.currentClass = BsTextContextDirective.getClassName(this.currentContext);
        this.renderer2.addClass(this.elementRef.nativeElement, this.currentClass);
    }
}
BsTextContextDirective.TEXT_PREFIX = 'text';
BsTextContextDirective.decorators = [
    { type: Directive, args: [{
                selector: '[arBsTextContext]'
            },] },
];
/** @nocollapse */
BsTextContextDirective.ctorParameters = () => [
    { type: Renderer2, },
    { type: ElementRef, },
    { type: BsGroupComponent, decorators: [{ type: Optional },] },
];
BsTextContextDirective.propDecorators = {
    "arBsTextContext": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
const INFO$1 = 'info';
const PRIMARY$1 = 'primary';
const SECONDARY$1 = 'secondary';
const SUCCESS$1 = 'success';
const WARNING$1 = 'warning';
const DANGER$1 = 'danger';
const DEFAULT$1 = '';
const LIGHT$1 = 'light';
const DARK$1 = 'dark';
const LINK$1 = 'link';
const WHITE = 'white';
const MUTED = 'muted';
const BsTextContextEnum = { INFO: INFO$1, PRIMARY: PRIMARY$1, SUCCESS: SUCCESS$1, WARNING: WARNING$1, DANGER: DANGER$1, DEFAULT: DEFAULT$1, SECONDARY: SECONDARY$1, LIGHT: LIGHT$1, DARK: DARK$1, LINK: LINK$1, WHITE, MUTED };
const BsTextContextArray = values(BsTextContextEnum);

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class BsBgContextDirective {
    /**
     * @param {?} renderer2
     * @param {?} elementRef
     * @param {?} bsContextGroup
     */
    constructor(renderer2, elementRef, bsContextGroup) {
        this.renderer2 = renderer2;
        this.elementRef = elementRef;
        this.bsContextGroup = bsContextGroup;
    }
    /**
     * @param {?} bsContext
     * @param {?=} defaultValue
     * @return {?}
     */
    static getTextColor(bsContext, defaultValue = BsTextContextEnum.DARK) {
        return get(BsBgContextDirective.COLOR_MAP, bsContext, defaultValue);
    }
    /**
     * @param {?} context
     * @return {?}
     */
    set arBsBgContext(context) {
        let /** @type {?} */ nextContext = context;
        if (this.bsContextGroup && !nextContext) {
            nextContext = /** @type {?} */ (this.bsContextGroup.bsBgContext);
        }
        if (nextContext !== this.currentContext) {
            this.decorate(nextContext);
        }
    }
    /**
     * @param {?} nextContext
     * @return {?}
     */
    decorate(nextContext) {
        this.renderer2.removeClass(this.elementRef.nativeElement, this.currentBgClass);
        this.renderer2.removeClass(this.elementRef.nativeElement, this.currentTextClass);
        this.currentContext = nextContext;
        this.currentBgClass = `${BsBgContextDirective.BG_PREFIX}-${this.currentContext}`;
        this.currentTextClass = this.getTextClass(nextContext);
        this.renderer2.addClass(this.elementRef.nativeElement, this.currentBgClass);
        this.renderer2.addClass(this.elementRef.nativeElement, this.currentTextClass);
    }
    /**
     * @param {?} bgContext
     * @return {?}
     */
    getTextClass(bgContext) {
        const /** @type {?} */ context = BsBgContextDirective.getTextColor(bgContext);
        return context
            ? BsTextContextDirective.getClassName(/** @type {?} */ (context))
            : '';
    }
}
BsBgContextDirective.BG_PREFIX = 'bg';
BsBgContextDirective.COLOR_MAP = {
    danger: BsTextContextEnum.WHITE,
    dark: BsTextContextEnum.WHITE,
    'default': BsTextContextEnum.DARK,
    light: BsTextContextEnum.DARK,
    info: BsTextContextEnum.WHITE,
    link: BsTextContextEnum.DARK,
    primary: BsTextContextEnum.WHITE,
    secondary: BsTextContextEnum.WHITE,
    warning: BsTextContextEnum.DARK,
    success: BsTextContextEnum.WHITE,
};
BsBgContextDirective.decorators = [
    { type: Directive, args: [{
                selector: '[arBsBgContext]'
            },] },
];
/** @nocollapse */
BsBgContextDirective.ctorParameters = () => [
    { type: Renderer2, },
    { type: ElementRef, },
    { type: BsGroupComponent, decorators: [{ type: Optional },] },
];
BsBgContextDirective.propDecorators = {
    "arBsBgContext": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ToggleService {
    constructor() {
        this.state = ToggleService.INACTIVE_STATE;
    }
    /**
     * @return {?}
     */
    get boolState() {
        return this.state === ToggleService.ACTIVE_STATE;
    }
    /**
     * @return {?}
     */
    toggle() {
        this.state = this.state === ToggleService.ACTIVE_STATE
            ? ToggleService.INACTIVE_STATE : ToggleService.ACTIVE_STATE;
    }
    /**
     * @return {?}
     */
    open() {
        this.state = ToggleService.ACTIVE_STATE;
    }
    /**
     * @return {?}
     */
    close() {
        this.state = ToggleService.INACTIVE_STATE;
    }
}
ToggleService.ACTIVE_STATE = 'active';
ToggleService.INACTIVE_STATE = 'inactive';
ToggleService.ANIMATION_HEIGHT = /** @type {?} */ (trigger('toggleServiceState', [
    state('active', style({ height: '*', padding: '*' })),
    state('inactive', style({ height: 0, overflow: 'hidden', padding: 0 })),
    transition('active <=> inactive', animate('100ms ease-out')),
    transition(':enter', [
        style({ height: '*' }),
        animate('1000ms ease-out')
    ]),
    transition(':leave', [
        style({ height: 0, overflow: 'hidden' }),
        animate('1000ms ease-out')
    ])
]));
ToggleService.ANIMATION_OPACITY = /** @type {?} */ (trigger('toggleServiceState', [
    state('active', style({ opacity: 1 })),
    state('inactive', style({ opacity: 0 })),
    transition('active <=> inactive', animate('200ms ease-out')),
    transition(':enter', [
        style({ opacity: 0 }),
        animate('200ms ease-in', style({ opacity: 1 }))
    ]),
    transition(':leave', [
        style({ opacity: 1 }),
        animate('200ms ease-out', style({ opacity: 0 }))
    ])
]));
ToggleService.decorators = [
    { type: Injectable },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class NavBarComponent {
    /**
     * @param {?} toggleService
     * @param {?} contextualService
     */
    constructor(toggleService, contextualService) {
        this.toggleService = toggleService;
        this.contextualService = contextualService;
        this.baseClass = true;
        this.contextualService.bsSizePrefix = 'expand';
        this.contextualService.baseClass = NavBarComponent.BASE_CLASS;
        toggleService.open();
    }
    /**
     * @return {?}
     */
    get navBarClass() {
        return '';
        /*switch (this.arBsContext) {
              case BsContextEnum.WARNING:
              case BsContextEnum.SECONDARY:
              case BsContextEnum.PRIMARY:
              case BsContextEnum.DARK:
              case BsContextEnum.DANGER:
              case BsContextEnum.SUCCESS:
                return `${this.baseClass}-${BsContextEnum.DARK}`;
              case BsContextEnum.LINK:
              case BsContextEnum.LIGHT:
              case BsContextEnum.DEFAULT:
              case BsContextEnum.INFO:
              default:
                return `${this.baseClass}-${BsContextEnum.LIGHT}`;
            }*/
    }
}
NavBarComponent.BASE_CLASS = 'navbar';
NavBarComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-nav-bar',
                template: `
    <div class="navbar-brand">
      <ng-content select="[navbar-brand]"></ng-content>
    </div>
    <button class="navbar-toggler" (click)="toggleService.toggle()">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="navbar-collapse" [@toggleServiceState]="toggleService.state">
      <ng-content></ng-content>
      <div class="form-inline">
        <ng-content select="[form-inline]"></ng-content>
      </div>
    </div>
  `,
                animations: [ToggleService.ANIMATION_HEIGHT],
                providers: [ToggleService, ContextualService],
                styles: [`
    .form-inline {
      margin-left: auto;
    }
  `]
            },] },
];
/** @nocollapse */
NavBarComponent.ctorParameters = () => [
    { type: ToggleService, },
    { type: ContextualService, },
];
NavBarComponent.propDecorators = {
    "baseClass": [{ type: HostBinding, args: ['class.navbar',] },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class NavBarNavComponent {
}
NavBarNavComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-nav-bar-nav',
                template: `
    <ul class="navbar-nav">
      <ng-content></ng-content>
    </ul>
  `
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class NavBarNavItemComponent {
}
NavBarNavItemComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-nav-bar-nav-item',
                template: `
    <li class="nav-item">
      <span class="nav-link">
        <ng-content></ng-content>
      </span>
    </li>
  `
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class IconService {
    /**
     * @param {?} rendererFactory2
     */
    constructor(rendererFactory2) {
        this.rendererFactory2 = rendererFactory2;
        this.icons = {};
        this.add = (path, id, viewBox = '0 0 24 24') => {
            if (!this.sprite) {
                this.icons[id] = path;
                return;
            }
            const /** @type {?} */ symbol = this.renderer.createElement('symbol', 'svg');
            this.renderer.setAttribute(symbol, 'id', id);
            this.renderer.setAttribute(symbol, 'viewBox', viewBox);
            const /** @type {?} */ pathElement = this.renderer.createElement('path', 'svg');
            this.renderer.setAttribute(pathElement, 'd', path);
            this.renderer.appendChild(symbol, pathElement);
            this.renderer.appendChild(this.sprite.nativeElement, symbol);
        };
        this.renderer = this.rendererFactory2.createRenderer(null, null);
    }
    /**
     * @param {?} element
     * @return {?}
     */
    initialize(element) {
        this.sprite = element;
        this.addMany(this.icons);
    }
    /**
     * @param {?} data
     * @return {?}
     */
    addMany(data) {
        this.icons = Object.assign({}, this.icons, data);
        if (!this.sprite) {
            return;
        }
        each(data, (path, id) => { this.add(path, id); });
    }
    /**
     * @return {?}
     */
    getIconsList() {
        return keys(this.icons);
    }
}
IconService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
IconService.ctorParameters = () => [
    { type: RendererFactory2, },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class LayoutComponent {
    /**
     * @param {?} iconService
     * @param {?} element
     */
    constructor(iconService, element) {
        this.iconService = iconService;
        this.element = element;
        this.documentClick = new Subject();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.iconService.initialize(this.iconsSprite);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    closeHandler(event) {
        this.documentClick.next(event);
    }
}
LayoutComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-layout',
                template: `
    <section class="argon-layout" [@inOutState]>
      <!-- App content -->
      <ng-content></ng-content>
      <!-- Modal layout -->
      <ar-modal-layout></ar-modal-layout>
      <!-- Alerts -->
      <ar-alert-area></ar-alert-area>
      <!-- Icons -->
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" #iconsSprite style="display: none;"></svg>
    </section>
  `,
                styles: [`body::-webkit-scrollbar-track{-webkit-box-shadow:inset 0 0 6px rgba(0,0,0,.3);background-color:#f8f9fa;border-radius:10px}body::-webkit-scrollbar{width:10px;height:10px;background-color:#f5f5f5;border-radius:10px}body::-webkit-scrollbar-thumb{border-radius:10px;background-image:-webkit-gradient(linear,left bottom,left top,color-stop(.44,#7a8793),color-stop(.72,#56606a),color-stop(.86,#343a40))}.ar-ui-scrollable-block{overflow-y:auto;overflow-x:hidden}.ar-ui-scrollable-block::-webkit-scrollbar-track{-webkit-box-shadow:inset 0 0 6px rgba(0,0,0,.3);background-color:#f8f9fa;border-radius:10px}.ar-ui-scrollable-block::-webkit-scrollbar{width:10px;height:10px;background-color:#f5f5f5;border-radius:10px}.ar-ui-scrollable-block::-webkit-scrollbar-thumb{border-radius:10px;background-image:-webkit-gradient(linear,left bottom,left top,color-stop(.44,#7a8793),color-stop(.72,#56606a),color-stop(.86,#343a40))}.btn+.btn:not(.btn-block){margin-left:.25rem}.btn-toolbar .btn+.btn:not(.btn-block){margin-left:inherit}.ar-form-autocomplete{position:relative}.ar-form-autocomplete .ar-ui-dropdown{position:static}.form-control.disabled{background-color:#e9ecef;opacity:1}`, `.argon-layout{overflow-x:hidden;display:block}.icons-sprite{display:none}`],
                encapsulation: ViewEncapsulation.None,
                animations: [trigger('inOutState', [
                        transition(':enter', [
                            style({ opacity: 0 }),
                            animate('1000ms ease-in', style({ opacity: 1 }))
                        ]),
                        transition(':leave', [
                            style({ opacity: 1 }),
                            animate('1000ms ease-out', style({ opacity: 0 }))
                        ])
                    ])
                ],
            },] },
];
/** @nocollapse */
LayoutComponent.ctorParameters = () => [
    { type: IconService, },
    { type: ElementRef, },
];
LayoutComponent.propDecorators = {
    "iconsSprite": [{ type: ViewChild, args: ['iconsSprite',] },],
    "closeHandler": [{ type: HostListener, args: ['document:click', ['$event'],] },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class DropdownComponent {
    /**
     * @param {?} element
     * @param {?} layout
     * @param {?} ref
     * @param {?} toggleService
     */
    constructor(element, layout, ref, toggleService) {
        this.element = element;
        this.layout = layout;
        this.ref = ref;
        this.toggleService = toggleService;
        this.closeHandler = (event) => {
            if (this.element.nativeElement.contains(event.target)) {
                return;
            }
            if (this.opened) {
                this.close();
                this.ref.markForCheck();
            }
        };
    }
    /**
     * @return {?}
     */
    get opened() {
        return this.toggleService.boolState;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.clickSubscription = this.layout.documentClick.subscribe(this.closeHandler);
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.clickSubscription) {
            this.clickSubscription.unsubscribe();
            this.clickSubscription = null;
        }
    }
    /**
     * @return {?}
     */
    close() {
        this.toggleService.close();
    }
    /**
     * @return {?}
     */
    toggle() {
        if (this.disabled) {
            this.close();
        }
        else {
            this.toggleService.toggle();
        }
    }
}
DropdownComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-dropdown',
                template: `
    <div class="dropdown ar-ui-dropdown" [class.dropup]="dropUp" [class.full-width]="fullWidth">
      <div class="dropdown-menu-selector" (click)="toggle()">
        <ng-content select="[title]"></ng-content>
      </div>
      <div
        *ngIf="opened"
        class="dropdown-menu"
        [@toggleServiceState]="toggleService.state"
      >
        <ng-content></ng-content>
      </div>
    </div>
  `,
                styles: [`.dropdown.full-width .dropdown-menu-selector{display:block}.dropdown.full-width .dropdown-menu{width:100%}.dropdown-menu-selector{cursor:pointer;display:inline-block}.dropdown-menu{display:block;padding:5px;overflow-y:auto}.dropdown-menu::-webkit-scrollbar-track{-webkit-box-shadow:inset 0 0 6px rgba(0,0,0,.3);background-color:#f8f9fa;border-radius:10px}.dropdown-menu::-webkit-scrollbar{width:10px;height:10px;background-color:#f5f5f5;border-radius:10px}.dropdown-menu::-webkit-scrollbar-thumb{border-radius:10px;background-image:-webkit-gradient(linear,left bottom,left top,color-stop(.44,#7a8793),color-stop(.72,#56606a),color-stop(.86,#343a40))}.dropup .dropdown-menu{top:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}`],
                animations: [ToggleService.ANIMATION_OPACITY],
                providers: [ToggleService]
            },] },
];
/** @nocollapse */
DropdownComponent.ctorParameters = () => [
    { type: ElementRef, },
    { type: LayoutComponent, },
    { type: ChangeDetectorRef, },
    { type: ToggleService, },
];
DropdownComponent.propDecorators = {
    "disabled": [{ type: Input },],
    "fullWidth": [{ type: Input },],
    "dropUp": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class DropdownMenuComponent extends DropdownComponent {
    /**
     * @param {?} contextualService
     * @param {?} element
     * @param {?} layout
     * @param {?} ref
     * @param {?} toggleService
     */
    constructor(contextualService, element, layout, ref, toggleService) {
        super(element, layout, ref, toggleService);
        this.contextualService = contextualService;
        this.element = element;
        this.layout = layout;
        this.ref = ref;
        this.toggleService = toggleService;
    }
}
DropdownMenuComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-dropdown-menu',
                template: `
      <div class="dropdown" [class.dropup]="dropUp" [class.full-width]="fullWidth">
        <ar-button class="dropdown-toggle"
                  [arBsContext]="contextualService.bsContext"
                  [arBsSize]="contextualService.bsSize"
                  (click)="toggle()"
        >
          <ng-content select="[title]"></ng-content>
        </ar-button>
        <ul
          class="dropdown-menu"
          [@toggleServiceState]="toggleService.state"
          *ngIf="opened"
        >
          <ng-content></ng-content>
        </ul>
      </div>
  `,
                styles: [`.dropdown.full-width .dropdown-menu-selector{display:block}.dropdown.full-width .dropdown-menu{width:100%}.dropdown-menu-selector{cursor:pointer;display:inline-block}.dropdown-menu{display:block;padding:5px;overflow-y:auto}.dropdown-menu::-webkit-scrollbar-track{-webkit-box-shadow:inset 0 0 6px rgba(0,0,0,.3);background-color:#f8f9fa;border-radius:10px}.dropdown-menu::-webkit-scrollbar{width:10px;height:10px;background-color:#f5f5f5;border-radius:10px}.dropdown-menu::-webkit-scrollbar-thumb{border-radius:10px;background-image:-webkit-gradient(linear,left bottom,left top,color-stop(.44,#7a8793),color-stop(.72,#56606a),color-stop(.86,#343a40))}.dropup .dropdown-menu{top:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}`],
                providers: [ContextualService, ToggleService],
                animations: [ToggleService.ANIMATION_OPACITY]
            },] },
];
/** @nocollapse */
DropdownMenuComponent.ctorParameters = () => [
    { type: ContextualService, },
    { type: ElementRef, },
    { type: LayoutComponent, },
    { type: ChangeDetectorRef, },
    { type: ToggleService, },
];
DropdownMenuComponent.propDecorators = {
    "disabled": [{ type: Input },],
    "fullWidth": [{ type: Input },],
    "dropUp": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class NavBarNavDropdownItemComponent {
    /**
     * @return {?}
     */
    close() {
        this.dropdown.close();
    }
}
NavBarNavDropdownItemComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-nav-bar-nav-dropdown-item',
                template: `
    <li class="nav-item">
      <ar-dropdown #dropdown>
        <ng-container title>
          <span class="nav-link dropdown-toggle">
            <ng-content select="[title]"></ng-content>
          </span>    
        </ng-container>
        <ng-content></ng-content>
      </ar-dropdown>
    </li>
  `
            },] },
];
/** @nocollapse */
NavBarNavDropdownItemComponent.propDecorators = {
    "dropdown": [{ type: ViewChild, args: ['dropdown',] },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class DropdownMenuItemComponent {
    /**
     * @param {?} dropdown
     * @param {?} dropdownMenu
     * @param {?} contextualService
     */
    constructor(dropdown, dropdownMenu, contextualService) {
        this.dropdown = dropdown;
        this.dropdownMenu = dropdownMenu;
        this.contextualService = contextualService;
        this.closeOnSelect = true;
        this.className = true;
        this.disabledClassName = false;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set disabled(val) {
        this.disabledClassName = val;
    }
    /**
     * @return {?}
     */
    get parentDropdown() {
        if (this.dropdown) {
            return this.dropdown;
        }
        if (this.dropdownMenu) {
            return this.dropdownMenu;
        }
        return null;
    }
    /**
     * @param {?} e
     * @return {?}
     */
    handleClick(e) {
        e.stopPropagation();
        if (this.disabledClassName) {
            return;
        }
        if (this.closeOnSelect && this.parentDropdown) {
            this.parentDropdown.close();
        }
    }
}
DropdownMenuItemComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-dropdown-menu-item',
                template: `
    <div class="prefix-block" [arBsBgContext]="contextualService.bsContext"></div>
    <ng-content></ng-content>
    <div class="pull-right">
      <ng-content select="[right]"></ng-content>
    </div>
  `,
                styles: [`.dropdown.full-width .dropdown-menu-selector{display:block}.dropdown.full-width .dropdown-menu{width:100%}.dropdown-menu-selector{cursor:pointer;display:inline-block}.dropdown-menu{display:block;padding:5px;overflow-y:auto}.dropdown-menu::-webkit-scrollbar-track{-webkit-box-shadow:inset 0 0 6px rgba(0,0,0,.3);background-color:#f8f9fa;border-radius:10px}.dropdown-menu::-webkit-scrollbar{width:10px;height:10px;background-color:#f5f5f5;border-radius:10px}.dropdown-menu::-webkit-scrollbar-thumb{border-radius:10px;background-image:-webkit-gradient(linear,left bottom,left top,color-stop(.44,#7a8793),color-stop(.72,#56606a),color-stop(.86,#343a40))}.dropup .dropdown-menu{top:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}:host{cursor:pointer;pointer-events:auto;margin-left:-5px;width:calc(100% + 10px);position:relative}:host .prefix-block{width:5px;position:absolute;left:0;top:0;bottom:0;opacity:0;-webkit-transition:opacity .2s ease-in;transition:opacity .2s ease-in}:host:hover .prefix-block{opacity:1}:host.disabled{pointer-events:none}:host.disabled:hover .prefix-block{opacity:0}`],
                providers: [ContextualService]
            },] },
];
/** @nocollapse */
DropdownMenuItemComponent.ctorParameters = () => [
    { type: DropdownComponent, decorators: [{ type: Optional },] },
    { type: DropdownMenuComponent, decorators: [{ type: Optional },] },
    { type: ContextualService, },
];
DropdownMenuItemComponent.propDecorators = {
    "disabled": [{ type: Input },],
    "closeOnSelect": [{ type: Input },],
    "className": [{ type: HostBinding, args: ['class.dropdown-item',] },],
    "disabledClassName": [{ type: HostBinding, args: ['class.disabled',] },],
    "handleClick": [{ type: HostListener, args: ['click', ['$event'],] },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class IconComponent {
    /**
     * @param {?} contextualService
     */
    constructor(contextualService) {
        this.contextualService = contextualService;
        this.className = true;
        contextualService.baseClass = IconComponent.BASE_CLASS;
    }
    /**
     * @return {?}
     */
    get svgStyle() {
        const /** @type {?} */ style$$1 = {};
        if (this.size) {
            style$$1['width.px'] = this.size;
            style$$1['height.px'] = this.size;
        }
        return style$$1;
    }
    /**
     * @return {?}
     */
    get link() { return `#${this.name}`; }
}
IconComponent.BASE_CLASS = 'ar-icon';
IconComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-icon',
                template: `
    <svg xmlns="http://www.w3.org/2000/svg" #svg [ngStyle]="svgStyle">
      <use [attr.xlink:href]="link" *ngIf="name" />
    </svg>
  `,
                styles: [`:host{display:inline-block;vertical-align:middle;line-height:0}:host svg{width:24px;height:24px}:host.ar-icon-xl svg{width:48px;height:48px}:host.ar-icon-lg svg{width:30px;height:30px}:host.ar-icon-md svg{width:24px;height:24px}:host.ar-icon-xs svg{width:20px;height:20px}:host.ar-icon-sm svg{width:16px;height:16px}:host.ar-icon-primary svg{fill:#007bff}:host.ar-icon-secondary svg{fill:#6c757d}:host.ar-icon-success svg{fill:#28a745}:host.ar-icon-info svg{fill:#17a2b8}:host.ar-icon-warning svg{fill:#ffc107}:host.ar-icon-danger svg{fill:#dc3545}:host.ar-icon-light svg{fill:#f8f9fa}:host.ar-icon-dark svg{fill:#343a40}:host.ar-icon-white svg{fill:#fff}`],
                providers: [ContextualService]
            },] },
];
/** @nocollapse */
IconComponent.ctorParameters = () => [
    { type: ContextualService, },
];
IconComponent.propDecorators = {
    "name": [{ type: Input },],
    "size": [{ type: Input },],
    "className": [{ type: HostBinding, args: ['class.ar-icon',] },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class FigureComponent {
}
FigureComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-figure',
                template: `
    <figure class="figure">
      <ng-content></ng-content>
      <figcaption class="figure-caption" [arTextAlign]="captionAlign">
        <ng-content select="[caption]"></ng-content>
      </figcaption>
    </figure>
  `
            },] },
];
/** @nocollapse */
FigureComponent.propDecorators = {
    "captionAlign": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class TextAlignDirective {
    /**
     * @param {?} renderer2
     * @param {?} elementRef
     * @param {?} bsGroup
     */
    constructor(renderer2, elementRef, bsGroup) {
        this.renderer2 = renderer2;
        this.elementRef = elementRef;
        this.bsGroup = bsGroup;
    }
    /**
     * @param {?} direction
     * @return {?}
     */
    set arTextAlign(direction) {
        let /** @type {?} */ nextDirection = /** @type {?} */ (direction);
        if (!nextDirection && this.bsGroup) {
            nextDirection = this.bsGroup.textAlign;
        }
        if (this.currentValue === nextDirection) {
            return;
        }
        this.decorate(nextDirection);
    }
    /**
     * @param {?} nextDirection
     * @return {?}
     */
    decorate(nextDirection) {
        if (this.currentClass) {
            this.renderer2.removeClass(this.elementRef.nativeElement, this.currentClass);
        }
        this.currentClass = nextDirection ? `text-${nextDirection}` : '';
        this.currentValue = nextDirection;
        if (this.currentClass) {
            this.renderer2.addClass(this.elementRef.nativeElement, this.currentClass);
        }
    }
}
TextAlignDirective.decorators = [
    { type: Directive, args: [{
                selector: '[arTextAlign]'
            },] },
];
/** @nocollapse */
TextAlignDirective.ctorParameters = () => [
    { type: Renderer2, },
    { type: ElementRef, },
    { type: BsGroupComponent, decorators: [{ type: Optional },] },
];
TextAlignDirective.propDecorators = {
    "arTextAlign": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class AlertComponent {
    /**
     * @param {?} contextualService
     */
    constructor(contextualService) {
        this.close = new EventEmitter();
        this.className = true;
        this.role = 'alert';
        contextualService.baseClass = AlertComponent.BASE_CLASS;
    }
}
AlertComponent.BASE_CLASS = 'alert';
AlertComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-alert',
                template: `
    <button *ngIf="closeable" type="button" class="close" (click)="close.emit()">
      <span aria-hidden="true">&times;</span>
    </button>
    <ng-content></ng-content>
  `,
                styles: [`
    :host { display: block; }
  `],
                providers: [
                    ContextualService
                ]
            },] },
];
/** @nocollapse */
AlertComponent.ctorParameters = () => [
    { type: ContextualService, },
];
AlertComponent.propDecorators = {
    "closeable": [{ type: Input },],
    "close": [{ type: Output },],
    "className": [{ type: HostBinding, args: ['class.alert',] },],
    "role": [{ type: HostBinding, args: ['attr.role',] },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class CardComponent {
    /**
     * @param {?} toggleService
     * @param {?} contextualService
     */
    constructor(toggleService, contextualService) {
        this.toggleService = toggleService;
        this.showHeader = false;
        this.showFooter = false;
        this.change = new EventEmitter();
        this.className = true;
        contextualService.baseClass = CardComponent.BASE_CLASS;
    }
    /**
     * @return {?}
     */
    get hasTopImage() { return Boolean(this.image) && !this.imageBottom; }
    /**
     * @return {?}
     */
    get hasBottomImage() { return Boolean(this.image) && this.imageBottom; }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.toggleService.state = this.collapsible && this.defaultCollapsed
            ? ToggleService.INACTIVE_STATE
            : ToggleService.ACTIVE_STATE;
    }
    /**
     * @return {?}
     */
    toggle() {
        if (!this.collapsible) {
            return;
        }
        this.toggleService.toggle();
        this.change.emit(this.toggleService.boolState);
    }
}
CardComponent.BASE_CLASS = 'card';
CardComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-card, [arCard]',
                template: `<img *ngIf="hasTopImage" class="card-img-top" [src]="image?.src" [alt]="image?.alt">
<div class="card-header"
     [class.pointer]="collapsible"
     [hidden]="!showHeader"
     (click)="toggle()"
>
  <ng-content select="[header]"></ng-content>
</div>
<div [class.card-body]="!imageOverlay"
     [class.card-img-overlay]="imageOverlay"
     [class.scrollable]="height"
     [ngStyle]="{ 'max-height.px': height }"
     [@toggleServiceState]="toggleService.state"
>
  <ng-container *ngIf="cardContent">
    <h5 class="card-title">{{ cardContent?.title }}</h5>
    <h6 class="card-subtitle" arBsTextContext="muted">{{ cardContent?.subtitle }}</h6>
    <p class="card-text">{{ cardContent?.text }}</p>
  </ng-container>
  <ng-content></ng-content>
</div>
<div class="card-footer" arBsTextContext="muted" [hidden]="!showFooter">
  <ng-content select="[footer]"></ng-content>
</div>
<img *ngIf="hasBottomImage" class="card-img-top" [src]="image?.src" [alt]="image?.alt">
`,
                styles: [`.scrollable{overflow-y:auto}.scrollable::-webkit-scrollbar-track{-webkit-box-shadow:inset 0 0 6px rgba(0,0,0,.3);background-color:#f8f9fa;border-radius:10px}.scrollable::-webkit-scrollbar{width:10px;height:10px;background-color:#f5f5f5;border-radius:10px}.scrollable::-webkit-scrollbar-thumb{border-radius:10px;background-image:-webkit-gradient(linear,left bottom,left top,color-stop(.44,#7a8793),color-stop(.72,#56606a),color-stop(.86,#343a40))}.pointer{cursor:pointer}`],
                animations: [ToggleService.ANIMATION_HEIGHT],
                providers: [ToggleService, ContextualService]
            },] },
];
/** @nocollapse */
CardComponent.ctorParameters = () => [
    { type: ToggleService, },
    { type: ContextualService, },
];
CardComponent.propDecorators = {
    "image": [{ type: Input },],
    "imageBottom": [{ type: Input },],
    "cardContent": [{ type: Input },],
    "imageOverlay": [{ type: Input },],
    "showHeader": [{ type: Input },],
    "showFooter": [{ type: Input },],
    "height": [{ type: Input },],
    "collapsible": [{ type: Input },],
    "defaultCollapsed": [{ type: Input },],
    "change": [{ type: Output },],
    "className": [{ type: HostBinding, args: ['class.card',] },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class InnerLinkComponent {
    /**
     * @param {?} element
     * @param {?} renderer
     * @param {?} card
     * @param {?} alertComponent
     */
    constructor(element, renderer, card, alertComponent) {
        this.element = element;
        this.renderer = renderer;
        this.card = card;
        this.alertComponent = alertComponent;
    }
    /**
     * @return {?}
     */
    get textContext() {
        return /** @type {?} */ ((this.disabled ? BsTextContextEnum.MUTED : BsTextContextEnum.DEFAULT));
    }
    /**
     * @return {?}
     */
    get isAlertLink() {
        return Boolean(this.alertComponent) || this.isAlert;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.card) {
            this.renderer.addClass(this.element.nativeElement, 'card-link');
        }
    }
}
InnerLinkComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-inner-link',
                template: `
    <a [routerLink]="url"
       [class.disabled]="disabled"
       [class.alert-link]="isAlertLink"
       [arBsTextContext]="textContext"
    >
      <ng-content></ng-content>
    </a>
  `,
                styles: [`a{cursor:pointer;color:inherit;text-decoration:underline}a.disabled{pointer-events:none;cursor:default;text-decoration:none}a:hover{text-decoration:none}`]
            },] },
];
/** @nocollapse */
InnerLinkComponent.ctorParameters = () => [
    { type: ElementRef, },
    { type: Renderer2, },
    { type: CardComponent, decorators: [{ type: Optional },] },
    { type: AlertComponent, decorators: [{ type: Optional },] },
];
InnerLinkComponent.propDecorators = {
    "url": [{ type: Input },],
    "disabled": [{ type: Input },],
    "isAlert": [{ type: Input },],
    "isCard": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class BlockComponent {
}
BlockComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-block',
                template: `
    <p *ngIf="title" class="h6 border-bottom border-muted" arBsTextContext="muted">{{ title }}</p>
    <ng-content></ng-content>
  `,
                styles: [`:host{display:block;margin-bottom:10px;padding:5px}`]
            },] },
];
/** @nocollapse */
BlockComponent.propDecorators = {
    "title": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class TabLayoutViewModel {
    /**
     * @return {?}
     */
    get hasOneTab() {
        return size(this.tabs) === 1;
    }
    /**
     * @return {?}
     */
    get hasTabs() {
        return size(this.tabs) > 1;
    }
    /**
     * @param {?} tab
     * @return {?}
     */
    addTab(tab) {
        if (!this.tabs) {
            this.tabs = [tab];
            this.selectTab(tab);
        }
        else {
            this.tabs.push(tab);
        }
    }
    /**
     * @param {?} tab
     * @return {?}
     */
    removeTab(tab) {
        if (this.selectedTab === tab) {
            const /** @type {?} */ nextTab = this.getNextTab(tab);
            this.selectTab(nextTab);
        }
        this.tabs = without(this.tabs, tab);
    }
    /**
     * @param {?=} tab
     * @return {?}
     */
    selectTab(tab) {
        if (!tab) {
            return;
        }
        if (this.selectedTab) {
            this.selectedTab.active = false;
        }
        tab.active = true;
        this.selectedTab = tab;
    }
    /**
     * @param {?} tab
     * @return {?}
     */
    exist(tab) {
        return Boolean(find(this.tabs, (item) => item === tab));
    }
    /**
     * @param {?} tabToDelete
     * @return {?}
     */
    getNextTab(tabToDelete) {
        const /** @type {?} */ currentTabIndex = findIndex(this.tabs, tabToDelete);
        if (currentTabIndex < 0) {
            return first(this.tabs);
        }
        if (currentTabIndex === 0 && size(this.tabs) > 1) {
            return this.tabs[1];
        }
        if (currentTabIndex > 0) {
            return this.tabs[currentTabIndex - 1];
        }
        return null;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class TabLayoutComponent {
    constructor() {
        this.tabRemove = new Subject();
        this.tabSelect = new Subject();
        this.tabCreate = new Subject();
        this.viewModel = new TabLayoutViewModel();
    }
    /**
     * @param {?} tab
     * @return {?}
     */
    registerTab(tab) {
        this.viewModel.addTab(tab);
    }
    /**
     * @param {?} tab
     * @return {?}
     */
    removeTab(tab) {
        if (tab.changed) {
            this.viewModel.tabToClose = tab;
        }
        else {
            this.deleteTab(tab);
        }
    }
    /**
     * @param {?} tab
     * @return {?}
     */
    exist(tab) {
        return this.viewModel.exist(tab);
    }
    /**
     * @param {?} tab
     * @return {?}
     */
    deleteTab(tab) {
        this.viewModel.removeTab(tab);
        if (tab.viewRef) {
            const /** @type {?} */ index = this.content.indexOf(tab.viewRef);
            this.content.remove(index);
        }
        this.tabRemove.next(tab);
        this.viewModel.tabToClose = null;
    }
    /**
     * @param {?} tab
     * @param {?} template
     * @param {?=} context
     * @return {?}
     */
    createTab(tab, template, context) {
        if (this.exist(tab)) {
            this.selectTab(tab);
            return;
        }
        tab.viewRef = this.content.createEmbeddedView(template, { $implicit: context, tab });
        this.registerTab(tab);
        this.selectTab(tab);
        this.tabCreate.next(tab);
    }
    /**
     * @param {?} tab
     * @return {?}
     */
    selectTab(tab) {
        this.viewModel.selectTab(tab);
        this.tabSelect.next(tab);
    }
}
TabLayoutComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-tab-layout',
                template: `<div class="content-layout">
    <div class="content" [class.has-notification]="viewModel.tabToClose">
        <ul class="nav nav-tabs nav-justified tab-links" *ngIf="viewModel.hasTabs">
            <li class="nav-item" *ngFor="let tab of viewModel.tabs" [class.changed]="tab.changed">
                <a (click)="selectTab(tab)" class="tab-link-inner nav-link" [class.active]="tab.active">
                    <ar-tab-link [model]="tab" (onClose)="removeTab($event)">{{tab.title}}</ar-tab-link>
                </a>
            </li>
        </ul>

        <ar-block class="content">
            <ng-container #content>
                <ng-content></ng-content>
            </ng-container>
        </ar-block>
    </div>

    <div class="notification modal-dialog modal-sm" *ngIf="viewModel.tabToClose">
        <div class="modal-content">
            <div class="modal-body">
                Discard changes?
            </div>
            <div class="modal-footer">
                <button class="btn" arBsContext="danger" arBsSize="sm" (click)="deleteTab(viewModel.tabToClose)">
                    Yes
                </button>
                <button class="btn" arBsContext="default" arBsSize="sm" (click)="viewModel.tabToClose = null">
                    Cancel
                </button>
            </div>
        </div>
    </div>
</div>
`,
                styles: [`.tab{cursor:pointer;display:inline-block;width:auto;margin-right:5px}.tab.changed:before{content:'*';display:inline-block;position:absolute;top:2px;left:2px;z-index:1;color:#000;font-size:14px}.tab.active.changed:before{color:#fff}.nav-item{cursor:pointer}.tab-links{overflow-x:auto;overflow-y:hidden;-ms-flex-wrap:wrap;flex-wrap:wrap;-ms-flex-line-pack:start;align-content:flex-start;-webkit-box-pack:start;-ms-flex-pack:start;justify-content:flex-start}.tab-links::-webkit-scrollbar-track{-webkit-box-shadow:inset 0 0 6px rgba(0,0,0,.3);background-color:#f8f9fa;border-radius:10px}.tab-links::-webkit-scrollbar{width:10px;height:10px;background-color:#f5f5f5;border-radius:10px}.tab-links::-webkit-scrollbar-thumb{border-radius:10px;background-image:-webkit-gradient(linear,left bottom,left top,color-stop(.44,#7a8793),color-stop(.72,#56606a),color-stop(.86,#343a40))}.tab-link-inner{padding:5px;font-size:90%;-webkit-transition:all .3s ease-in;transition:all .3s ease-in}.tab-link-inner.active:hover{color:initial}.content.has-notification{-webkit-filter:grayscale(50%) opacity(50%);filter:grayscale(50%) opacity(50%)}.content.has-notification:before{content:" ";z-index:1;width:100%;height:100%;display:block;position:absolute}.content-layout{position:relative}.content-layout .notification{position:absolute;top:30px;left:calc(50% - 150px)}.content{position:relative}`]
            },] },
];
/** @nocollapse */
TabLayoutComponent.ctorParameters = () => [];
TabLayoutComponent.propDecorators = {
    "content": [{ type: ViewChild, args: ['content', { read: ViewContainerRef },] },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class TabModel {
    /**
     * @param {?=} title
     * @param {?=} closeable
     */
    constructor(title = 'Tab title', closeable = false) {
        this.title = title;
        this.closeable = closeable;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class TabComponent {
    /**
     * @param {?} tabLayout
     */
    constructor(tabLayout) {
        this.tabLayout = tabLayout;
    }
    /**
     * @return {?}
     */
    get isActive() {
        return (this.model && this.model.active);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.model) {
            return;
        }
        this.model = new TabModel(this.title);
        this.tabLayout.registerTab(this.model);
        if (this.selected) {
            this.tabLayout.selectTab(this.model);
        }
    }
    /**
     * @param {?} title
     * @return {?}
     */
    updateTitle(title) {
        if (this.model) {
            this.model.title = title;
        }
    }
}
TabComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-tab',
                template: `
    <section *ngIf="isActive" [@toggleServiceState]>
      <ng-content></ng-content>
    </section>
  `,
                animations: [trigger('toggleServiceState', [
                        transition(':enter', [
                            style({ opacity: 0 }),
                            animate('200ms ease-in', style({ opacity: 1 }))
                        ]),
                        transition(':leave', [
                            style({ opacity: 1, position: 'absolute', top: '5px', right: '5px', left: '5px' }),
                            animate('200ms ease-out', style({ opacity: 0 }))
                        ])
                    ])
                ],
            },] },
];
/** @nocollapse */
TabComponent.ctorParameters = () => [
    { type: TabLayoutComponent, },
];
TabComponent.propDecorators = {
    "dynamicContent": [{ type: ViewChild, args: ['dynamicContent', { read: ViewContainerRef },] },],
    "title": [{ type: Input },],
    "selected": [{ type: Input },],
    "model": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class TabLinkComponent {
    constructor() {
        this.onClose = new EventEmitter();
    }
    /**
     * @param {?} event
     * @return {?}
     */
    closeTabHandler(event) {
        event.stopPropagation();
        this.onClose.emit(this.model);
    }
}
TabLinkComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-tab-link',
                template: `
      <ng-content></ng-content>
      <button type="button" class="close" (click)="closeTabHandler($event)" *ngIf="model.closeable">
          <span aria-hidden="true">×</span>
      </button>
  `,
                styles: [
                    `:host { white-space: nowrap; }`,
                    `.close { position: absolute; right: 3px; top: 0; }`
                ]
            },] },
];
/** @nocollapse */
TabLinkComponent.propDecorators = {
    "model": [{ type: Input },],
    "onClose": [{ type: Output },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class DropdownDividerComponent {
}
DropdownDividerComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-dropdown-divider',
                template: `
    <small arBsTextContext="muted">
      <ng-content></ng-content>
    </small>
    <div class="dropdown-divider"></div>
  `
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class AlertsAreaService {
    constructor() {
        this.addAlert = (message) => {
            this.removeOldestAlert();
            message.viewRef = this.container.createEmbeddedView(this.template, { $implicit: message });
            this.createAutohideObserver(message).subscribe(this.removeAlert);
        };
        this.removeAlert = (message) => {
            if (!message) {
                return;
            }
            this.removeAlertByViewRef(message.viewRef);
        };
    }
    /**
     * @param {?} container
     * @return {?}
     */
    registerContainer(container) {
        this.container = container;
    }
    /**
     * @param {?} template
     * @return {?}
     */
    registerTemplate(template) {
        this.template = template;
    }
    /**
     * @return {?}
     */
    removeOldestAlert() {
        if (this.container.length === AlertsAreaService.MAX_ALERT_STACK_SIZE) {
            this.container.remove(0);
        }
    }
    /**
     * @param {?} view
     * @return {?}
     */
    removeAlertByViewRef(view) {
        const /** @type {?} */ index = this.container.indexOf(view);
        if (index >= 0) {
            this.container.remove(index);
        }
    }
    /**
     * @param {?} message
     * @return {?}
     */
    createAutohideObserver(message) {
        if (!message.shouldAutohide) {
            return of(null);
        }
        return of(message).pipe(delay(message.displayTime));
    }
}
AlertsAreaService.MAX_ALERT_STACK_SIZE = 3;
AlertsAreaService.decorators = [
    { type: Injectable },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class AlertAreaComponent {
    /**
     * @param {?} messageService
     * @param {?} alertService
     */
    constructor(messageService, alertService) {
        this.messageService = messageService;
        this.alertService = alertService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.alertService.registerContainer(this.alertContainer);
        this.alertService.registerTemplate(this.alertTemplate);
        this.alertSubscription = this.messageService.of(DisplayAlertMessage).subscribe(this.alertService.addAlert);
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.alertSubscription.unsubscribe();
    }
}
AlertAreaComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-alert-area',
                template: `
      <ng-container #alertContainer></ng-container>
      <ng-template #alertTemplate let-message>
          <ar-alert [arBsContext]="message.bsContext"
                   [closeable]="message.closeable"
                   (close)="alertService.removeAlert(message)"
                   class="inner-alert"
          >
              <ng-container *ngIf="message.isSimple; else contentTemplate">{{ message.message }}</ng-container>
              <ng-template #contentTemplate>
                  <ng-container *ngTemplateOutlet="message.template; context: { $implicit: message.templateContext }">
                  </ng-container>
              </ng-template>
          </ar-alert>
      </ng-template>
  `,
                providers: [AlertsAreaService],
                styles: [`:host{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:reverse;-ms-flex-direction:column-reverse;flex-direction:column-reverse;width:500px;height:auto;position:fixed;bottom:0;right:0;z-index:1;padding:10px}.inner-alert{opacity:.95}`]
            },] },
];
/** @nocollapse */
AlertAreaComponent.ctorParameters = () => [
    { type: MessageBusService, },
    { type: AlertsAreaService, },
];
AlertAreaComponent.propDecorators = {
    "alertContainer": [{ type: ViewChild, args: ['alertContainer', { read: ViewContainerRef },] },],
    "alertTemplate": [{ type: ViewChild, args: ['alertTemplate',] },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class AlertHeadingComponent {
}
AlertHeadingComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-alert-heading',
                template: `
    <span class="h4 alert-heading">
      <ng-content></ng-content>
    </span>
  `
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class LoremIpsumComponent {
    constructor() {
        this.loremIpsumOriginal = `
    Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
    totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo.
    Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos,
    qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, 
    consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt, 
    ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, 
    quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?
    Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur,
    vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur?
    At vero eos et accusamus et iusto odio dignissimos ducimus,
    qui blanditiis praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias excepturi sint,
    obcaecati cupiditate non provident, similique sunt in culpa, qui officia deserunt mollitia animi,
    id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
    Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id,
    quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.
    Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet,
    ut et voluptates repudiandae sint et molestiae non recusandae.
    Itaque earum rerum hic tenetur a sapiente delectus,
    ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.
  `;
    }
}
LoremIpsumComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-lorem-ipsum',
                template: `
    <p>{{ loremIpsumOriginal | arTruncate:length }}</p>
  `
            },] },
];
/** @nocollapse */
LoremIpsumComponent.propDecorators = {
    "length": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class HelpBlockComponent {
    constructor() {
        this.mini = true;
    }
}
HelpBlockComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-help-block, ar-form-text',
                template: `
    <span class="form-text" [class.form-text--mini]="mini" arBsTextContext="muted">
      <ng-content></ng-content>
    </span>
  `,
                styles: [`
    .form-text--mini {
      font-size: 80%;
    }
  `]
            },] },
];
/** @nocollapse */
HelpBlockComponent.propDecorators = {
    "mini": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class OuterLinkComponent extends InnerLinkComponent {
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        const { url, href } = changes;
        if (!url || (url.currentValue === url.previousValue && !url.firstChange)) {
            return;
        }
        if (!url.currentValue) {
            this.urlString = '';
            this.parsedUrl = null;
            return;
        }
        this.urlString = url.currentValue;
        // let value = url.currentValue;
        //
        // if (!/^https?:\/\//i.test(value)) {
        //   value = 'http://' + value;
        // }
        //
        // this.parsedUrl = URL(value, {});
        // this.urlString = this.parsedUrl.toString();
    }
}
OuterLinkComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-outer-link',
                template: `
    <a target="_blank" [href]="urlString" *ngIf="url"
       [class.disabled]="disabled"
       [class.alert-link]="isAlertLink"
       [arBsTextContext]="textContext"
    >
        <ng-content></ng-content>
    </a>
  `,
                styles: [`a{cursor:pointer;color:inherit;text-decoration:underline}a.disabled{pointer-events:none;cursor:default;text-decoration:none}a:hover{text-decoration:none}`, `.as-text{color:inherit}.as-text:focus,.as-text:hover{text-decoration:none}`]
            },] },
];
/** @nocollapse */
OuterLinkComponent.propDecorators = {
    "url": [{ type: Input },],
    "disabled": [{ type: Input },],
    "isAlert": [{ type: Input },],
    "isCard": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class PageHeaderComponent {
}
PageHeaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-page-header',
                template: `
      <ar-block class="page-header clearfix border-bottom">
          <div class="pull-left">
            <h1>
                <ng-content></ng-content>
            </h1>
          </div>
          <div class="pull-right">
            <ng-content select="[side-content]"></ng-content>
          </div>
      </ar-block>
  `
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
const XL = 'xl';
const LG = 'lg';
const MD = 'md';
const SM = 'sm';
const XS = 'xs';
const DEFAULT$2 = '';
const BsSizeEnum = { XL, LG, MD, XS, SM, DEFAULT: DEFAULT$2 };
const BsSizeArray = values(BsSizeEnum);

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class PagingListSizeComponent {
    /**
     * @param {?} contextualService
     */
    constructor(contextualService) {
        this.contextualService = contextualService;
        this.list = [10, 20, 50, 100];
        this.bsSize = BsSizeEnum.SM;
        this.onSizeChange = new EventEmitter();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        const { size: size$$1 } = changes;
        if (!size$$1.firstChange && size$$1.previousValue !== size$$1.currentValue) {
            this.form.reset({ size: size$$1.currentValue }, { emitEvent: false });
        }
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        const /** @type {?} */ size$$1 = new FormControl(this.size);
        this.changeSubscription = size$$1.valueChanges
            .subscribe((val) => { this.onSizeChange.emit(val); });
        this.form = new FormGroup({ size: size$$1 });
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.changeSubscription.unsubscribe();
    }
}
PagingListSizeComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-paging-list-size',
                template: `
    <div *ngIf='form' [formGroup]="form">
        <select class="form-control" formControlName="size" [arBsSize]="contextualService.bsSize">
            <option *ngFor="let item of list" [value]="item">{{item}}</option>
        </select>
    </div>
  `,
                styles: [`:host{display:inline-block;margin:0 5px;padding-bottom:1px}`],
                changeDetection: ChangeDetectionStrategy.OnPush,
                providers: [ContextualService]
            },] },
];
/** @nocollapse */
PagingListSizeComponent.ctorParameters = () => [
    { type: ContextualService, },
];
PagingListSizeComponent.propDecorators = {
    "size": [{ type: Input },],
    "list": [{ type: Input },],
    "bsSize": [{ type: Input },],
    "onSizeChange": [{ type: Output },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class PaginationComponent {
    /**
     * @param {?} contextualService
     */
    constructor(contextualService) {
        this.contextualService = contextualService;
        this.onPageSelect = new EventEmitter();
        this._total = 0;
        this.createPageItem = (page, title = null, disabled = false, common = false) => {
            return {
                title: title || String(page),
                page,
                disabled: disabled || (common && page === this.current) || page < 0 || page > this.total,
                common
            };
        };
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set total(val) {
        this._total = Number(val);
        this.pageLinks = this.createPages(this.total);
    }
    /**
     * @return {?}
     */
    get total() { return this._total; }
    /**
     * @param {?} val
     * @return {?}
     */
    set current(val) {
        this._currentPage = Number(val);
        this.pageLinks = this.createPages(this.total);
    }
    /**
     * @return {?}
     */
    get current() { return this._currentPage; }
    /**
     * @return {?}
     */
    get isVisible() {
        return this.total > 0 && this.current <= this.total && this.current > 0;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.pageLinks = this.createPages(this.total);
    }
    /**
     * @param {?} page
     * @return {?}
     */
    handlePageClick(page) {
        if (!page || page.disabled || this.isActivePage(page)) {
            return;
        }
        this.onPageSelect.emit(page.page);
    }
    /**
     * @param {?} page
     * @return {?}
     */
    isActivePage(page) {
        return !page.disabled && !page.common && this.current === page.page;
    }
    /**
     * @param {?} total
     * @return {?}
     */
    createPages(total) {
        if (total <= PaginationComponent.MAX_DISPLAY) {
            return map(range(1, total + 1), (page) => this.createPageItem(page));
        }
        if (this.current < PaginationComponent.EDGE_COUNT - 1) {
            return concat([this.createPageItem(this.current - 1, PaginationComponent.PREV_TITLE, this.current < 2, true)], map(range(1, PaginationComponent.EDGE_COUNT), (page) => this.createPageItem(page)), [this.createPageItem(0, PaginationComponent.EMPTY_TITLE, true, true)], map(range(total - PaginationComponent.MIN_COUNT + 1, total + 1), (page) => this.createPageItem(page)), [this.createPageItem(this.current + 1, PaginationComponent.NEXT_TITLE, false, true)]);
        }
        if (this.total - PaginationComponent.EDGE_COUNT + 2 < this.current) {
            return concat([this.createPageItem(this.current - 1, PaginationComponent.PREV_TITLE, false, true)], map(range(1, PaginationComponent.MIN_COUNT + 1), (page) => this.createPageItem(page)), [this.createPageItem(0, PaginationComponent.EMPTY_TITLE, true, true)], map(range(total - PaginationComponent.EDGE_COUNT + 2, total + 1), (page) => this.createPageItem(page)), [this.createPageItem(this.current + 1, PaginationComponent.NEXT_TITLE, false, true)]);
        }
        return concat([this.createPageItem(this.current - 1, PaginationComponent.PREV_TITLE, false, true)], map(range(1, PaginationComponent.MIN_COUNT), (page) => this.createPageItem(page)), [this.createPageItem(0, PaginationComponent.EMPTY_TITLE, true, true)], map(range(this.current - 1, this.current + 2), (page) => this.createPageItem(page)), [this.createPageItem(0, PaginationComponent.EMPTY_TITLE, true, true)], map(range(total - PaginationComponent.MIN_COUNT + 2, total + 1), (page) => this.createPageItem(page)), [this.createPageItem(this.current + 1, PaginationComponent.NEXT_TITLE, false, true)]);
    }
}
PaginationComponent.PREV_TITLE = '«';
PaginationComponent.NEXT_TITLE = '»';
PaginationComponent.EMPTY_TITLE = '...';
PaginationComponent.MAX_DISPLAY = 15;
PaginationComponent.EDGE_COUNT = 6;
PaginationComponent.MIN_COUNT = 3;
PaginationComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-pagination',
                template: `
      <nav *ngIf="isVisible" class="noselect">
          <ul class="pagination" [arBsSize]="contextualService.bsSize">
              <li *ngFor="let item of pageLinks"
                  [class.active]="isActivePage(item)"
                  [class.disabled]="item.disabled"
                  class="page-item"
              >
                  <a class="pointer page-link" (click)="handlePageClick(item)">
                      {{item.title}}
                  </a>
              </li>
          </ul>
      </nav>
  `,
                styles: [
                    ':host { display: inline-block; vertical-align: middle; }',
                    '.pagination { margin: 0; }'
                ],
                providers: [ContextualService]
            },] },
];
/** @nocollapse */
PaginationComponent.ctorParameters = () => [
    { type: ContextualService, },
];
PaginationComponent.propDecorators = {
    "total": [{ type: Input },],
    "current": [{ type: Input },],
    "onPageSelect": [{ type: Output },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class PagingTextComponent {
    /**
     * @return {?}
     */
    get shouldDisplay() {
        return this.paging && this.paging.getTotalCount() > 0;
    }
}
PagingTextComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-paging-text',
                template: `
    <small *ngIf="shouldDisplay">
      Showing {{ paging.getStartRecord() }} to {{ paging.getEndRecord() }} of {{ paging.getTotalCount() }} entries
    </small>
  `,
                styles: [
                    `:host {
        display: inline-block;
        margin: 0 1em;
    }`
                ]
            },] },
];
/** @nocollapse */
PagingTextComponent.propDecorators = {
    "paging": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class PagingControlComponent {
    /**
     * @param {?} contextualService
     */
    constructor(contextualService) {
        this.contextualService = contextualService;
        this.bsSize = BsSizeEnum.SM;
        this.onChange = new EventEmitter();
    }
}
PagingControlComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-paging-control',
                template: `
    <ng-container *ngIf="paging">
      <small>Records per page</small>
      <ar-paging-list-size
        [size]="paging.getPageSize()"
        (onSizeChange)="onChange.emit({ pageSize: $event, page: paging.getPage() })"
        [arBsSize]="contextualService.bsSize"
      >
      </ar-paging-list-size>
      <ar-pagination
        [total]="paging.getPagesCount()"
        [current]="paging.getPage()"
        (onPageSelect)="onChange.emit({ pageSize: paging.getPageSize(), page: $event })"
        [arBsSize]="contextualService.bsSize"
      >
      </ar-pagination>
      <ar-paging-text [paging]="paging"></ar-paging-text>
    </ng-container>
  `,
                styles: [
                    ':host { display: flex; align-items: center; } '
                ],
                providers: [ContextualService]
            },] },
];
/** @nocollapse */
PagingControlComponent.ctorParameters = () => [
    { type: ContextualService, },
];
PagingControlComponent.propDecorators = {
    "paging": [{ type: Input },],
    "bsSize": [{ type: Input },],
    "onChange": [{ type: Output },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class MailLinkComponent {
}
MailLinkComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-mail-link',
                template: `
    <a *ngIf="email" href="mailto:{{email}}">{{email}}</a>
  `
            },] },
];
/** @nocollapse */
MailLinkComponent.propDecorators = {
    "email": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ModalLayoutViewModel {
    /**
     * @param {?} viewContainer
     */
    constructor(viewContainer) {
        this.container = viewContainer;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ModalService {
    /**
     * @param {?} logService
     */
    constructor(logService) {
        this.logService = logService;
    }
    /**
     * @param {?} template
     * @param {?} config
     * @param {?=} context
     * @return {?}
     */
    showModal(template, config, context) {
        this.checkLayout();
        if (this.layoutViewModel.template) {
            this.layoutViewModel.container.clear();
        }
        this.layoutViewModel.viewRef = this.layoutViewModel.container.createEmbeddedView(template, { $implicit: context });
        this.layoutViewModel.template = template;
        this.layoutViewModel.config = config;
        this.layoutViewModel.opened = true;
    }
    /**
     * @return {?}
     */
    hideModal() {
        this.checkLayout();
        this.layoutViewModel.opened = false;
        this.layoutViewModel.template = null;
        this.layoutViewModel.viewRef = null;
        this.layoutViewModel.config = null;
        this.layoutViewModel.container.clear();
    }
    /**
     * @param {?} viewContainer
     * @return {?}
     */
    registerLayout(viewContainer) {
        if (this.layoutViewModel) {
            this.logService.log('ModalService: skip Modal Layout already registered error');
        }
        else if (this.layoutViewModel) {
            throw new Error('ModalService: another instance Modal Layout already registered');
        }
        this.layoutViewModel = this.createViewModel(viewContainer);
        return this.layoutViewModel;
    }
    /**
     * @return {?}
     */
    unregisterLayout() {
        this.checkLayout();
        this.layoutViewModel = null;
    }
    /**
     * @param {?} viewContainer
     * @return {?}
     */
    createViewModel(viewContainer) {
        return new ModalLayoutViewModel(viewContainer);
    }
    /**
     * @return {?}
     */
    checkLayout() {
        if (!this.layoutViewModel) {
            throw new Error('ModalService: modal layout not registered');
        }
    }
}
ModalService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
ModalService.ctorParameters = () => [
    { type: LogService, },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
let modalService;
const modalServiceFactory = (logService) => {
    if (!modalService) {
        modalService = new ModalService(logService);
    }
    return modalService;
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
const ɵ0$1 = modalServiceFactory;
class ModalLayoutComponent {
    /**
     * @param {?} modalService
     */
    constructor(modalService) {
        this.modalService = modalService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.viewModel = this.modalService.registerLayout(this.modalContent);
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.modalService.unregisterLayout();
    }
    /**
     * @param {?} event
     * @return {?}
     */
    backdropClick(event) {
        if (!this.viewModel.config) {
            return;
        }
        if (!this.viewModel.config.backdropClickClose) {
            return;
        }
        if (this.dialog.nativeElement.contains(event.target)) {
            return;
        }
        this.modalService.hideModal();
    }
    /**
     * @return {?}
     */
    getDialogClassName() {
        let /** @type {?} */ className = this.viewModel.config
            ? `modal-dialog ${this.viewModel.config.className}`
            : 'modal-dialog';
        className = this.viewModel.config && this.viewModel.config.centered
            ? `${className} modal-dialog-centered`
            : className;
        return className;
    }
}
ModalLayoutComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-modal-layout',
                template: `
      <div class="modal fade" [class.show]="viewModel.opened" (click)="backdropClick($event)">
          <div [class]="getDialogClassName()" #dialog>
            <ng-container #modalContent></ng-container>
          </div>
      </div>
      <div class="modal-backdrop fade show" *ngIf="viewModel.opened"></div>
  `,
                styles: ['.show { display: block; }'],
                providers: [{
                        provide: ModalService,
                        useFactory: ɵ0$1,
                        deps: [LogService]
                    }]
            },] },
];
/** @nocollapse */
ModalLayoutComponent.ctorParameters = () => [
    { type: ModalService, },
];
ModalLayoutComponent.propDecorators = {
    "modalContent": [{ type: ViewChild, args: ['modalContent', { read: ViewContainerRef },] },],
    "dialog": [{ type: ViewChild, args: ['dialog', { read: ElementRef },] },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ModalDialogConfigModel {
    constructor() {
        this.backdropClickClose = true;
        this.centered = false;
        this._classes = [];
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set large(value) {
        this.toggleClass('modal-lg', value);
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set small(value) {
        this.toggleClass('modal-sm', value);
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set customClass(value) {
        this.toggleClass(value, true);
    }
    /**
     * @return {?}
     */
    get className() {
        return join(this._classes, ' ');
    }
    /**
     * @param {?} className
     * @param {?} state
     * @return {?}
     */
    toggleClass(className, state$$1) {
        if (indexOf(this._classes, className) >= 0 && state$$1) {
            return;
        }
        this._classes = state$$1
            ? concat(this._classes, className)
            : without(this._classes, className);
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
const ɵ0$2 = modalServiceFactory;
class ModalComponent {
    /**
     * @param {?} modalService
     */
    constructor(modalService) {
        this.modalService = modalService;
        this.config = ModalComponent.createConfig();
        this.showHeader = true;
        this.showFooter = true;
    }
    /**
     * @return {?}
     */
    static createConfig() { return new ModalDialogConfigModel(); }
    /**
     * @param {?=} context
     * @return {?}
     */
    show(context) {
        this.modalService.showModal(this.modalTemplate, this.config, context);
    }
    /**
     * @return {?}
     */
    hide() {
        this.modalService.hideModal();
    }
}
ModalComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-modal',
                template: `
    <ng-template #modalTemplate let-context>
        <div class="modal-content" [@toggleServiceState]>
            <div class="modal-header" *ngIf="showHeader">
                <ng-container *ngIf="headerTemplate">
                    <ng-container *ngTemplateOutlet="headerTemplate; context: { $implicit: context }">
                    </ng-container>
                </ng-container>
                <ng-content select="[header]"></ng-content>
                <button type="button" class="close" (click)="hide()" >
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ng-container *ngIf="bodyTemplate">
                    <ng-container *ngTemplateOutlet="bodyTemplate; context: { $implicit: context }"></ng-container>
                </ng-container>
                <ng-content></ng-content>
            </div>
            <div class="modal-footer" *ngIf="showFooter">
                <ng-container *ngIf="footerTemplate">
                    <ng-container *ngTemplateOutlet="footerTemplate; context: { $implicit: context }"></ng-container>
                </ng-container>
                <ng-content select="[footer]"></ng-content>
            </div>
        </div>
    </ng-template>
  `,
                styles: [`
    .modal-content {
        word-wrap: break-word;
        overflow-wrap: break-word;
    }
  `],
                providers: [{
                        provide: ModalService,
                        useFactory: ɵ0$2,
                        deps: [LogService]
                    }],
                animations: [
                    ToggleService.ANIMATION_OPACITY
                ]
            },] },
];
/** @nocollapse */
ModalComponent.ctorParameters = () => [
    { type: ModalService, },
];
ModalComponent.propDecorators = {
    "modalTemplate": [{ type: ViewChild, args: ['modalTemplate', { read: TemplateRef },] },],
    "headerTemplate": [{ type: Input },],
    "bodyTemplate": [{ type: Input },],
    "footerTemplate": [{ type: Input },],
    "config": [{ type: Input },],
    "showHeader": [{ type: Input },],
    "showFooter": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ModalHeaderComponent {
}
ModalHeaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-modal-header',
                template: `
    <span class="modal-header-content">
      <ng-content></ng-content>
    </span>
  `,
                styles: [`
    .modal-header-content {
      font-weight: bold;
      font-size: 110%;
    }
  `]
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @deprecated
 * @see ar-card
 */
class PanelComponent {
    constructor() {
        this.showHeader = false;
        this.showFooter = false;
        this.change = new EventEmitter();
    }
}
PanelComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-panel',
                template: `
    <ar-card
      [arBsBgContext]="bsContext"
      [collapsible]="collapsible"
      [showHeader]="showHeader"
      [showFooter]="showFooter"
      [defaultCollapsed]="defaultCollapsed"
      [height]="height"
      (change)="change.emit($event)"
    >
      <ng-container header>
        <ng-content select="[header]" #header></ng-content>
      </ng-container>
      <ng-content></ng-content>
      <ng-container footer>
        <ng-content select="[footer]" #header></ng-content>
      </ng-container>
    </ar-card>
  `
            },] },
];
/** @nocollapse */
PanelComponent.propDecorators = {
    "bsContext": [{ type: Input },],
    "collapsible": [{ type: Input },],
    "showHeader": [{ type: Input },],
    "showFooter": [{ type: Input },],
    "defaultCollapsed": [{ type: Input },],
    "height": [{ type: Input },],
    "change": [{ type: Output },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class WidthDirective {
    /**
     * @param {?} renderer2
     * @param {?} elementRef
     * @param {?} logService
     */
    constructor(renderer2, elementRef, logService) {
        this.renderer2 = renderer2;
        this.elementRef = elementRef;
        this.logService = logService;
    }
    /**
     * @param {?} width
     * @return {?}
     */
    set arWidth(width) {
        if (width === this.currentWidth) {
            return;
        }
        this.checkWidthValue(width);
        this.decorate(width);
    }
    /**
     * @param {?} nextWidth
     * @return {?}
     */
    decorate(nextWidth) {
        this.renderer2.removeClass(this.elementRef.nativeElement, this.currentClass);
        this.currentWidth = nextWidth;
        this.currentClass = nextWidth ? `${WidthDirective.WIDTH_PREFIX}-${this.currentWidth}` : '';
        if (this.currentClass) {
            this.renderer2.addClass(this.elementRef.nativeElement, this.currentClass);
        }
    }
    /**
     * @param {?} value
     * @return {?}
     */
    checkWidthValue(value) {
        const /** @type {?} */ stringValue = value ? String(value) : '';
        if (indexOf(WidthDirective.ALLOWED_VALUES, stringValue) < 0) {
            this.logService.warn(`WidthDirective: Only ${WidthDirective.ALLOWED_VALUES} values could be accepted`);
        }
    }
}
WidthDirective.WIDTH_PREFIX = 'w';
WidthDirective.ALLOWED_VALUES = ['', '25', '50', '75', '100'];
WidthDirective.decorators = [
    { type: Directive, args: [{
                selector: '[arWidth]'
            },] },
];
/** @nocollapse */
WidthDirective.ctorParameters = () => [
    { type: Renderer2, },
    { type: ElementRef, },
    { type: LogService, },
];
WidthDirective.propDecorators = {
    "arWidth": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class BorderDirective {
    /**
     * @param {?} renderer2
     * @param {?} elementRef
     */
    constructor(renderer2, elementRef) {
        this.renderer2 = renderer2;
        this.elementRef = elementRef;
    }
    /**
     * @param {?} context
     * @return {?}
     */
    set arBorder(context) {
        if (this.currentContext !== context) {
            this.decorate(context);
        }
    }
    /**
     * @param {?} isRounded
     * @return {?}
     */
    set arBorderRounded(isRounded) {
        if (this.rounded === isRounded) {
            return;
        }
        this.setRounded(isRounded);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.renderer2.addClass(this.elementRef.nativeElement, BorderDirective.BORDER_PREFIX);
    }
    /**
     * @param {?} context
     * @return {?}
     */
    decorate(context) {
        this.renderer2.removeClass(this.elementRef.nativeElement, this.currentContextClass);
        this.currentContext = context;
        this.currentContextClass = context ? `${BorderDirective.BORDER_PREFIX}-${this.currentContext}` : '';
        if (this.currentContextClass) {
            this.renderer2.addClass(this.elementRef.nativeElement, this.currentContextClass);
        }
    }
    /**
     * @param {?} isRounded
     * @return {?}
     */
    setRounded(isRounded) {
        if (isRounded) {
            this.renderer2.addClass(this.elementRef.nativeElement, BorderDirective.ROUNDED_PREFIX);
        }
        else {
            this.renderer2.removeClass(this.elementRef.nativeElement, BorderDirective.ROUNDED_PREFIX);
        }
        this.rounded = isRounded;
    }
}
BorderDirective.BORDER_PREFIX = 'border';
BorderDirective.ROUNDED_PREFIX = 'rounded';
BorderDirective.decorators = [
    { type: Directive, args: [{
                selector: '[arBorder]'
            },] },
];
/** @nocollapse */
BorderDirective.ctorParameters = () => [
    { type: Renderer2, },
    { type: ElementRef, },
];
BorderDirective.propDecorators = {
    "arBorder": [{ type: Input },],
    "arBorderRounded": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class CardGroupComponent {
    constructor() {
        this.isGroup = true;
        this.isDeck = false;
        this.isColumns = false;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set deck(val) {
        this.isColumns = !val;
        this.isGroup = !val;
        this.isDeck = val;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set columns(val) {
        this.isColumns = val;
        this.isGroup = !val;
        this.isDeck = !val;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set group(val) {
        this.isColumns = !val;
        this.isGroup = val;
        this.isDeck = !val;
    }
}
CardGroupComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-card-group, [arCardGroup]',
                template: `
    <ng-content></ng-content>
  `,
                styles: [`:host.card-columns{display:block}`]
            },] },
];
/** @nocollapse */
CardGroupComponent.propDecorators = {
    "deck": [{ type: Input },],
    "columns": [{ type: Input },],
    "group": [{ type: Input },],
    "isGroup": [{ type: HostBinding, args: ['class.card-group',] },],
    "isDeck": [{ type: HostBinding, args: ['class.card-deck',] },],
    "isColumns": [{ type: HostBinding, args: ['class.card-columns',] },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class DeprecatedDirective {
    /**
     * @param {?} logService
     */
    constructor(logService) {
        this.logService = logService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.logService.warn('bsContext, bsSize properties were deprecated. Use arBsContext, arBsSize directives instead');
    }
}
DeprecatedDirective.decorators = [
    { type: Directive, args: [{
                // tslint:disable-next-line
                selector: '[bsSize]:not(ar-bs-group), [bsContext]:not(ar-bs-group)'
            },] },
];
/** @nocollapse */
DeprecatedDirective.ctorParameters = () => [
    { type: LogService, },
];
DeprecatedDirective.propDecorators = {
    "bsSize": [{ type: Input },],
    "bsContext": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ProgressBarComponent {
    /**
     * @param {?} contextualService
     */
    constructor(contextualService) {
        this.contextualService = contextualService;
        this.showValue = false;
        this.striped = false;
        this.active = false;
        this.baseClass = true;
        this.role = 'progressbar';
        this.ariaValueNow = '0';
        this.ariaValueMin = '0';
        this.ariaValueMax = '100';
        this._value = 0;
        contextualService.baseClass = ProgressBarComponent.BASE_CLASS;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set height(val) {
        this.hostStyle = val;
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set value(val) {
        this.ariaValueNow = String(val);
        this._value = val;
    }
    /**
     * @return {?}
     */
    get value() { return this._value; }
    /**
     * @return {?}
     */
    get valueString() {
        if (this.value < 0 || this.value > 100) {
            throw new Error('ProgressBarComponent: value should be a number between 0 and 100!');
        }
        return `${this.value}%`;
    }
    /**
     * @return {?}
     */
    get style() {
        return { width: this.valueString };
    }
}
ProgressBarComponent.BASE_CLASS = 'progress';
ProgressBarComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-progress-bar',
                template: `
    <div class="progress-bar"
         [class.progress-bar-striped]="striped"
         [class.progress-bar-animated]="active"
         role="progressbar"
         [attr.aria-valuenow]="value"
         aria-valuemin="0"
         aria-valuemax="100"
         [ngStyle]="style"
         [arBsBgContext]="contextualService.bsContext"
    >
      <ng-container *ngIf="showValue">
        {{ valueString }}
      </ng-container>
    </div>
  `,
                providers: [ContextualService]
            },] },
];
/** @nocollapse */
ProgressBarComponent.ctorParameters = () => [
    { type: ContextualService, },
];
ProgressBarComponent.propDecorators = {
    "showValue": [{ type: Input },],
    "striped": [{ type: Input },],
    "active": [{ type: Input },],
    "height": [{ type: Input },],
    "value": [{ type: Input },],
    "baseClass": [{ type: HostBinding, args: ['class.progress',] },],
    "role": [{ type: HostBinding, args: ['attr.role',] },],
    "ariaValueNow": [{ type: HostBinding, args: ['attr.aria-valuenow',] },],
    "ariaValueMin": [{ type: HostBinding, args: ['attr.aria-valuemin',] },],
    "ariaValueMax": [{ type: HostBinding, args: ['attr.aria-valuemax',] },],
    "hostStyle": [{ type: HostBinding, args: ['style.height.px',] },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class SpinnerComponent {
    /**
     * @param {?} state
     * @return {?}
     */
    set visible(state$$1) { this.state = state$$1; }
    /**
     * @return {?}
     */
    get visible() { return this.state; }
    /**
     * @param {?} state
     * @return {?}
     */
    set invisible(state$$1) {
        this.state = !state$$1;
    }
    /**
     * @return {?}
     */
    show() {
        this.state = true;
    }
    /**
     * @return {?}
     */
    hide() {
        this.state = false;
    }
}
SpinnerComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-spinner',
                template: `
    <div class="spinner" *ngIf="state"></div>
    <div [class.layout]="state">
      <ng-template *ngTemplateOutlet="templateRef"></ng-template>
      <ng-content></ng-content>
    </div>
  `,
                styles: [`@-webkit-keyframes spinner{to{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}@keyframes spinner{to{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}:host{position:relative;display:block;min-height:80px}.spinner{z-index:1;width:0;height:0;position:absolute;top:50%;left:50%}.spinner:before{content:'';-webkit-box-sizing:border-box;box-sizing:border-box;position:absolute;top:50%;left:50%;width:50px;height:50px;margin-top:-25px;margin-left:-25px;border-radius:50%;border:1px solid #f6f;border-top-color:#0e0;border-right-color:#0dd;border-bottom-color:#f90;-webkit-animation:.6s linear infinite spinner;animation:.6s linear infinite spinner}.layout{-webkit-filter:grayscale(100%);filter:grayscale(100%)}`]
            },] },
];
/** @nocollapse */
SpinnerComponent.propDecorators = {
    "visible": [{ type: Input },],
    "invisible": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class SpinnerDirective {
    /**
     * @param {?} factoryResolver
     * @param {?} templateRef
     * @param {?} viewContainer
     */
    constructor(factoryResolver, templateRef, viewContainer) {
        this.templateRef = templateRef;
        this.viewContainer = viewContainer;
        const /** @type {?} */ factory = factoryResolver.resolveComponentFactory(SpinnerComponent);
        this.component = factory.create(this.viewContainer.parentInjector);
        this.component.instance.templateRef = this.templateRef;
        this.viewContainer.insert(this.component.hostView);
    }
    /**
     * @param {?} condition
     * @return {?}
     */
    set arSpinner(condition) {
        this.component.instance.state = condition;
    }
}
SpinnerDirective.decorators = [
    { type: Directive, args: [{
                selector: '[arSpinner]'
            },] },
];
/** @nocollapse */
SpinnerDirective.ctorParameters = () => [
    { type: ComponentFactoryResolver, },
    { type: TemplateRef, },
    { type: ViewContainerRef, },
];
SpinnerDirective.propDecorators = {
    "arSpinner": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class SwitchComponent {
    constructor() {
        this.toggle = new EventEmitter();
    }
    /**
     * @return {?}
     */
    clickHandler() {
        if (this.disabled) {
            return;
        }
        this.toggle.emit();
    }
}
SwitchComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-switch',
                template: `
      <div class="onoffswitch" [class.disabled]="disabled" (click)="clickHandler()">
          <span class="onoffswitch-label" [class.checked]="active">
              <span class="onoffswitch-inner"></span>
              <span class="onoffswitch-switch"></span>
          </span>
      </div>
  `,
                styles: [`.onoffswitch{display:inline-block;position:relative;width:56px;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none}.onoffswitch-label{display:block;overflow:hidden;cursor:pointer;border:1px solid #fff;border-radius:10px;text-align:left}.onoffswitch-label.checked .onoffswitch-inner{margin-left:0}.onoffswitch-label.checked .onoffswitch-switch{right:0;background-color:#6bc95f}.onoffswitch-inner{display:block;width:200%;margin-left:-100%;-webkit-transition:margin .3s ease-in 0s;transition:margin .3s ease-in 0s}.onoffswitch-inner:after,.onoffswitch-inner:before{display:block;float:left;width:50%;height:19px;padding:0;line-height:19px;font-size:10px;font-family:Trebuchet,Arial,sans-serif;font-weight:700;-webkit-box-sizing:border-box;box-sizing:border-box}.onoffswitch-inner:before{content:"ON";padding-left:10px;background-color:#154975;color:#fff}.onoffswitch-inner:after{content:"OFF";padding-right:10px;background-color:#154975;color:#fff;text-align:right}.onoffswitch-switch{display:block;width:14px;margin:4px;background:#db3941;position:absolute;top:0;bottom:0;right:35px;border:1px solid #fff;border-radius:8px;-webkit-transition:all .3s ease-in 0s;transition:all .3s ease-in 0s}.onoffswitch.disabled .onoffswitch-switch{background-color:#cfcfcf}.onoffswitch.disabled .onoffswitch-inner:after,.onoffswitch.disabled .onoffswitch-inner:before{background-color:#cfcfcf;color:#fff}`]
            },] },
];
/** @nocollapse */
SwitchComponent.propDecorators = {
    "active": [{ type: Input },],
    "disabled": [{ type: Input },],
    "toggle": [{ type: Output },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class FormService {
    constructor() {
        this.id = 0;
    }
    /**
     * @return {?}
     */
    generateControlId() {
        return this.generateId(`${this.formId}_${FormService.CONTROL_PREFIX}`);
    }
    /**
     * @param {?} control_id
     * @param {?} value
     * @return {?}
     */
    generateSubControlId(control_id, value) {
        return `${control_id}_${value}`;
    }
    /**
     * @param {?} prefix
     * @return {?}
     */
    generateId(prefix) {
        this.id++;
        return `${prefix}_${this.id}`;
    }
}
FormService.FORM_ID_PREFIX = 'Form';
FormService.CONTROL_PREFIX = 'Input';
FormService.decorators = [
    { type: Injectable },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class FormComponent {
    /**
     * @param {?} formService
     * @param {?} logService
     */
    constructor(formService, logService) {
        this.formService = formService;
        this.logService = logService;
        this.isInline = false;
        this.autocomplete = 'off';
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set inline(val) {
        this.isInline = Boolean(val);
    }
    /**
     * @param {?} val
     * @return {?}
     */
    set bgContext(val) {
        this.formService.bsBgContext = val;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (!this.id) {
            this.logService.warn('Form id param required!');
        }
        this.formService.formId = this.id;
    }
}
FormComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-form, [arForm]',
                template: `
    <ng-content></ng-content>
  `,
                styles: [`[arForm].form-inline .form-group,ar-form.form-inline .form-group{margin-right:1rem!important}[arForm].form-inline .form-group:last-child,ar-form.form-inline .form-group:last-child{margin-right:0!important}`],
                providers: [FormService],
                encapsulation: ViewEncapsulation.None
            },] },
];
/** @nocollapse */
FormComponent.ctorParameters = () => [
    { type: FormService, },
    { type: LogService, },
];
FormComponent.propDecorators = {
    "inline": [{ type: Input },],
    "id": [{ type: Input },],
    "bgContext": [{ type: Input },],
    "isInline": [{ type: HostBinding, args: ['class.form-inline',] },],
    "autocomplete": [{ type: HostBinding, args: ['attr.autocomplete',] },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class RowComponent {
    /**
     * @param {?} formComponent
     */
    constructor(formComponent) {
        this.formComponent = formComponent;
        this.baseClass = true;
        this.formRow = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.formRow = Boolean(this.formComponent);
        this.baseClass = !this.formRow;
    }
}
RowComponent.ROW_CLASS = 'row';
RowComponent.FORM_ROW_CLASS = 'form-row';
RowComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-row, [arRow]',
                template: '<ng-content></ng-content>'
            },] },
];
/** @nocollapse */
RowComponent.ctorParameters = () => [
    { type: FormComponent, decorators: [{ type: Optional },] },
];
RowComponent.propDecorators = {
    "baseClass": [{ type: HostBinding, args: ['class.row',] },],
    "formRow": [{ type: HostBinding, args: ['class.form-row',] },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ColComponent {
    constructor() {
        this.className = true;
    }
}
ColComponent.decorators = [
    { type: Component, args: [{
                selector: 'ar-col',
                template: '<ng-content></ng-content>',
                styles: [`
    :host { display: block; }
  `]
            },] },
];
/** @nocollapse */
ColComponent.propDecorators = {
    "className": [{ type: HostBinding, args: ['class.col',] },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ScrollableDirective {
    /**
     * @param {?} renderer2
     * @param {?} elementRef
     */
    constructor(renderer2, elementRef) {
        this.renderer2 = renderer2;
        this.elementRef = elementRef;
    }
    /**
     * @param {?} height
     * @return {?}
     */
    set arScrollable(height) {
        this.currentHeight = height || 'auto';
        if (this.elementRef.nativeElement) {
            this.renderer2.setStyle(this.elementRef.nativeElement, 'height', this.currentHeight);
        }
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.renderer2.addClass(this.elementRef.nativeElement, ScrollableDirective.CLASS_NAME);
        this.renderer2.setStyle(this.elementRef.nativeElement, 'height', this.currentHeight);
        this.renderer2.setStyle(this.elementRef.nativeElement, 'display', 'block');
    }
}
ScrollableDirective.CLASS_NAME = 'ar-ui-scrollable-block';
ScrollableDirective.decorators = [
    { type: Directive, args: [{
                selector: '[arScrollable]'
            },] },
];
/** @nocollapse */
ScrollableDirective.ctorParameters = () => [
    { type: Renderer2, },
    { type: ElementRef, },
];
ScrollableDirective.propDecorators = {
    "arScrollable": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class UiModule {
}
UiModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    ReactiveFormsModule,
                    RouterModule,
                    CoreModule
                ],
                declarations: [
                    AlertComponent,
                    AlertAreaComponent,
                    AlertHeadingComponent,
                    HelpBlockComponent,
                    LayoutComponent,
                    LoremIpsumComponent,
                    SimpleLayoutComponent,
                    ButtonComponent,
                    ButtonGroupComponent,
                    BsGroupComponent,
                    NavBarComponent,
                    NavBarNavComponent,
                    NavBarNavItemComponent,
                    NavBarNavDropdownItemComponent,
                    BsSizeDirective,
                    BsContextDirective,
                    BsBgContextDirective,
                    BsTextContextDirective,
                    TextAlignDirective,
                    WidthDirective,
                    BorderDirective,
                    DeprecatedDirective,
                    SpinnerDirective,
                    ScrollableDirective,
                    DropdownComponent,
                    DropdownMenuComponent,
                    DropdownMenuItemComponent,
                    DropdownDividerComponent,
                    IconComponent,
                    FigureComponent,
                    InnerLinkComponent,
                    OuterLinkComponent,
                    BlockComponent,
                    TabComponent,
                    TabLayoutComponent,
                    TabLinkComponent,
                    PageHeaderComponent,
                    PagingListSizeComponent,
                    PaginationComponent,
                    PagingTextComponent,
                    PagingControlComponent,
                    MailLinkComponent,
                    ModalLayoutComponent,
                    ModalComponent,
                    ModalHeaderComponent,
                    PanelComponent,
                    CardComponent,
                    CardGroupComponent,
                    ProgressBarComponent,
                    SpinnerComponent,
                    SwitchComponent,
                    RowComponent,
                    ColComponent
                ],
                exports: [
                    AlertComponent,
                    AlertHeadingComponent,
                    HelpBlockComponent,
                    LayoutComponent,
                    LoremIpsumComponent,
                    SimpleLayoutComponent,
                    ButtonComponent,
                    ButtonGroupComponent,
                    BsGroupComponent,
                    NavBarComponent,
                    NavBarNavComponent,
                    NavBarNavItemComponent,
                    NavBarNavDropdownItemComponent,
                    BsSizeDirective,
                    BsContextDirective,
                    BsBgContextDirective,
                    BsTextContextDirective,
                    TextAlignDirective,
                    WidthDirective,
                    BorderDirective,
                    DeprecatedDirective,
                    SpinnerDirective,
                    ScrollableDirective,
                    DropdownComponent,
                    DropdownMenuComponent,
                    DropdownMenuItemComponent,
                    DropdownDividerComponent,
                    IconComponent,
                    FigureComponent,
                    InnerLinkComponent,
                    OuterLinkComponent,
                    BlockComponent,
                    TabComponent,
                    TabLayoutComponent,
                    TabLinkComponent,
                    PageHeaderComponent,
                    PagingListSizeComponent,
                    PaginationComponent,
                    PagingControlComponent,
                    MailLinkComponent,
                    ModalComponent,
                    ModalHeaderComponent,
                    PanelComponent,
                    CardComponent,
                    CardGroupComponent,
                    ProgressBarComponent,
                    SpinnerComponent,
                    SwitchComponent,
                    RowComponent,
                    ColComponent
                ],
                providers: [
                    IconService
                ],
                entryComponents: [
                    SpinnerComponent
                ]
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
// Argon
// export * from './ui/UiModule';
// export * from './tools/index';
// export * from './tables/index';
// export * from './form/index';
// export * from './core/index';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * Generated bundle index. Do not edit.
 */

export { CoreModule, UiModule, DictionaryDirective as ɵi, InfinityScrollDirective as ɵf, ScrollContainerDirective as ɵg, dictionaryServiceFactory as ɵl, messageBusServiceFactory as ɵk, DictionaryPipe as ɵc, TrimPipe as ɵa, TruncatePipe as ɵb, AlertService as ɵm, DictionaryService as ɵd, LogService as ɵe, MessageBusService as ɵj, QueryOptionsBuilderService as ɵn, ScrollCollectionService as ɵh, FormComponent as ɵcx, FormService as ɵcy, AlertComponent as ɵo, AlertAreaComponent as ɵu, AlertHeadingComponent as ɵw, BlockComponent as ɵcb, BsGroupComponent as ɵs, ButtonComponent as ɵbc, ButtonGroupComponent as ɵbd, CardComponent as ɵbz, CardGroupComponent as ɵcs, ColComponent as ɵcz, DropdownComponent as ɵbs, DropdownDividerComponent as ɵbv, DropdownMenuComponent as ɵbt, DropdownMenuItemComponent as ɵbu, FigureComponent as ɵbx, HelpBlockComponent as ɵx, IconComponent as ɵbw, InnerLinkComponent as ɵby, LayoutComponent as ɵy, LoremIpsumComponent as ɵba, MailLinkComponent as ɵck, ModalComponent as ɵco, ModalHeaderComponent as ɵcq, ModalLayoutComponent as ɵcl, NavBarComponent as ɵbe, NavBarNavComponent as ɵbg, NavBarNavDropdownItemComponent as ɵbi, NavBarNavItemComponent as ɵbh, OuterLinkComponent as ɵca, PageHeaderComponent as ɵcf, PaginationComponent as ɵch, PagingControlComponent as ɵcj, PagingListSizeComponent as ɵcg, PagingTextComponent as ɵci, PanelComponent as ɵcr, ProgressBarComponent as ɵct, RowComponent as ɵcw, SpinnerComponent as ɵcu, SwitchComponent as ɵcv, TabComponent as ɵcc, TabLayoutComponent as ɵcd, TabLinkComponent as ɵce, BorderDirective as ɵbo, BsBgContextDirective as ɵbj, BsContextDirective as ɵq, BsSizeDirective as ɵt, BsTextContextDirective as ɵbl, ClassNameDirective as ɵr, DeprecatedDirective as ɵbp, ScrollableDirective as ɵbr, SpinnerDirective as ɵbq, TextAlignDirective as ɵbm, WidthDirective as ɵbn, modalServiceFactory as ɵcn, SimpleLayoutComponent as ɵbb, ModalDialogConfigModel as ɵcp, AlertsAreaService as ɵv, ContextualService as ɵp, IconService as ɵz, ModalService as ɵcm, ToggleService as ɵbf, BsTextContextEnum as ɵbk };
//# sourceMappingURL=argon.js.map
