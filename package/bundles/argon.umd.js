(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('lodash'), require('rxjs'), require('rxjs/operators'), require('@angular/common/http'), require('qs'), require('@angular/animations'), require('@angular/forms'), require('@angular/common'), require('@angular/router')) :
	typeof define === 'function' && define.amd ? define('argon', ['exports', '@angular/core', 'lodash', 'rxjs', 'rxjs/operators', '@angular/common/http', 'qs', '@angular/animations', '@angular/forms', '@angular/common', '@angular/router'], factory) :
	(factory((global.argon = {}),global.ng.core,global.lodash,global.rxjs,global.Rx.Observable.prototype,global.ng.common.http,global.qs,global.ng.animations,global.ng.forms,global.ng.common,global.ng.router));
}(this, (function (exports,core,lodash,rxjs,operators,http,qs,animations,forms,common,router) { 'use strict';

/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0
THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.
See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */
var extendStatics = Object.setPrototypeOf ||
    ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
    function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var TrimPipe = /** @class */ (function () {
    function TrimPipe() {
    }
    TrimPipe.prototype.transform = function (value, chars, mode) {
        switch (mode) {
            case TrimPipe.MODE_START:
                return lodash.trimStart(String(value), chars);
            case TrimPipe.MODE_END:
                return lodash.trimEnd(String(value), chars);
            default:
                return lodash.trim(String(value), chars);
        }
    };
    return TrimPipe;
}());
TrimPipe.MODE_START = 'start';
TrimPipe.MODE_END = 'end';
TrimPipe.decorators = [
    { type: core.Pipe, args: [{
                name: 'arTrim'
            },] },
];
var LogService = /** @class */ (function () {
    function LogService() {
    }
    LogService.prototype.log = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (core.isDevMode()) {
            console.log.apply(null, args);
        }
    };
    LogService.prototype.warn = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (core.isDevMode()) {
            console.warn.apply(null, args);
        }
    };
    return LogService;
}());
LogService.decorators = [
    { type: core.Injectable },
];
var MessageBusService = /** @class */ (function () {
    function MessageBusService(logService) {
        this.logService = logService;
        this.bus = new rxjs.Subject();
    }
    MessageBusService.prototype.of = function (messageClass) {
        return this.bus.pipe(operators.filter(function (message) { return message.isMessageType(messageClass); }), operators.map(function (message) { return (message); }));
    };
    MessageBusService.prototype.send = function (message) {
        this.logService.log('MessageBusService: send', message);
        this.bus.next(message);
    };
    return MessageBusService;
}());
MessageBusService.decorators = [
    { type: core.Injectable },
];
MessageBusService.ctorParameters = function () { return [
    { type: LogService, },
]; };
var INFO = 'info';
var PRIMARY = 'primary';
var SECONDARY = 'secondary';
var SUCCESS = 'success';
var WARNING = 'warning';
var DANGER = 'danger';
var DEFAULT = 'default';
var LIGHT = 'light';
var DARK = 'dark';
var LINK = 'link';
var BsContextEnum = { INFO: INFO, PRIMARY: PRIMARY, SUCCESS: SUCCESS, WARNING: WARNING, DANGER: DANGER, DEFAULT: DEFAULT, SECONDARY: SECONDARY, LIGHT: LIGHT, DARK: DARK, LINK: LINK };
var BsContextArray = lodash.values(BsContextEnum);
var DisplayAlertMessage = /** @class */ (function () {
    function DisplayAlertMessage(message, bsContext, closeable, displayTime) {
        if (bsContext === void 0) { bsContext = 'info'; }
        if (closeable === void 0) { closeable = true; }
        if (displayTime === void 0) { displayTime = DisplayAlertMessage.DEFAULT_DISPLAY_TIME; }
        this.message = message;
        this.bsContext = bsContext;
        this.closeable = closeable;
        this.displayTime = displayTime;
        this.type = DisplayAlertMessage.type;
    }
    Object.defineProperty(DisplayAlertMessage.prototype, "isSimple", {
        get: function () { return !this.template; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DisplayAlertMessage.prototype, "shouldAutohide", {
        get: function () { return this.displayTime > 0; },
        enumerable: true,
        configurable: true
    });
    DisplayAlertMessage.prototype.isMessageType = function (messageClass) {
        return messageClass.type === this.type;
    };
    DisplayAlertMessage.prototype.setTemplate = function (template, context) {
        this.template = template;
        this.templateContext = context;
    };
    return DisplayAlertMessage;
}());
DisplayAlertMessage.DEFAULT_DISPLAY_TIME = 5000;
DisplayAlertMessage.type = 'DisplayAlertMessage';
var AlertService = /** @class */ (function () {
    function AlertService(messageService) {
        this.messageService = messageService;
    }
    AlertService.prototype.showAlert = function (props) {
        this.messageService.send(this.createMessage(props));
    };
    AlertService.prototype.createMessage = function (props) {
        var message = new DisplayAlertMessage(props.message, props.bsContext, props.closeable, props.displayTime);
        if (props.template) {
            message.setTemplate(props.template, props.templateContext);
        }
        return message;
    };
    return AlertService;
}());
AlertService.TYPES = BsContextEnum;
AlertService.decorators = [
    { type: core.Injectable },
];
AlertService.ctorParameters = function () { return [
    { type: MessageBusService, },
]; };
var TruncatePipe = /** @class */ (function () {
    function TruncatePipe() {
    }
    TruncatePipe.prototype.transform = function (value, length) {
        if (!length) {
            return value;
        }
        return lodash.truncate(String(value), {
            separator: ' ',
            length: length
        });
    };
    return TruncatePipe;
}());
TruncatePipe.decorators = [
    { type: core.Pipe, args: [{
                name: 'arTruncate'
            },] },
];
var InfinityScrollDirective = /** @class */ (function () {
    function InfinityScrollDirective(element, zone) {
        this.element = element;
        this.zone = zone;
        this.infinityScroll = new core.EventEmitter();
        this.scrollEvent = new rxjs.Subject();
    }
    InfinityScrollDirective.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.zone.runOutsideAngular(function () {
            rxjs.fromEvent(_this.element.nativeElement, 'scroll')
                .pipe(operators.debounceTime(50))
                .subscribe(function (event) {
                var nativeElement = _this.element.nativeElement;
                var scrollHeight = nativeElement.scrollHeight - nativeElement.clientHeight;
                var scrollTop = nativeElement.scrollTop;
                var scrollRatio = scrollTop / scrollHeight;
                _this.infinityScroll.emit(scrollRatio);
                _this.scrollEvent.next(scrollRatio);
            });
        });
    };
    return InfinityScrollDirective;
}());
InfinityScrollDirective.decorators = [
    { type: core.Directive, args: [{
                selector: '[arInfinityScroll]'
            },] },
];
InfinityScrollDirective.ctorParameters = function () { return [
    { type: core.ElementRef, },
    { type: core.NgZone, },
]; };
InfinityScrollDirective.propDecorators = {
    "infinityScroll": [{ type: core.Output },],
};
var ScrollCollectionService = /** @class */ (function () {
    function ScrollCollectionService(ngZone) {
        var _this = this;
        this.ngZone = ngZone;
        this._collection = [];
        this.renderToTop = function () {
            if (_this._middleIndex <= _this.minIndex || lodash.size(_this._collection) === 0) {
                return;
            }
            var startIndex = _this._middleIndex - _this.minIndex - _this.offsetSize;
            var itemsToAdd = _this._collection.slice(startIndex < 0 ? 0 : startIndex, _this._middleIndex - _this.minIndex);
            var renderedItems = _this._collection.slice(_this._middleIndex - _this.minIndex, _this._middleIndex + _this.minIndex);
            var collectionToChange = lodash.concat(itemsToAdd, renderedItems);
            var collectionSize = lodash.size(collectionToChange);
            for (var i = collectionSize - 1; i >= _this.viewElementCount; i--) {
                collectionToChange[i].shouldDetach = true;
            }
            _this._middleIndex -= _this.offsetSize;
            if (_this._middleIndex < _this.minIndex) {
                _this._middleIndex = _this.minIndex;
            }
            _this.renderCollection(collectionToChange, true);
            _this.containerElement.nativeElement.scrollTop = _this.topScrollSize;
        };
        this.renderToBottom = function () {
            if (_this._middleIndex >= _this.maxIndex || lodash.size(_this._collection) === 0) {
                return;
            }
            var itemsToAdd = _this._collection.slice(_this._middleIndex + _this.minIndex, _this._middleIndex + _this.minIndex + _this.offsetSize);
            var renderedItems = _this._collection.slice(_this._middleIndex - _this.minIndex, _this._middleIndex + _this.minIndex);
            var collectionToChange = lodash.concat(renderedItems, itemsToAdd);
            var collectionSize = lodash.size(collectionToChange);
            for (var i = 0; i < collectionSize - _this.viewElementCount; i++) {
                collectionToChange[i].shouldDetach = true;
            }
            _this._middleIndex += _this.offsetSize;
            if (_this._middleIndex > _this.maxIndex) {
                _this._middleIndex = _this.maxIndex;
            }
            _this.renderCollection(collectionToChange);
        };
    }
    Object.defineProperty(ScrollCollectionService.prototype, "viewElementCount", {
        get: function () {
            return this.options.viewElementCount || ScrollCollectionService.DEFAULT_VIEW_ELEMENT_COUNT;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ScrollCollectionService.prototype, "offsetSize", {
        get: function () {
            return this.options.offsetSize || ScrollCollectionService.DEFAULT_OFFSET_SIZE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ScrollCollectionService.prototype, "itemsContainer", {
        get: function () {
            return this.options.itemsContainer;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ScrollCollectionService.prototype, "template", {
        get: function () {
            return this.options.template;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ScrollCollectionService.prototype, "containerElement", {
        get: function () {
            return this.options.containerElement;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ScrollCollectionService.prototype, "collection", {
        get: function () {
            return lodash.map(this._collection, function (item) { return item.data; });
        },
        set: function (val) {
            this.destroyCollection(this._collection);
            this._collection = this.createCollection(val);
            if (this.initialized) {
                this.itemsContainer.clear();
                this._middleIndex = this.minIndex;
                var collectionToRender = lodash.slice(this._collection, 0, this.viewElementCount);
                this.renderCollection(collectionToRender);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ScrollCollectionService.prototype, "scrollRatio", {
        set: function (val) {
            if (val > 0.99) {
                this.ngZone.run(this.renderToBottom);
            }
            else if (val < 0.01) {
                this.ngZone.run(this.renderToTop);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ScrollCollectionService.prototype, "minIndex", {
        get: function () {
            return Math.ceil(this.viewElementCount / 2);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ScrollCollectionService.prototype, "maxIndex", {
        get: function () {
            return this.size - this.minIndex;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ScrollCollectionService.prototype, "size", {
        get: function () {
            return lodash.size(this._collection);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ScrollCollectionService.prototype, "hasItems", {
        get: function () {
            return this.size > 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ScrollCollectionService.prototype, "initialized", {
        get: function () {
            return Boolean(this.options);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ScrollCollectionService.prototype, "topScrollSize", {
        get: function () {
            var nativeElement = this.containerElement.nativeElement;
            var scrollHeight = nativeElement.scrollHeight - nativeElement.clientHeight;
            return scrollHeight * (this.offsetSize / this.viewElementCount);
        },
        enumerable: true,
        configurable: true
    });
    ScrollCollectionService.prototype.initialize = function (options) {
        this.options = options;
        this._middleIndex = this.minIndex;
        this.collection = this.options.initialCollection || this.collection;
        this.options.initialCollection = [];
    };
    ScrollCollectionService.prototype.destroy = function () {
        this.destroyCollection(this._collection);
    };
    ScrollCollectionService.prototype.renderCollection = function (collection, reverseRender) {
        var _this = this;
        if (reverseRender === void 0) { reverseRender = false; }
        var collectionToRender = reverseRender ? lodash.reverse(collection) : collection;
        lodash.each(collectionToRender, function (item) {
            var index = item.viewRef ? _this.itemsContainer.indexOf(item.viewRef) : -1;
            if (item.shouldDetach && index >= 0) {
                _this.removeElement(item, index);
            }
            else if (index < 0) {
                _this.renderElement(item, reverseRender ? 0 : null);
            }
        });
    };
    ScrollCollectionService.prototype.renderElement = function (item, index) {
        if (item.viewRef) {
            this.itemsContainer.insert(item.viewRef, index);
        }
        else {
            item.viewRef = this.itemsContainer.createEmbeddedView(this.template, { $implicit: item.data }, index);
        }
    };
    ScrollCollectionService.prototype.removeElement = function (item, index) {
        item.shouldDetach = false;
        this.itemsContainer.detach(index);
    };
    ScrollCollectionService.prototype.destroyCollection = function (collection) {
        lodash.each(collection, function (item) {
            if (item.viewRef && !item.viewRef.destroyed) {
                item.viewRef.destroy();
            }
        });
    };
    ScrollCollectionService.prototype.createCollection = function (options) {
        return lodash.map(options, function (data) { return ({ data: data }); });
    };
    return ScrollCollectionService;
}());
ScrollCollectionService.DEFAULT_VIEW_ELEMENT_COUNT = 20;
ScrollCollectionService.DEFAULT_OFFSET_SIZE = 5;
ScrollCollectionService.decorators = [
    { type: core.Injectable },
];
ScrollCollectionService.ctorParameters = function () { return [
    { type: core.NgZone, },
]; };
var ScrollContainerDirective = /** @class */ (function () {
    function ScrollContainerDirective(container, template, scrollService, scrollDirective) {
        this.container = container;
        this.template = template;
        this.scrollService = scrollService;
        this.scrollDirective = scrollDirective;
    }
    Object.defineProperty(ScrollContainerDirective.prototype, "arScrollContainer", {
        set: function (val) {
            this.scrollService.collection = val;
        },
        enumerable: true,
        configurable: true
    });
    ScrollContainerDirective.prototype.ngOnInit = function () {
        var _this = this;
        if (!this.scrollDirective) {
            throw Error('InfinityScrollDirective in parent node required!');
        }
        this.scrollService.initialize({
            template: this.template,
            itemsContainer: this.container,
            containerElement: this.scrollDirective.element,
            viewElementCount: 50,
            offsetSize: 10
        });
        this.scrollSubscription = this.scrollDirective.scrollEvent.subscribe(function (ratio) {
            _this.scrollService.scrollRatio = ratio;
        });
    };
    ScrollContainerDirective.prototype.ngOnDestroy = function () {
        this.scrollService.destroy();
        if (this.scrollSubscription) {
            this.scrollSubscription.unsubscribe();
        }
    };
    return ScrollContainerDirective;
}());
ScrollContainerDirective.decorators = [
    { type: core.Directive, args: [{
                selector: '[arScrollContainer]',
                providers: [ScrollCollectionService]
            },] },
];
ScrollContainerDirective.ctorParameters = function () { return [
    { type: core.ViewContainerRef, },
    { type: core.TemplateRef, },
    { type: ScrollCollectionService, },
    { type: InfinityScrollDirective, },
]; };
ScrollContainerDirective.propDecorators = {
    "arScrollContainer": [{ type: core.Input },],
};
var messageBusService;
var messageBusServiceFactory = function (logService) {
    if (!messageBusService) {
        messageBusService = new MessageBusService(logService);
    }
    return messageBusService;
};
var DictionaryService = /** @class */ (function () {
    function DictionaryService(logService) {
        this.logService = logService;
        this.dictionaries = new Map();
    }
    DictionaryService.prototype.add = function (key, dictionary) {
        var hasDictionary = this.dictionaries.has(key);
        var source = lodash.isArray(dictionary)
            ? rxjs.of((dictionary)).pipe(operators.shareReplay(1))
            : ((dictionary)).pipe(operators.shareReplay(1));
        if (hasDictionary) {
            var dictionarySubject = this.dictionaries.get(key);
            dictionarySubject.next(source);
        }
        else {
            var dictionarySubject = new rxjs.BehaviorSubject(source);
            this.dictionaries.set(key, dictionarySubject);
        }
    };
    DictionaryService.prototype.get = function (key) {
        if (!this.dictionaries.has(key)) {
            this.logService.warn("DictionaryService: Dictionary " + key + " does not exist. Return raw dictionary");
            this.add(key, []);
        }
        return this.dictionaries.get(key).asObservable()
            .pipe(operators.mergeMap(function (data) { return data; }));
    };
    DictionaryService.prototype.remove = function (key) {
        if (!this.dictionaries.has(key)) {
            this.logService.warn("DictionaryService: Dictionary " + key + " does not exist. Nothing removed.");
        }
        else {
            this.dictionaries.get(key).next(rxjs.of([]).pipe(operators.shareReplay(1)));
        }
    };
    return DictionaryService;
}());
DictionaryService.decorators = [
    { type: core.Injectable },
];
DictionaryService.ctorParameters = function () { return [
    { type: LogService, },
]; };
var DictionaryPipe = /** @class */ (function () {
    function DictionaryPipe(dictionaryService) {
        this.dictionaryService = dictionaryService;
    }
    DictionaryPipe.prototype.transform = function (value, key) {
        return this.dictionaryService.get(key).pipe(operators.map(function (dictionary) {
            var item = lodash.find(dictionary, { value: value });
            return item ? item.label : '';
        }));
    };
    return DictionaryPipe;
}());
DictionaryPipe.decorators = [
    { type: core.Pipe, args: [{
                name: 'arDictionary'
            },] },
];
DictionaryPipe.ctorParameters = function () { return [
    { type: DictionaryService, },
]; };
var DictionaryDirective = /** @class */ (function () {
    function DictionaryDirective(container, template, dictionaryService) {
        var _this = this;
        this.container = container;
        this.template = template;
        this.dictionaryService = dictionaryService;
        this._dictionary = [];
        this.setDictionary = function (dictionary) {
            _this._dictionary = dictionary;
            _this.updateView();
        };
    }
    Object.defineProperty(DictionaryDirective.prototype, "arDictionaryName", {
        set: function (value) {
            if (this._currentDictionaryKey === value) {
                return;
            }
            this.subscribeToDictionary(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DictionaryDirective.prototype, "arDictionary", {
        set: function (value) {
            if (value === this._currentValue) {
                return;
            }
            this._currentValue = value;
            this.updateView();
        },
        enumerable: true,
        configurable: true
    });
    DictionaryDirective.prototype.ngOnDestroy = function () {
        this.unsubscribeFromDictionary();
    };
    DictionaryDirective.prototype.subscribeToDictionary = function (value) {
        this.unsubscribeFromDictionary();
        this._currentDictionaryKey = value;
        if (!this._currentDictionaryKey) {
            return;
        }
        this._dictionarySubscription = this.dictionaryService.get(this._currentDictionaryKey).subscribe(this.setDictionary);
    };
    DictionaryDirective.prototype.unsubscribeFromDictionary = function () {
        if (this._dictionarySubscription) {
            this._dictionarySubscription.unsubscribe();
        }
        this._dictionarySubscription = null;
    };
    DictionaryDirective.prototype.updateView = function () {
        var nextItem = this.getDictionaryItem();
        if (nextItem === this._currentDictionaryItem) {
            return;
        }
        this._currentDictionaryItem = nextItem;
        this.container.clear();
        if (nextItem) {
            this.container.createEmbeddedView(this.template, { $implicit: nextItem });
        }
    };
    DictionaryDirective.prototype.getDictionaryItem = function () {
        var _this = this;
        return lodash.find(this._dictionary, function (item) { return item.value === _this._currentValue; });
    };
    return DictionaryDirective;
}());
DictionaryDirective.decorators = [
    { type: core.Directive, args: [{
                selector: '[arDictionary]'
            },] },
];
DictionaryDirective.ctorParameters = function () { return [
    { type: core.ViewContainerRef, },
    { type: core.TemplateRef, },
    { type: DictionaryService, },
]; };
DictionaryDirective.propDecorators = {
    "arDictionaryName": [{ type: core.Input },],
    "arDictionary": [{ type: core.Input },],
};
var dictionaryService;
var dictionaryServiceFactory = function (logService) {
    if (!dictionaryService) {
        dictionaryService = new DictionaryService(logService);
    }
    return dictionaryService;
};
var QueryOptionsBuilderService = /** @class */ (function () {
    function QueryOptionsBuilderService() {
    }
    QueryOptionsBuilderService.prototype.createHttpParams = function (queryParams, options) {
        return new http.HttpParams(({
            fromString: this.stringify(queryParams, options)
        }));
    };
    QueryOptionsBuilderService.prototype.parse = function (source, options) {
        return qs.parse(source, options);
    };
    QueryOptionsBuilderService.prototype.stringify = function (object, options) {
        return qs.stringify(object, options);
    };
    return QueryOptionsBuilderService;
}());
QueryOptionsBuilderService.decorators = [
    { type: core.Injectable },
];
var ɵ0 = messageBusServiceFactory;
var ɵ1 = dictionaryServiceFactory;
var CoreModule = /** @class */ (function () {
    function CoreModule() {
    }
    return CoreModule;
}());
CoreModule.decorators = [
    { type: core.NgModule, args: [{
                imports: [],
                exports: [
                    TrimPipe,
                    TruncatePipe,
                    DictionaryPipe,
                    InfinityScrollDirective,
                    ScrollContainerDirective,
                    DictionaryDirective,
                ],
                declarations: [
                    TrimPipe,
                    TruncatePipe,
                    DictionaryPipe,
                    InfinityScrollDirective,
                    ScrollContainerDirective,
                    DictionaryDirective,
                ],
                providers: [
                    { provide: MessageBusService, useFactory: ɵ0, deps: [LogService] },
                    { provide: DictionaryService, useFactory: ɵ1, deps: [LogService] },
                    AlertService,
                    LogService,
                    QueryOptionsBuilderService,
                ]
            },] },
];
var ClassNameDirective = /** @class */ (function () {
    function ClassNameDirective(renderer2, elementRef, parentDirective) {
        var _this = this;
        this.renderer2 = renderer2;
        this.elementRef = elementRef;
        this.parentDirective = parentDirective;
        this.change = new rxjs.Subject();
        this.decorate = function (context) {
            _this.change.next(context);
            _this.context = _this.createEnumTypeInstance(context);
            _this.originalContext = context;
            var nextContextClassName = _this.createContextClassName(_this.originalClassName, _this.context);
            if (String(_this.contextClassName) === String(nextContextClassName)) {
                return;
            }
            if (_this.contextClassName) {
                _this.renderer2.removeClass(_this.elementRef.nativeElement, _this.contextClassName);
            }
            if (nextContextClassName) {
                _this.contextClassName = nextContextClassName;
                _this.renderer2.addClass(_this.elementRef.nativeElement, _this.contextClassName);
            }
        };
        var classList = this.elementRef.nativeElement.classList;
        this.originalClassName = classList ? (lodash.first(classList)) : '';
    }
    Object.defineProperty(ClassNameDirective.prototype, "baseClassName", {
        get: function () {
            return this.originalClassName;
        },
        set: function (val) {
            this.originalClassName = val;
            this.decorate(this.originalContext);
        },
        enumerable: true,
        configurable: true
    });
    ClassNameDirective.prototype.ngOnDestroy = function () {
        this.unsubscribeFromParent();
    };
    ClassNameDirective.prototype.subscribeToParent = function () {
        if (!this.parentSubscription && this.parentDirective) {
            this.parentSubscription = this.parentDirective.change.subscribe(this.decorate);
        }
    };
    ClassNameDirective.prototype.unsubscribeFromParent = function () {
        if (this.parentSubscription) {
            this.parentSubscription.unsubscribe();
            this.parentSubscription = null;
        }
    };
    return ClassNameDirective;
}());
var BsGroupComponent = /** @class */ (function () {
    function BsGroupComponent() {
    }
    return BsGroupComponent;
}());
BsGroupComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-bs-group, [arBsGroup]',
                template: "    \n    <ng-content></ng-content>",
            },] },
];
BsGroupComponent.propDecorators = {
    "bsSize": [{ type: core.Input },],
    "bsContext": [{ type: core.Input },],
    "bsBgContext": [{ type: core.Input },],
    "bsTextContext": [{ type: core.Input },],
    "textAlign": [{ type: core.Input },],
};
var BsContextDirective = /** @class */ (function (_super) {
    __extends(BsContextDirective, _super);
    function BsContextDirective(renderer2, elementRef, bsContextGroup, parentDirective) {
        var _this = _super.call(this, renderer2, elementRef, parentDirective) || this;
        _this.bsContextGroup = bsContextGroup;
        _this.parentDirective = parentDirective;
        _this.prefix = '';
        return _this;
    }
    Object.defineProperty(BsContextDirective.prototype, "arBsContextPrefix", {
        get: function () {
            return this.prefix;
        },
        set: function (prefix) {
            this.prefix = prefix || '';
            this.decorate(this.originalContext);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BsContextDirective.prototype, "arBsContext", {
        get: function () {
            return this.originalContext;
        },
        set: function (context) {
            var nextContext = context;
            if (!nextContext && this.bsContextGroup) {
                nextContext = (this.bsContextGroup.bsContext);
                this.unsubscribeFromParent();
            }
            else if (!nextContext && this.parentDirective && this.parentDirective.arBsContext) {
                nextContext = this.parentDirective.arBsContext
                    ? this.parentDirective.arBsContext
                    : ('');
                this.subscribeToParent();
            }
            else {
                this.unsubscribeFromParent();
            }
            this.decorate(nextContext);
        },
        enumerable: true,
        configurable: true
    });
    BsContextDirective.prototype.createContextClassName = function (originalClass, contextValue) {
        if (!originalClass) {
            return '';
        }
        return this.prefix
            ? originalClass + "-" + this.prefix + "-" + contextValue
            : originalClass + "-" + contextValue;
    };
    BsContextDirective.prototype.createEnumTypeInstance = function (context) {
        return String(context);
    };
    return BsContextDirective;
}(ClassNameDirective));
BsContextDirective.decorators = [
    { type: core.Directive, args: [{
                selector: '[arBsContext]'
            },] },
];
BsContextDirective.ctorParameters = function () { return [
    { type: core.Renderer2, },
    { type: core.ElementRef, },
    { type: BsGroupComponent, decorators: [{ type: core.Optional },] },
    { type: BsContextDirective, decorators: [{ type: core.Optional }, { type: core.SkipSelf },] },
]; };
BsContextDirective.propDecorators = {
    "arBsContextPrefix": [{ type: core.Input },],
    "arBsContext": [{ type: core.Input },],
};
var BsSizeDirective = /** @class */ (function (_super) {
    __extends(BsSizeDirective, _super);
    function BsSizeDirective(renderer2, elementRef, bsSizeGroup, parentDirective) {
        var _this = _super.call(this, renderer2, elementRef, parentDirective) || this;
        _this.bsSizeGroup = bsSizeGroup;
        _this.parentDirective = parentDirective;
        _this.prefix = '';
        return _this;
    }
    Object.defineProperty(BsSizeDirective.prototype, "arBsSizePrefix", {
        get: function () {
            return this.prefix;
        },
        set: function (prefix) {
            this.prefix = prefix || '';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BsSizeDirective.prototype, "arBsSize", {
        get: function () {
            return this.originalContext;
        },
        set: function (context) {
            var nextContext = context;
            if (!nextContext && this.bsSizeGroup) {
                nextContext = (this.bsSizeGroup.bsSize);
                this.unsubscribeFromParent();
            }
            else if (!nextContext && this.parentDirective) {
                nextContext = this.parentDirective.arBsSize
                    ? this.parentDirective.arBsSize
                    : ('');
                this.subscribeToParent();
            }
            else {
                this.unsubscribeFromParent();
            }
            this.decorate(nextContext);
        },
        enumerable: true,
        configurable: true
    });
    BsSizeDirective.prototype.createContextClassName = function (originalClass, contextValue) {
        if (!originalClass) {
            return '';
        }
        return this.prefix
            ? originalClass + "-" + this.prefix + "-" + contextValue
            : originalClass + "-" + contextValue;
    };
    BsSizeDirective.prototype.createEnumTypeInstance = function (context) {
        return String(context);
    };
    return BsSizeDirective;
}(ClassNameDirective));
BsSizeDirective.decorators = [
    { type: core.Directive, args: [{
                selector: '[arBsSize]'
            },] },
];
BsSizeDirective.ctorParameters = function () { return [
    { type: core.Renderer2, },
    { type: core.ElementRef, },
    { type: BsGroupComponent, decorators: [{ type: core.Optional },] },
    { type: BsSizeDirective, decorators: [{ type: core.Optional }, { type: core.SkipSelf },] },
]; };
BsSizeDirective.propDecorators = {
    "arBsSizePrefix": [{ type: core.Input },],
    "arBsSize": [{ type: core.Input },],
};
var ContextualService = /** @class */ (function () {
    function ContextualService(bsContextDirective, bsSizeDirective) {
        this.bsContextDirective = bsContextDirective;
        this.bsSizeDirective = bsSizeDirective;
    }
    Object.defineProperty(ContextualService.prototype, "baseClass", {
        set: function (val) {
            this.setBaseClassName(this.bsContextDirective, val);
            this.setBaseClassName(this.bsSizeDirective, val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContextualService.prototype, "bsContext", {
        get: function () {
            return this.bsContextDirective
                ? this.bsContextDirective.arBsContext
                : '';
        },
        set: function (val) {
            if (this.bsContextDirective) {
                this.bsContextDirective.arBsContext = val;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContextualService.prototype, "bsContextPrefix", {
        get: function () {
            return this.bsContextDirective
                ? this.bsContextDirective.arBsContextPrefix
                : '';
        },
        set: function (val) {
            if (this.bsContextDirective) {
                this.bsContextDirective.arBsContextPrefix = val;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContextualService.prototype, "bsSizePrefix", {
        get: function () {
            return this.bsSizeDirective
                ? this.bsSizeDirective.arBsSizePrefix
                : '';
        },
        set: function (val) {
            if (this.bsSizeDirective) {
                this.bsSizeDirective.arBsSizePrefix = val;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContextualService.prototype, "bsSize", {
        get: function () {
            return this.bsSizeDirective
                ? this.bsSizeDirective.arBsSize
                : '';
        },
        set: function (val) {
            if (this.bsSizeDirective) {
                this.bsSizeDirective.arBsSize = val;
            }
        },
        enumerable: true,
        configurable: true
    });
    ContextualService.prototype.setBaseClassName = function (contextualDirective, className) {
        if (contextualDirective) {
            contextualDirective.baseClassName = className;
        }
    };
    return ContextualService;
}());
ContextualService.decorators = [
    { type: core.Injectable },
];
ContextualService.ctorParameters = function () { return [
    { type: BsContextDirective, decorators: [{ type: core.Host }, { type: core.Self }, { type: core.Optional },] },
    { type: BsSizeDirective, decorators: [{ type: core.Host }, { type: core.Self }, { type: core.Optional },] },
]; };
var ButtonGroupComponent = /** @class */ (function () {
    function ButtonGroupComponent(contextualService) {
        this.contextualService = contextualService;
    }
    Object.defineProperty(ButtonGroupComponent.prototype, "bsContext", {
        get: function () { return this.contextualService.bsContext; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ButtonGroupComponent.prototype, "bsContextPrefix", {
        get: function () { return this.contextualService.bsContextPrefix; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ButtonGroupComponent.prototype, "bsSize", {
        get: function () { return this.contextualService.bsSize; },
        enumerable: true,
        configurable: true
    });
    return ButtonGroupComponent;
}());
ButtonGroupComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-button-group',
                template: "    \n    <div class=\"btn-toolbar\"\n         [class.btn-group-vertical]=\"vertical\"\n         [class.btn-group]=\"!vertical\"\n    >\n      <ng-content></ng-content>\n    </div>\n  ",
                changeDetection: core.ChangeDetectionStrategy.OnPush,
                providers: [ContextualService],
            },] },
];
ButtonGroupComponent.ctorParameters = function () { return [
    { type: ContextualService, },
]; };
ButtonGroupComponent.propDecorators = {
    "vertical": [{ type: core.Input },],
};
var ButtonComponent = /** @class */ (function () {
    function ButtonComponent(contextualService, buttonGroup) {
        this.contextualService = contextualService;
        this.buttonGroup = buttonGroup;
        this.click = new core.EventEmitter();
        this.className = true;
        this.blockClassName = false;
        this.disabledClassName = false;
        this.role = 'button';
        this.attrDisabled = false;
        contextualService.baseClass = ButtonComponent.BASE_CLASS;
    }
    Object.defineProperty(ButtonComponent.prototype, "disabled", {
        set: function (val) {
            this.attrDisabled = val;
            this.disabledClassName = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ButtonComponent.prototype, "block", {
        set: function (val) {
            this.blockClassName = val;
        },
        enumerable: true,
        configurable: true
    });
    ButtonComponent.prototype.ngOnInit = function () {
        if (this.buttonGroup) {
            this.contextualService.bsContext = this.buttonGroup.bsContext;
            this.contextualService.bsSize = this.buttonGroup.bsSize;
            this.contextualService.bsContextPrefix = this.buttonGroup.bsContextPrefix;
        }
    };
    return ButtonComponent;
}());
ButtonComponent.BASE_CLASS = 'btn';
ButtonComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-button, [arButton]',
                template: "\n    <ng-content></ng-content>\n  ",
                styles: [":host{pointer-events:auto}:host.disabled{pointer-events:none}"],
                providers: [ContextualService],
                changeDetection: core.ChangeDetectionStrategy.OnPush
            },] },
];
ButtonComponent.ctorParameters = function () { return [
    { type: ContextualService, },
    { type: ButtonGroupComponent, decorators: [{ type: core.Optional },] },
]; };
ButtonComponent.propDecorators = {
    "disabled": [{ type: core.Input },],
    "block": [{ type: core.Input },],
    "click": [{ type: core.Output },],
    "className": [{ type: core.HostBinding, args: ['class.btn',] },],
    "blockClassName": [{ type: core.HostBinding, args: ['class.btn-block',] },],
    "disabledClassName": [{ type: core.HostBinding, args: ['class.disabled',] },],
    "role": [{ type: core.HostBinding, args: ['attr.role',] },],
    "attrDisabled": [{ type: core.HostBinding, args: ['attr.disabled',] },],
};
var SimpleLayoutComponent = /** @class */ (function () {
    function SimpleLayoutComponent() {
    }
    return SimpleLayoutComponent;
}());
SimpleLayoutComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-simple-layout',
                template: "\n    <header>\n      <ng-content select=\"[header]\"></ng-content>\n    </header>\n    <section>\n      <ng-content></ng-content>\n    </section>\n    <footer>\n      <ng-content select=\"[footer]\"></ng-content>\n    </footer>\n  ",
                styles: [":host{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;min-height:100vh}:host section{padding:10px}:host footer{margin-top:auto}"]
            },] },
];
var BsTextContextDirective = /** @class */ (function () {
    function BsTextContextDirective(renderer2, elementRef, bsContextGroup) {
        this.renderer2 = renderer2;
        this.elementRef = elementRef;
        this.bsContextGroup = bsContextGroup;
    }
    BsTextContextDirective.getClassName = function (context) {
        return BsTextContextDirective.TEXT_PREFIX + "-" + context;
    };
    Object.defineProperty(BsTextContextDirective.prototype, "arBsTextContext", {
        set: function (context) {
            var nextContext = context;
            if (this.bsContextGroup && !nextContext) {
                nextContext = (this.bsContextGroup.bsTextContext);
            }
            if (nextContext !== this.currentContext) {
                this.decorate(nextContext);
            }
        },
        enumerable: true,
        configurable: true
    });
    BsTextContextDirective.prototype.decorate = function (nextContext) {
        this.renderer2.removeClass(this.elementRef.nativeElement, this.currentClass);
        this.currentContext = nextContext;
        this.currentClass = BsTextContextDirective.getClassName(this.currentContext);
        this.renderer2.addClass(this.elementRef.nativeElement, this.currentClass);
    };
    return BsTextContextDirective;
}());
BsTextContextDirective.TEXT_PREFIX = 'text';
BsTextContextDirective.decorators = [
    { type: core.Directive, args: [{
                selector: '[arBsTextContext]'
            },] },
];
BsTextContextDirective.ctorParameters = function () { return [
    { type: core.Renderer2, },
    { type: core.ElementRef, },
    { type: BsGroupComponent, decorators: [{ type: core.Optional },] },
]; };
BsTextContextDirective.propDecorators = {
    "arBsTextContext": [{ type: core.Input },],
};
var INFO$1 = 'info';
var PRIMARY$1 = 'primary';
var SECONDARY$1 = 'secondary';
var SUCCESS$1 = 'success';
var WARNING$1 = 'warning';
var DANGER$1 = 'danger';
var DEFAULT$1 = '';
var LIGHT$1 = 'light';
var DARK$1 = 'dark';
var LINK$1 = 'link';
var WHITE = 'white';
var MUTED = 'muted';
var BsTextContextEnum = { INFO: INFO$1, PRIMARY: PRIMARY$1, SUCCESS: SUCCESS$1, WARNING: WARNING$1, DANGER: DANGER$1, DEFAULT: DEFAULT$1, SECONDARY: SECONDARY$1, LIGHT: LIGHT$1, DARK: DARK$1, LINK: LINK$1, WHITE: WHITE, MUTED: MUTED };
var BsTextContextArray = lodash.values(BsTextContextEnum);
var BsBgContextDirective = /** @class */ (function () {
    function BsBgContextDirective(renderer2, elementRef, bsContextGroup) {
        this.renderer2 = renderer2;
        this.elementRef = elementRef;
        this.bsContextGroup = bsContextGroup;
    }
    BsBgContextDirective.getTextColor = function (bsContext, defaultValue) {
        if (defaultValue === void 0) { defaultValue = BsTextContextEnum.DARK; }
        return lodash.get(BsBgContextDirective.COLOR_MAP, bsContext, defaultValue);
    };
    Object.defineProperty(BsBgContextDirective.prototype, "arBsBgContext", {
        set: function (context) {
            var nextContext = context;
            if (this.bsContextGroup && !nextContext) {
                nextContext = (this.bsContextGroup.bsBgContext);
            }
            if (nextContext !== this.currentContext) {
                this.decorate(nextContext);
            }
        },
        enumerable: true,
        configurable: true
    });
    BsBgContextDirective.prototype.decorate = function (nextContext) {
        this.renderer2.removeClass(this.elementRef.nativeElement, this.currentBgClass);
        this.renderer2.removeClass(this.elementRef.nativeElement, this.currentTextClass);
        this.currentContext = nextContext;
        this.currentBgClass = BsBgContextDirective.BG_PREFIX + "-" + this.currentContext;
        this.currentTextClass = this.getTextClass(nextContext);
        this.renderer2.addClass(this.elementRef.nativeElement, this.currentBgClass);
        this.renderer2.addClass(this.elementRef.nativeElement, this.currentTextClass);
    };
    BsBgContextDirective.prototype.getTextClass = function (bgContext) {
        var context = BsBgContextDirective.getTextColor(bgContext);
        return context
            ? BsTextContextDirective.getClassName((context))
            : '';
    };
    return BsBgContextDirective;
}());
BsBgContextDirective.BG_PREFIX = 'bg';
BsBgContextDirective.COLOR_MAP = {
    danger: BsTextContextEnum.WHITE,
    dark: BsTextContextEnum.WHITE,
    'default': BsTextContextEnum.DARK,
    light: BsTextContextEnum.DARK,
    info: BsTextContextEnum.WHITE,
    link: BsTextContextEnum.DARK,
    primary: BsTextContextEnum.WHITE,
    secondary: BsTextContextEnum.WHITE,
    warning: BsTextContextEnum.DARK,
    success: BsTextContextEnum.WHITE,
};
BsBgContextDirective.decorators = [
    { type: core.Directive, args: [{
                selector: '[arBsBgContext]'
            },] },
];
BsBgContextDirective.ctorParameters = function () { return [
    { type: core.Renderer2, },
    { type: core.ElementRef, },
    { type: BsGroupComponent, decorators: [{ type: core.Optional },] },
]; };
BsBgContextDirective.propDecorators = {
    "arBsBgContext": [{ type: core.Input },],
};
var ToggleService = /** @class */ (function () {
    function ToggleService() {
        this.state = ToggleService.INACTIVE_STATE;
    }
    Object.defineProperty(ToggleService.prototype, "boolState", {
        get: function () {
            return this.state === ToggleService.ACTIVE_STATE;
        },
        enumerable: true,
        configurable: true
    });
    ToggleService.prototype.toggle = function () {
        this.state = this.state === ToggleService.ACTIVE_STATE
            ? ToggleService.INACTIVE_STATE : ToggleService.ACTIVE_STATE;
    };
    ToggleService.prototype.open = function () {
        this.state = ToggleService.ACTIVE_STATE;
    };
    ToggleService.prototype.close = function () {
        this.state = ToggleService.INACTIVE_STATE;
    };
    return ToggleService;
}());
ToggleService.ACTIVE_STATE = 'active';
ToggleService.INACTIVE_STATE = 'inactive';
ToggleService.ANIMATION_HEIGHT = (animations.trigger('toggleServiceState', [
    animations.state('active', animations.style({ height: '*', padding: '*' })),
    animations.state('inactive', animations.style({ height: 0, overflow: 'hidden', padding: 0 })),
    animations.transition('active <=> inactive', animations.animate('100ms ease-out')),
    animations.transition(':enter', [
        animations.style({ height: '*' }),
        animations.animate('1000ms ease-out')
    ]),
    animations.transition(':leave', [
        animations.style({ height: 0, overflow: 'hidden' }),
        animations.animate('1000ms ease-out')
    ])
]));
ToggleService.ANIMATION_OPACITY = (animations.trigger('toggleServiceState', [
    animations.state('active', animations.style({ opacity: 1 })),
    animations.state('inactive', animations.style({ opacity: 0 })),
    animations.transition('active <=> inactive', animations.animate('200ms ease-out')),
    animations.transition(':enter', [
        animations.style({ opacity: 0 }),
        animations.animate('200ms ease-in', animations.style({ opacity: 1 }))
    ]),
    animations.transition(':leave', [
        animations.style({ opacity: 1 }),
        animations.animate('200ms ease-out', animations.style({ opacity: 0 }))
    ])
]));
ToggleService.decorators = [
    { type: core.Injectable },
];
var NavBarComponent = /** @class */ (function () {
    function NavBarComponent(toggleService, contextualService) {
        this.toggleService = toggleService;
        this.contextualService = contextualService;
        this.baseClass = true;
        this.contextualService.bsSizePrefix = 'expand';
        this.contextualService.baseClass = NavBarComponent.BASE_CLASS;
        toggleService.open();
    }
    Object.defineProperty(NavBarComponent.prototype, "navBarClass", {
        get: function () {
            return '';
        },
        enumerable: true,
        configurable: true
    });
    return NavBarComponent;
}());
NavBarComponent.BASE_CLASS = 'navbar';
NavBarComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-nav-bar',
                template: "\n    <div class=\"navbar-brand\">\n      <ng-content select=\"[navbar-brand]\"></ng-content>\n    </div>\n    <button class=\"navbar-toggler\" (click)=\"toggleService.toggle()\">\n      <span class=\"navbar-toggler-icon\"></span>\n    </button>\n    <div class=\"navbar-collapse\" [@toggleServiceState]=\"toggleService.state\">\n      <ng-content></ng-content>\n      <div class=\"form-inline\">\n        <ng-content select=\"[form-inline]\"></ng-content>\n      </div>\n    </div>\n  ",
                animations: [ToggleService.ANIMATION_HEIGHT],
                providers: [ToggleService, ContextualService],
                styles: ["\n    .form-inline {\n      margin-left: auto;\n    }\n  "]
            },] },
];
NavBarComponent.ctorParameters = function () { return [
    { type: ToggleService, },
    { type: ContextualService, },
]; };
NavBarComponent.propDecorators = {
    "baseClass": [{ type: core.HostBinding, args: ['class.navbar',] },],
};
var NavBarNavComponent = /** @class */ (function () {
    function NavBarNavComponent() {
    }
    return NavBarNavComponent;
}());
NavBarNavComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-nav-bar-nav',
                template: "\n    <ul class=\"navbar-nav\">\n      <ng-content></ng-content>\n    </ul>\n  "
            },] },
];
var NavBarNavItemComponent = /** @class */ (function () {
    function NavBarNavItemComponent() {
    }
    return NavBarNavItemComponent;
}());
NavBarNavItemComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-nav-bar-nav-item',
                template: "\n    <li class=\"nav-item\">\n      <span class=\"nav-link\">\n        <ng-content></ng-content>\n      </span>\n    </li>\n  "
            },] },
];
var IconService = /** @class */ (function () {
    function IconService(rendererFactory2) {
        var _this = this;
        this.rendererFactory2 = rendererFactory2;
        this.icons = {};
        this.add = function (path, id, viewBox) {
            if (viewBox === void 0) { viewBox = '0 0 24 24'; }
            if (!_this.sprite) {
                _this.icons[id] = path;
                return;
            }
            var symbol = _this.renderer.createElement('symbol', 'svg');
            _this.renderer.setAttribute(symbol, 'id', id);
            _this.renderer.setAttribute(symbol, 'viewBox', viewBox);
            var pathElement = _this.renderer.createElement('path', 'svg');
            _this.renderer.setAttribute(pathElement, 'd', path);
            _this.renderer.appendChild(symbol, pathElement);
            _this.renderer.appendChild(_this.sprite.nativeElement, symbol);
        };
        this.renderer = this.rendererFactory2.createRenderer(null, null);
    }
    IconService.prototype.initialize = function (element) {
        this.sprite = element;
        this.addMany(this.icons);
    };
    IconService.prototype.addMany = function (data) {
        var _this = this;
        this.icons = Object.assign({}, this.icons, data);
        if (!this.sprite) {
            return;
        }
        lodash.each(data, function (path, id) { _this.add(path, id); });
    };
    IconService.prototype.getIconsList = function () {
        return lodash.keys(this.icons);
    };
    return IconService;
}());
IconService.decorators = [
    { type: core.Injectable },
];
IconService.ctorParameters = function () { return [
    { type: core.RendererFactory2, },
]; };
var LayoutComponent = /** @class */ (function () {
    function LayoutComponent(iconService, element) {
        this.iconService = iconService;
        this.element = element;
        this.documentClick = new rxjs.Subject();
    }
    LayoutComponent.prototype.ngOnInit = function () {
        this.iconService.initialize(this.iconsSprite);
    };
    LayoutComponent.prototype.closeHandler = function (event) {
        this.documentClick.next(event);
    };
    return LayoutComponent;
}());
LayoutComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-layout',
                template: "\n    <section class=\"argon-layout\" [@inOutState]>\n      <!-- App content -->\n      <ng-content></ng-content>\n      <!-- Modal layout -->\n      <ar-modal-layout></ar-modal-layout>\n      <!-- Alerts -->\n      <ar-alert-area></ar-alert-area>\n      <!-- Icons -->\n      <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" #iconsSprite style=\"display: none;\"></svg>\n    </section>\n  ",
                styles: ["body::-webkit-scrollbar-track{-webkit-box-shadow:inset 0 0 6px rgba(0,0,0,.3);background-color:#f8f9fa;border-radius:10px}body::-webkit-scrollbar{width:10px;height:10px;background-color:#f5f5f5;border-radius:10px}body::-webkit-scrollbar-thumb{border-radius:10px;background-image:-webkit-gradient(linear,left bottom,left top,color-stop(.44,#7a8793),color-stop(.72,#56606a),color-stop(.86,#343a40))}.ar-ui-scrollable-block{overflow-y:auto;overflow-x:hidden}.ar-ui-scrollable-block::-webkit-scrollbar-track{-webkit-box-shadow:inset 0 0 6px rgba(0,0,0,.3);background-color:#f8f9fa;border-radius:10px}.ar-ui-scrollable-block::-webkit-scrollbar{width:10px;height:10px;background-color:#f5f5f5;border-radius:10px}.ar-ui-scrollable-block::-webkit-scrollbar-thumb{border-radius:10px;background-image:-webkit-gradient(linear,left bottom,left top,color-stop(.44,#7a8793),color-stop(.72,#56606a),color-stop(.86,#343a40))}.btn+.btn:not(.btn-block){margin-left:.25rem}.btn-toolbar .btn+.btn:not(.btn-block){margin-left:inherit}.ar-form-autocomplete{position:relative}.ar-form-autocomplete .ar-ui-dropdown{position:static}.form-control.disabled{background-color:#e9ecef;opacity:1}", ".argon-layout{overflow-x:hidden;display:block}.icons-sprite{display:none}"],
                encapsulation: core.ViewEncapsulation.None,
                animations: [animations.trigger('inOutState', [
                        animations.transition(':enter', [
                            animations.style({ opacity: 0 }),
                            animations.animate('1000ms ease-in', animations.style({ opacity: 1 }))
                        ]),
                        animations.transition(':leave', [
                            animations.style({ opacity: 1 }),
                            animations.animate('1000ms ease-out', animations.style({ opacity: 0 }))
                        ])
                    ])
                ],
            },] },
];
LayoutComponent.ctorParameters = function () { return [
    { type: IconService, },
    { type: core.ElementRef, },
]; };
LayoutComponent.propDecorators = {
    "iconsSprite": [{ type: core.ViewChild, args: ['iconsSprite',] },],
    "closeHandler": [{ type: core.HostListener, args: ['document:click', ['$event'],] },],
};
var DropdownComponent = /** @class */ (function () {
    function DropdownComponent(element, layout, ref, toggleService) {
        var _this = this;
        this.element = element;
        this.layout = layout;
        this.ref = ref;
        this.toggleService = toggleService;
        this.closeHandler = function (event) {
            if (_this.element.nativeElement.contains(event.target)) {
                return;
            }
            if (_this.opened) {
                _this.close();
                _this.ref.markForCheck();
            }
        };
    }
    Object.defineProperty(DropdownComponent.prototype, "opened", {
        get: function () {
            return this.toggleService.boolState;
        },
        enumerable: true,
        configurable: true
    });
    DropdownComponent.prototype.ngOnInit = function () {
        this.clickSubscription = this.layout.documentClick.subscribe(this.closeHandler);
    };
    DropdownComponent.prototype.ngOnDestroy = function () {
        if (this.clickSubscription) {
            this.clickSubscription.unsubscribe();
            this.clickSubscription = null;
        }
    };
    DropdownComponent.prototype.close = function () {
        this.toggleService.close();
    };
    DropdownComponent.prototype.toggle = function () {
        if (this.disabled) {
            this.close();
        }
        else {
            this.toggleService.toggle();
        }
    };
    return DropdownComponent;
}());
DropdownComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-dropdown',
                template: "\n    <div class=\"dropdown ar-ui-dropdown\" [class.dropup]=\"dropUp\" [class.full-width]=\"fullWidth\">\n      <div class=\"dropdown-menu-selector\" (click)=\"toggle()\">\n        <ng-content select=\"[title]\"></ng-content>\n      </div>\n      <div\n        *ngIf=\"opened\"\n        class=\"dropdown-menu\"\n        [@toggleServiceState]=\"toggleService.state\"\n      >\n        <ng-content></ng-content>\n      </div>\n    </div>\n  ",
                styles: [".dropdown.full-width .dropdown-menu-selector{display:block}.dropdown.full-width .dropdown-menu{width:100%}.dropdown-menu-selector{cursor:pointer;display:inline-block}.dropdown-menu{display:block;padding:5px;overflow-y:auto}.dropdown-menu::-webkit-scrollbar-track{-webkit-box-shadow:inset 0 0 6px rgba(0,0,0,.3);background-color:#f8f9fa;border-radius:10px}.dropdown-menu::-webkit-scrollbar{width:10px;height:10px;background-color:#f5f5f5;border-radius:10px}.dropdown-menu::-webkit-scrollbar-thumb{border-radius:10px;background-image:-webkit-gradient(linear,left bottom,left top,color-stop(.44,#7a8793),color-stop(.72,#56606a),color-stop(.86,#343a40))}.dropup .dropdown-menu{top:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}"],
                animations: [ToggleService.ANIMATION_OPACITY],
                providers: [ToggleService]
            },] },
];
DropdownComponent.ctorParameters = function () { return [
    { type: core.ElementRef, },
    { type: LayoutComponent, },
    { type: core.ChangeDetectorRef, },
    { type: ToggleService, },
]; };
DropdownComponent.propDecorators = {
    "disabled": [{ type: core.Input },],
    "fullWidth": [{ type: core.Input },],
    "dropUp": [{ type: core.Input },],
};
var DropdownMenuComponent = /** @class */ (function (_super) {
    __extends(DropdownMenuComponent, _super);
    function DropdownMenuComponent(contextualService, element, layout, ref, toggleService) {
        var _this = _super.call(this, element, layout, ref, toggleService) || this;
        _this.contextualService = contextualService;
        _this.element = element;
        _this.layout = layout;
        _this.ref = ref;
        _this.toggleService = toggleService;
        return _this;
    }
    return DropdownMenuComponent;
}(DropdownComponent));
DropdownMenuComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-dropdown-menu',
                template: "\n      <div class=\"dropdown\" [class.dropup]=\"dropUp\" [class.full-width]=\"fullWidth\">\n        <ar-button class=\"dropdown-toggle\"\n                  [arBsContext]=\"contextualService.bsContext\"\n                  [arBsSize]=\"contextualService.bsSize\"\n                  (click)=\"toggle()\"\n        >\n          <ng-content select=\"[title]\"></ng-content>\n        </ar-button>\n        <ul\n          class=\"dropdown-menu\"\n          [@toggleServiceState]=\"toggleService.state\"\n          *ngIf=\"opened\"\n        >\n          <ng-content></ng-content>\n        </ul>\n      </div>\n  ",
                styles: [".dropdown.full-width .dropdown-menu-selector{display:block}.dropdown.full-width .dropdown-menu{width:100%}.dropdown-menu-selector{cursor:pointer;display:inline-block}.dropdown-menu{display:block;padding:5px;overflow-y:auto}.dropdown-menu::-webkit-scrollbar-track{-webkit-box-shadow:inset 0 0 6px rgba(0,0,0,.3);background-color:#f8f9fa;border-radius:10px}.dropdown-menu::-webkit-scrollbar{width:10px;height:10px;background-color:#f5f5f5;border-radius:10px}.dropdown-menu::-webkit-scrollbar-thumb{border-radius:10px;background-image:-webkit-gradient(linear,left bottom,left top,color-stop(.44,#7a8793),color-stop(.72,#56606a),color-stop(.86,#343a40))}.dropup .dropdown-menu{top:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}"],
                providers: [ContextualService, ToggleService],
                animations: [ToggleService.ANIMATION_OPACITY]
            },] },
];
DropdownMenuComponent.ctorParameters = function () { return [
    { type: ContextualService, },
    { type: core.ElementRef, },
    { type: LayoutComponent, },
    { type: core.ChangeDetectorRef, },
    { type: ToggleService, },
]; };
DropdownMenuComponent.propDecorators = {
    "disabled": [{ type: core.Input },],
    "fullWidth": [{ type: core.Input },],
    "dropUp": [{ type: core.Input },],
};
var NavBarNavDropdownItemComponent = /** @class */ (function () {
    function NavBarNavDropdownItemComponent() {
    }
    NavBarNavDropdownItemComponent.prototype.close = function () {
        this.dropdown.close();
    };
    return NavBarNavDropdownItemComponent;
}());
NavBarNavDropdownItemComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-nav-bar-nav-dropdown-item',
                template: "\n    <li class=\"nav-item\">\n      <ar-dropdown #dropdown>\n        <ng-container title>\n          <span class=\"nav-link dropdown-toggle\">\n            <ng-content select=\"[title]\"></ng-content>\n          </span>    \n        </ng-container>\n        <ng-content></ng-content>\n      </ar-dropdown>\n    </li>\n  "
            },] },
];
NavBarNavDropdownItemComponent.propDecorators = {
    "dropdown": [{ type: core.ViewChild, args: ['dropdown',] },],
};
var DropdownMenuItemComponent = /** @class */ (function () {
    function DropdownMenuItemComponent(dropdown, dropdownMenu, contextualService) {
        this.dropdown = dropdown;
        this.dropdownMenu = dropdownMenu;
        this.contextualService = contextualService;
        this.closeOnSelect = true;
        this.className = true;
        this.disabledClassName = false;
    }
    Object.defineProperty(DropdownMenuItemComponent.prototype, "disabled", {
        set: function (val) {
            this.disabledClassName = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DropdownMenuItemComponent.prototype, "parentDropdown", {
        get: function () {
            if (this.dropdown) {
                return this.dropdown;
            }
            if (this.dropdownMenu) {
                return this.dropdownMenu;
            }
            return null;
        },
        enumerable: true,
        configurable: true
    });
    DropdownMenuItemComponent.prototype.handleClick = function (e) {
        e.stopPropagation();
        if (this.disabledClassName) {
            return;
        }
        if (this.closeOnSelect && this.parentDropdown) {
            this.parentDropdown.close();
        }
    };
    return DropdownMenuItemComponent;
}());
DropdownMenuItemComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-dropdown-menu-item',
                template: "\n    <div class=\"prefix-block\" [arBsBgContext]=\"contextualService.bsContext\"></div>\n    <ng-content></ng-content>\n    <div class=\"pull-right\">\n      <ng-content select=\"[right]\"></ng-content>\n    </div>\n  ",
                styles: [".dropdown.full-width .dropdown-menu-selector{display:block}.dropdown.full-width .dropdown-menu{width:100%}.dropdown-menu-selector{cursor:pointer;display:inline-block}.dropdown-menu{display:block;padding:5px;overflow-y:auto}.dropdown-menu::-webkit-scrollbar-track{-webkit-box-shadow:inset 0 0 6px rgba(0,0,0,.3);background-color:#f8f9fa;border-radius:10px}.dropdown-menu::-webkit-scrollbar{width:10px;height:10px;background-color:#f5f5f5;border-radius:10px}.dropdown-menu::-webkit-scrollbar-thumb{border-radius:10px;background-image:-webkit-gradient(linear,left bottom,left top,color-stop(.44,#7a8793),color-stop(.72,#56606a),color-stop(.86,#343a40))}.dropup .dropdown-menu{top:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}:host{cursor:pointer;pointer-events:auto;margin-left:-5px;width:calc(100% + 10px);position:relative}:host .prefix-block{width:5px;position:absolute;left:0;top:0;bottom:0;opacity:0;-webkit-transition:opacity .2s ease-in;transition:opacity .2s ease-in}:host:hover .prefix-block{opacity:1}:host.disabled{pointer-events:none}:host.disabled:hover .prefix-block{opacity:0}"],
                providers: [ContextualService]
            },] },
];
DropdownMenuItemComponent.ctorParameters = function () { return [
    { type: DropdownComponent, decorators: [{ type: core.Optional },] },
    { type: DropdownMenuComponent, decorators: [{ type: core.Optional },] },
    { type: ContextualService, },
]; };
DropdownMenuItemComponent.propDecorators = {
    "disabled": [{ type: core.Input },],
    "closeOnSelect": [{ type: core.Input },],
    "className": [{ type: core.HostBinding, args: ['class.dropdown-item',] },],
    "disabledClassName": [{ type: core.HostBinding, args: ['class.disabled',] },],
    "handleClick": [{ type: core.HostListener, args: ['click', ['$event'],] },],
};
var IconComponent = /** @class */ (function () {
    function IconComponent(contextualService) {
        this.contextualService = contextualService;
        this.className = true;
        contextualService.baseClass = IconComponent.BASE_CLASS;
    }
    Object.defineProperty(IconComponent.prototype, "svgStyle", {
        get: function () {
            var style$$1 = {};
            if (this.size) {
                style$$1['width.px'] = this.size;
                style$$1['height.px'] = this.size;
            }
            return style$$1;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(IconComponent.prototype, "link", {
        get: function () { return "#" + this.name; },
        enumerable: true,
        configurable: true
    });
    return IconComponent;
}());
IconComponent.BASE_CLASS = 'ar-icon';
IconComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-icon',
                template: "\n    <svg xmlns=\"http://www.w3.org/2000/svg\" #svg [ngStyle]=\"svgStyle\">\n      <use [attr.xlink:href]=\"link\" *ngIf=\"name\" />\n    </svg>\n  ",
                styles: [":host{display:inline-block;vertical-align:middle;line-height:0}:host svg{width:24px;height:24px}:host.ar-icon-xl svg{width:48px;height:48px}:host.ar-icon-lg svg{width:30px;height:30px}:host.ar-icon-md svg{width:24px;height:24px}:host.ar-icon-xs svg{width:20px;height:20px}:host.ar-icon-sm svg{width:16px;height:16px}:host.ar-icon-primary svg{fill:#007bff}:host.ar-icon-secondary svg{fill:#6c757d}:host.ar-icon-success svg{fill:#28a745}:host.ar-icon-info svg{fill:#17a2b8}:host.ar-icon-warning svg{fill:#ffc107}:host.ar-icon-danger svg{fill:#dc3545}:host.ar-icon-light svg{fill:#f8f9fa}:host.ar-icon-dark svg{fill:#343a40}:host.ar-icon-white svg{fill:#fff}"],
                providers: [ContextualService]
            },] },
];
IconComponent.ctorParameters = function () { return [
    { type: ContextualService, },
]; };
IconComponent.propDecorators = {
    "name": [{ type: core.Input },],
    "size": [{ type: core.Input },],
    "className": [{ type: core.HostBinding, args: ['class.ar-icon',] },],
};
var FigureComponent = /** @class */ (function () {
    function FigureComponent() {
    }
    return FigureComponent;
}());
FigureComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-figure',
                template: "\n    <figure class=\"figure\">\n      <ng-content></ng-content>\n      <figcaption class=\"figure-caption\" [arTextAlign]=\"captionAlign\">\n        <ng-content select=\"[caption]\"></ng-content>\n      </figcaption>\n    </figure>\n  "
            },] },
];
FigureComponent.propDecorators = {
    "captionAlign": [{ type: core.Input },],
};
var TextAlignDirective = /** @class */ (function () {
    function TextAlignDirective(renderer2, elementRef, bsGroup) {
        this.renderer2 = renderer2;
        this.elementRef = elementRef;
        this.bsGroup = bsGroup;
    }
    Object.defineProperty(TextAlignDirective.prototype, "arTextAlign", {
        set: function (direction) {
            var nextDirection = (direction);
            if (!nextDirection && this.bsGroup) {
                nextDirection = this.bsGroup.textAlign;
            }
            if (this.currentValue === nextDirection) {
                return;
            }
            this.decorate(nextDirection);
        },
        enumerable: true,
        configurable: true
    });
    TextAlignDirective.prototype.decorate = function (nextDirection) {
        if (this.currentClass) {
            this.renderer2.removeClass(this.elementRef.nativeElement, this.currentClass);
        }
        this.currentClass = nextDirection ? "text-" + nextDirection : '';
        this.currentValue = nextDirection;
        if (this.currentClass) {
            this.renderer2.addClass(this.elementRef.nativeElement, this.currentClass);
        }
    };
    return TextAlignDirective;
}());
TextAlignDirective.decorators = [
    { type: core.Directive, args: [{
                selector: '[arTextAlign]'
            },] },
];
TextAlignDirective.ctorParameters = function () { return [
    { type: core.Renderer2, },
    { type: core.ElementRef, },
    { type: BsGroupComponent, decorators: [{ type: core.Optional },] },
]; };
TextAlignDirective.propDecorators = {
    "arTextAlign": [{ type: core.Input },],
};
var AlertComponent = /** @class */ (function () {
    function AlertComponent(contextualService) {
        this.close = new core.EventEmitter();
        this.className = true;
        this.role = 'alert';
        contextualService.baseClass = AlertComponent.BASE_CLASS;
    }
    return AlertComponent;
}());
AlertComponent.BASE_CLASS = 'alert';
AlertComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-alert',
                template: "\n    <button *ngIf=\"closeable\" type=\"button\" class=\"close\" (click)=\"close.emit()\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n    <ng-content></ng-content>\n  ",
                styles: ["\n    :host { display: block; }\n  "],
                providers: [
                    ContextualService
                ]
            },] },
];
AlertComponent.ctorParameters = function () { return [
    { type: ContextualService, },
]; };
AlertComponent.propDecorators = {
    "closeable": [{ type: core.Input },],
    "close": [{ type: core.Output },],
    "className": [{ type: core.HostBinding, args: ['class.alert',] },],
    "role": [{ type: core.HostBinding, args: ['attr.role',] },],
};
var CardComponent = /** @class */ (function () {
    function CardComponent(toggleService, contextualService) {
        this.toggleService = toggleService;
        this.showHeader = false;
        this.showFooter = false;
        this.change = new core.EventEmitter();
        this.className = true;
        contextualService.baseClass = CardComponent.BASE_CLASS;
    }
    Object.defineProperty(CardComponent.prototype, "hasTopImage", {
        get: function () { return Boolean(this.image) && !this.imageBottom; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CardComponent.prototype, "hasBottomImage", {
        get: function () { return Boolean(this.image) && this.imageBottom; },
        enumerable: true,
        configurable: true
    });
    CardComponent.prototype.ngOnInit = function () {
        this.toggleService.state = this.collapsible && this.defaultCollapsed
            ? ToggleService.INACTIVE_STATE
            : ToggleService.ACTIVE_STATE;
    };
    CardComponent.prototype.toggle = function () {
        if (!this.collapsible) {
            return;
        }
        this.toggleService.toggle();
        this.change.emit(this.toggleService.boolState);
    };
    return CardComponent;
}());
CardComponent.BASE_CLASS = 'card';
CardComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-card, [arCard]',
                template: "<img *ngIf=\"hasTopImage\" class=\"card-img-top\" [src]=\"image?.src\" [alt]=\"image?.alt\">\n<div class=\"card-header\"\n     [class.pointer]=\"collapsible\"\n     [hidden]=\"!showHeader\"\n     (click)=\"toggle()\"\n>\n  <ng-content select=\"[header]\"></ng-content>\n</div>\n<div [class.card-body]=\"!imageOverlay\"\n     [class.card-img-overlay]=\"imageOverlay\"\n     [class.scrollable]=\"height\"\n     [ngStyle]=\"{ 'max-height.px': height }\"\n     [@toggleServiceState]=\"toggleService.state\"\n>\n  <ng-container *ngIf=\"cardContent\">\n    <h5 class=\"card-title\">{{ cardContent?.title }}</h5>\n    <h6 class=\"card-subtitle\" arBsTextContext=\"muted\">{{ cardContent?.subtitle }}</h6>\n    <p class=\"card-text\">{{ cardContent?.text }}</p>\n  </ng-container>\n  <ng-content></ng-content>\n</div>\n<div class=\"card-footer\" arBsTextContext=\"muted\" [hidden]=\"!showFooter\">\n  <ng-content select=\"[footer]\"></ng-content>\n</div>\n<img *ngIf=\"hasBottomImage\" class=\"card-img-top\" [src]=\"image?.src\" [alt]=\"image?.alt\">\n",
                styles: [".scrollable{overflow-y:auto}.scrollable::-webkit-scrollbar-track{-webkit-box-shadow:inset 0 0 6px rgba(0,0,0,.3);background-color:#f8f9fa;border-radius:10px}.scrollable::-webkit-scrollbar{width:10px;height:10px;background-color:#f5f5f5;border-radius:10px}.scrollable::-webkit-scrollbar-thumb{border-radius:10px;background-image:-webkit-gradient(linear,left bottom,left top,color-stop(.44,#7a8793),color-stop(.72,#56606a),color-stop(.86,#343a40))}.pointer{cursor:pointer}"],
                animations: [ToggleService.ANIMATION_HEIGHT],
                providers: [ToggleService, ContextualService]
            },] },
];
CardComponent.ctorParameters = function () { return [
    { type: ToggleService, },
    { type: ContextualService, },
]; };
CardComponent.propDecorators = {
    "image": [{ type: core.Input },],
    "imageBottom": [{ type: core.Input },],
    "cardContent": [{ type: core.Input },],
    "imageOverlay": [{ type: core.Input },],
    "showHeader": [{ type: core.Input },],
    "showFooter": [{ type: core.Input },],
    "height": [{ type: core.Input },],
    "collapsible": [{ type: core.Input },],
    "defaultCollapsed": [{ type: core.Input },],
    "change": [{ type: core.Output },],
    "className": [{ type: core.HostBinding, args: ['class.card',] },],
};
var InnerLinkComponent = /** @class */ (function () {
    function InnerLinkComponent(element, renderer, card, alertComponent) {
        this.element = element;
        this.renderer = renderer;
        this.card = card;
        this.alertComponent = alertComponent;
    }
    Object.defineProperty(InnerLinkComponent.prototype, "textContext", {
        get: function () {
            return ((this.disabled ? BsTextContextEnum.MUTED : BsTextContextEnum.DEFAULT));
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InnerLinkComponent.prototype, "isAlertLink", {
        get: function () {
            return Boolean(this.alertComponent) || this.isAlert;
        },
        enumerable: true,
        configurable: true
    });
    InnerLinkComponent.prototype.ngOnInit = function () {
        if (this.card) {
            this.renderer.addClass(this.element.nativeElement, 'card-link');
        }
    };
    return InnerLinkComponent;
}());
InnerLinkComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-inner-link',
                template: "\n    <a [routerLink]=\"url\"\n       [class.disabled]=\"disabled\"\n       [class.alert-link]=\"isAlertLink\"\n       [arBsTextContext]=\"textContext\"\n    >\n      <ng-content></ng-content>\n    </a>\n  ",
                styles: ["a{cursor:pointer;color:inherit;text-decoration:underline}a.disabled{pointer-events:none;cursor:default;text-decoration:none}a:hover{text-decoration:none}"]
            },] },
];
InnerLinkComponent.ctorParameters = function () { return [
    { type: core.ElementRef, },
    { type: core.Renderer2, },
    { type: CardComponent, decorators: [{ type: core.Optional },] },
    { type: AlertComponent, decorators: [{ type: core.Optional },] },
]; };
InnerLinkComponent.propDecorators = {
    "url": [{ type: core.Input },],
    "disabled": [{ type: core.Input },],
    "isAlert": [{ type: core.Input },],
    "isCard": [{ type: core.Input },],
};
var BlockComponent = /** @class */ (function () {
    function BlockComponent() {
    }
    return BlockComponent;
}());
BlockComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-block',
                template: "\n    <p *ngIf=\"title\" class=\"h6 border-bottom border-muted\" arBsTextContext=\"muted\">{{ title }}</p>\n    <ng-content></ng-content>\n  ",
                styles: [":host{display:block;margin-bottom:10px;padding:5px}"]
            },] },
];
BlockComponent.propDecorators = {
    "title": [{ type: core.Input },],
};
var TabLayoutViewModel = /** @class */ (function () {
    function TabLayoutViewModel() {
    }
    Object.defineProperty(TabLayoutViewModel.prototype, "hasOneTab", {
        get: function () {
            return lodash.size(this.tabs) === 1;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TabLayoutViewModel.prototype, "hasTabs", {
        get: function () {
            return lodash.size(this.tabs) > 1;
        },
        enumerable: true,
        configurable: true
    });
    TabLayoutViewModel.prototype.addTab = function (tab) {
        if (!this.tabs) {
            this.tabs = [tab];
            this.selectTab(tab);
        }
        else {
            this.tabs.push(tab);
        }
    };
    TabLayoutViewModel.prototype.removeTab = function (tab) {
        if (this.selectedTab === tab) {
            var nextTab = this.getNextTab(tab);
            this.selectTab(nextTab);
        }
        this.tabs = lodash.without(this.tabs, tab);
    };
    TabLayoutViewModel.prototype.selectTab = function (tab) {
        if (!tab) {
            return;
        }
        if (this.selectedTab) {
            this.selectedTab.active = false;
        }
        tab.active = true;
        this.selectedTab = tab;
    };
    TabLayoutViewModel.prototype.exist = function (tab) {
        return Boolean(lodash.find(this.tabs, function (item) { return item === tab; }));
    };
    TabLayoutViewModel.prototype.getNextTab = function (tabToDelete) {
        var currentTabIndex = lodash.findIndex(this.tabs, tabToDelete);
        if (currentTabIndex < 0) {
            return lodash.first(this.tabs);
        }
        if (currentTabIndex === 0 && lodash.size(this.tabs) > 1) {
            return this.tabs[1];
        }
        if (currentTabIndex > 0) {
            return this.tabs[currentTabIndex - 1];
        }
        return null;
    };
    return TabLayoutViewModel;
}());
var TabLayoutComponent = /** @class */ (function () {
    function TabLayoutComponent() {
        this.tabRemove = new rxjs.Subject();
        this.tabSelect = new rxjs.Subject();
        this.tabCreate = new rxjs.Subject();
        this.viewModel = new TabLayoutViewModel();
    }
    TabLayoutComponent.prototype.registerTab = function (tab) {
        this.viewModel.addTab(tab);
    };
    TabLayoutComponent.prototype.removeTab = function (tab) {
        if (tab.changed) {
            this.viewModel.tabToClose = tab;
        }
        else {
            this.deleteTab(tab);
        }
    };
    TabLayoutComponent.prototype.exist = function (tab) {
        return this.viewModel.exist(tab);
    };
    TabLayoutComponent.prototype.deleteTab = function (tab) {
        this.viewModel.removeTab(tab);
        if (tab.viewRef) {
            var index = this.content.indexOf(tab.viewRef);
            this.content.remove(index);
        }
        this.tabRemove.next(tab);
        this.viewModel.tabToClose = null;
    };
    TabLayoutComponent.prototype.createTab = function (tab, template, context) {
        if (this.exist(tab)) {
            this.selectTab(tab);
            return;
        }
        tab.viewRef = this.content.createEmbeddedView(template, { $implicit: context, tab: tab });
        this.registerTab(tab);
        this.selectTab(tab);
        this.tabCreate.next(tab);
    };
    TabLayoutComponent.prototype.selectTab = function (tab) {
        this.viewModel.selectTab(tab);
        this.tabSelect.next(tab);
    };
    return TabLayoutComponent;
}());
TabLayoutComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-tab-layout',
                template: "<div class=\"content-layout\">\n    <div class=\"content\" [class.has-notification]=\"viewModel.tabToClose\">\n        <ul class=\"nav nav-tabs nav-justified tab-links\" *ngIf=\"viewModel.hasTabs\">\n            <li class=\"nav-item\" *ngFor=\"let tab of viewModel.tabs\" [class.changed]=\"tab.changed\">\n                <a (click)=\"selectTab(tab)\" class=\"tab-link-inner nav-link\" [class.active]=\"tab.active\">\n                    <ar-tab-link [model]=\"tab\" (onClose)=\"removeTab($event)\">{{tab.title}}</ar-tab-link>\n                </a>\n            </li>\n        </ul>\n\n        <ar-block class=\"content\">\n            <ng-container #content>\n                <ng-content></ng-content>\n            </ng-container>\n        </ar-block>\n    </div>\n\n    <div class=\"notification modal-dialog modal-sm\" *ngIf=\"viewModel.tabToClose\">\n        <div class=\"modal-content\">\n            <div class=\"modal-body\">\n                Discard changes?\n            </div>\n            <div class=\"modal-footer\">\n                <button class=\"btn\" arBsContext=\"danger\" arBsSize=\"sm\" (click)=\"deleteTab(viewModel.tabToClose)\">\n                    Yes\n                </button>\n                <button class=\"btn\" arBsContext=\"default\" arBsSize=\"sm\" (click)=\"viewModel.tabToClose = null\">\n                    Cancel\n                </button>\n            </div>\n        </div>\n    </div>\n</div>\n",
                styles: [".tab{cursor:pointer;display:inline-block;width:auto;margin-right:5px}.tab.changed:before{content:'*';display:inline-block;position:absolute;top:2px;left:2px;z-index:1;color:#000;font-size:14px}.tab.active.changed:before{color:#fff}.nav-item{cursor:pointer}.tab-links{overflow-x:auto;overflow-y:hidden;-ms-flex-wrap:wrap;flex-wrap:wrap;-ms-flex-line-pack:start;align-content:flex-start;-webkit-box-pack:start;-ms-flex-pack:start;justify-content:flex-start}.tab-links::-webkit-scrollbar-track{-webkit-box-shadow:inset 0 0 6px rgba(0,0,0,.3);background-color:#f8f9fa;border-radius:10px}.tab-links::-webkit-scrollbar{width:10px;height:10px;background-color:#f5f5f5;border-radius:10px}.tab-links::-webkit-scrollbar-thumb{border-radius:10px;background-image:-webkit-gradient(linear,left bottom,left top,color-stop(.44,#7a8793),color-stop(.72,#56606a),color-stop(.86,#343a40))}.tab-link-inner{padding:5px;font-size:90%;-webkit-transition:all .3s ease-in;transition:all .3s ease-in}.tab-link-inner.active:hover{color:initial}.content.has-notification{-webkit-filter:grayscale(50%) opacity(50%);filter:grayscale(50%) opacity(50%)}.content.has-notification:before{content:\" \";z-index:1;width:100%;height:100%;display:block;position:absolute}.content-layout{position:relative}.content-layout .notification{position:absolute;top:30px;left:calc(50% - 150px)}.content{position:relative}"]
            },] },
];
TabLayoutComponent.ctorParameters = function () { return []; };
TabLayoutComponent.propDecorators = {
    "content": [{ type: core.ViewChild, args: ['content', { read: core.ViewContainerRef },] },],
};
var TabModel = /** @class */ (function () {
    function TabModel(title, closeable) {
        if (title === void 0) { title = 'Tab title'; }
        if (closeable === void 0) { closeable = false; }
        this.title = title;
        this.closeable = closeable;
    }
    return TabModel;
}());
var TabComponent = /** @class */ (function () {
    function TabComponent(tabLayout) {
        this.tabLayout = tabLayout;
    }
    Object.defineProperty(TabComponent.prototype, "isActive", {
        get: function () {
            return (this.model && this.model.active);
        },
        enumerable: true,
        configurable: true
    });
    TabComponent.prototype.ngOnInit = function () {
        if (this.model) {
            return;
        }
        this.model = new TabModel(this.title);
        this.tabLayout.registerTab(this.model);
        if (this.selected) {
            this.tabLayout.selectTab(this.model);
        }
    };
    TabComponent.prototype.updateTitle = function (title) {
        if (this.model) {
            this.model.title = title;
        }
    };
    return TabComponent;
}());
TabComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-tab',
                template: "\n    <section *ngIf=\"isActive\" [@toggleServiceState]>\n      <ng-content></ng-content>\n    </section>\n  ",
                animations: [animations.trigger('toggleServiceState', [
                        animations.transition(':enter', [
                            animations.style({ opacity: 0 }),
                            animations.animate('200ms ease-in', animations.style({ opacity: 1 }))
                        ]),
                        animations.transition(':leave', [
                            animations.style({ opacity: 1, position: 'absolute', top: '5px', right: '5px', left: '5px' }),
                            animations.animate('200ms ease-out', animations.style({ opacity: 0 }))
                        ])
                    ])
                ],
            },] },
];
TabComponent.ctorParameters = function () { return [
    { type: TabLayoutComponent, },
]; };
TabComponent.propDecorators = {
    "dynamicContent": [{ type: core.ViewChild, args: ['dynamicContent', { read: core.ViewContainerRef },] },],
    "title": [{ type: core.Input },],
    "selected": [{ type: core.Input },],
    "model": [{ type: core.Input },],
};
var TabLinkComponent = /** @class */ (function () {
    function TabLinkComponent() {
        this.onClose = new core.EventEmitter();
    }
    TabLinkComponent.prototype.closeTabHandler = function (event) {
        event.stopPropagation();
        this.onClose.emit(this.model);
    };
    return TabLinkComponent;
}());
TabLinkComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-tab-link',
                template: "\n      <ng-content></ng-content>\n      <button type=\"button\" class=\"close\" (click)=\"closeTabHandler($event)\" *ngIf=\"model.closeable\">\n          <span aria-hidden=\"true\">\u00D7</span>\n      </button>\n  ",
                styles: [
                    ":host { white-space: nowrap; }",
                    ".close { position: absolute; right: 3px; top: 0; }"
                ]
            },] },
];
TabLinkComponent.propDecorators = {
    "model": [{ type: core.Input },],
    "onClose": [{ type: core.Output },],
};
var DropdownDividerComponent = /** @class */ (function () {
    function DropdownDividerComponent() {
    }
    return DropdownDividerComponent;
}());
DropdownDividerComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-dropdown-divider',
                template: "\n    <small arBsTextContext=\"muted\">\n      <ng-content></ng-content>\n    </small>\n    <div class=\"dropdown-divider\"></div>\n  "
            },] },
];
var AlertsAreaService = /** @class */ (function () {
    function AlertsAreaService() {
        var _this = this;
        this.addAlert = function (message) {
            _this.removeOldestAlert();
            message.viewRef = _this.container.createEmbeddedView(_this.template, { $implicit: message });
            _this.createAutohideObserver(message).subscribe(_this.removeAlert);
        };
        this.removeAlert = function (message) {
            if (!message) {
                return;
            }
            _this.removeAlertByViewRef(message.viewRef);
        };
    }
    AlertsAreaService.prototype.registerContainer = function (container) {
        this.container = container;
    };
    AlertsAreaService.prototype.registerTemplate = function (template) {
        this.template = template;
    };
    AlertsAreaService.prototype.removeOldestAlert = function () {
        if (this.container.length === AlertsAreaService.MAX_ALERT_STACK_SIZE) {
            this.container.remove(0);
        }
    };
    AlertsAreaService.prototype.removeAlertByViewRef = function (view) {
        var index = this.container.indexOf(view);
        if (index >= 0) {
            this.container.remove(index);
        }
    };
    AlertsAreaService.prototype.createAutohideObserver = function (message) {
        if (!message.shouldAutohide) {
            return rxjs.of(null);
        }
        return rxjs.of(message).pipe(operators.delay(message.displayTime));
    };
    return AlertsAreaService;
}());
AlertsAreaService.MAX_ALERT_STACK_SIZE = 3;
AlertsAreaService.decorators = [
    { type: core.Injectable },
];
var AlertAreaComponent = /** @class */ (function () {
    function AlertAreaComponent(messageService, alertService) {
        this.messageService = messageService;
        this.alertService = alertService;
    }
    AlertAreaComponent.prototype.ngOnInit = function () {
        this.alertService.registerContainer(this.alertContainer);
        this.alertService.registerTemplate(this.alertTemplate);
        this.alertSubscription = this.messageService.of(DisplayAlertMessage).subscribe(this.alertService.addAlert);
    };
    AlertAreaComponent.prototype.ngOnDestroy = function () {
        this.alertSubscription.unsubscribe();
    };
    return AlertAreaComponent;
}());
AlertAreaComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-alert-area',
                template: "\n      <ng-container #alertContainer></ng-container>\n      <ng-template #alertTemplate let-message>\n          <ar-alert [arBsContext]=\"message.bsContext\"\n                   [closeable]=\"message.closeable\"\n                   (close)=\"alertService.removeAlert(message)\"\n                   class=\"inner-alert\"\n          >\n              <ng-container *ngIf=\"message.isSimple; else contentTemplate\">{{ message.message }}</ng-container>\n              <ng-template #contentTemplate>\n                  <ng-container *ngTemplateOutlet=\"message.template; context: { $implicit: message.templateContext }\">\n                  </ng-container>\n              </ng-template>\n          </ar-alert>\n      </ng-template>\n  ",
                providers: [AlertsAreaService],
                styles: [":host{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:reverse;-ms-flex-direction:column-reverse;flex-direction:column-reverse;width:500px;height:auto;position:fixed;bottom:0;right:0;z-index:1;padding:10px}.inner-alert{opacity:.95}"]
            },] },
];
AlertAreaComponent.ctorParameters = function () { return [
    { type: MessageBusService, },
    { type: AlertsAreaService, },
]; };
AlertAreaComponent.propDecorators = {
    "alertContainer": [{ type: core.ViewChild, args: ['alertContainer', { read: core.ViewContainerRef },] },],
    "alertTemplate": [{ type: core.ViewChild, args: ['alertTemplate',] },],
};
var AlertHeadingComponent = /** @class */ (function () {
    function AlertHeadingComponent() {
    }
    return AlertHeadingComponent;
}());
AlertHeadingComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-alert-heading',
                template: "\n    <span class=\"h4 alert-heading\">\n      <ng-content></ng-content>\n    </span>\n  "
            },] },
];
var LoremIpsumComponent = /** @class */ (function () {
    function LoremIpsumComponent() {
        this.loremIpsumOriginal = "\n    Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,\n    totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo.\n    Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos,\n    qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, \n    consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt, \n    ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, \n    quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?\n    Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur,\n    vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur?\n    At vero eos et accusamus et iusto odio dignissimos ducimus,\n    qui blanditiis praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias excepturi sint,\n    obcaecati cupiditate non provident, similique sunt in culpa, qui officia deserunt mollitia animi,\n    id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.\n    Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id,\n    quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.\n    Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet,\n    ut et voluptates repudiandae sint et molestiae non recusandae.\n    Itaque earum rerum hic tenetur a sapiente delectus,\n    ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.\n  ";
    }
    return LoremIpsumComponent;
}());
LoremIpsumComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-lorem-ipsum',
                template: "\n    <p>{{ loremIpsumOriginal | arTruncate:length }}</p>\n  "
            },] },
];
LoremIpsumComponent.propDecorators = {
    "length": [{ type: core.Input },],
};
var HelpBlockComponent = /** @class */ (function () {
    function HelpBlockComponent() {
        this.mini = true;
    }
    return HelpBlockComponent;
}());
HelpBlockComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-help-block, ar-form-text',
                template: "\n    <span class=\"form-text\" [class.form-text--mini]=\"mini\" arBsTextContext=\"muted\">\n      <ng-content></ng-content>\n    </span>\n  ",
                styles: ["\n    .form-text--mini {\n      font-size: 80%;\n    }\n  "]
            },] },
];
HelpBlockComponent.propDecorators = {
    "mini": [{ type: core.Input },],
};
var OuterLinkComponent = /** @class */ (function (_super) {
    __extends(OuterLinkComponent, _super);
    function OuterLinkComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    OuterLinkComponent.prototype.ngOnChanges = function (changes) {
        var url = changes.url, href = changes.href;
        if (!url || (url.currentValue === url.previousValue && !url.firstChange)) {
            return;
        }
        if (!url.currentValue) {
            this.urlString = '';
            this.parsedUrl = null;
            return;
        }
        this.urlString = url.currentValue;
    };
    return OuterLinkComponent;
}(InnerLinkComponent));
OuterLinkComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-outer-link',
                template: "\n    <a target=\"_blank\" [href]=\"urlString\" *ngIf=\"url\"\n       [class.disabled]=\"disabled\"\n       [class.alert-link]=\"isAlertLink\"\n       [arBsTextContext]=\"textContext\"\n    >\n        <ng-content></ng-content>\n    </a>\n  ",
                styles: ["a{cursor:pointer;color:inherit;text-decoration:underline}a.disabled{pointer-events:none;cursor:default;text-decoration:none}a:hover{text-decoration:none}", ".as-text{color:inherit}.as-text:focus,.as-text:hover{text-decoration:none}"]
            },] },
];
OuterLinkComponent.propDecorators = {
    "url": [{ type: core.Input },],
    "disabled": [{ type: core.Input },],
    "isAlert": [{ type: core.Input },],
    "isCard": [{ type: core.Input },],
};
var PageHeaderComponent = /** @class */ (function () {
    function PageHeaderComponent() {
    }
    return PageHeaderComponent;
}());
PageHeaderComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-page-header',
                template: "\n      <ar-block class=\"page-header clearfix border-bottom\">\n          <div class=\"pull-left\">\n            <h1>\n                <ng-content></ng-content>\n            </h1>\n          </div>\n          <div class=\"pull-right\">\n            <ng-content select=\"[side-content]\"></ng-content>\n          </div>\n      </ar-block>\n  "
            },] },
];
var XL = 'xl';
var LG = 'lg';
var MD = 'md';
var SM = 'sm';
var XS = 'xs';
var DEFAULT$2 = '';
var BsSizeEnum = { XL: XL, LG: LG, MD: MD, XS: XS, SM: SM, DEFAULT: DEFAULT$2 };
var BsSizeArray = lodash.values(BsSizeEnum);
var PagingListSizeComponent = /** @class */ (function () {
    function PagingListSizeComponent(contextualService) {
        this.contextualService = contextualService;
        this.list = [10, 20, 50, 100];
        this.bsSize = BsSizeEnum.SM;
        this.onSizeChange = new core.EventEmitter();
    }
    PagingListSizeComponent.prototype.ngOnChanges = function (changes) {
        var size$$1 = changes.size;
        if (!size$$1.firstChange && size$$1.previousValue !== size$$1.currentValue) {
            this.form.reset({ size: size$$1.currentValue }, { emitEvent: false });
        }
    };
    PagingListSizeComponent.prototype.ngOnInit = function () {
        var _this = this;
        var size$$1 = new forms.FormControl(this.size);
        this.changeSubscription = size$$1.valueChanges
            .subscribe(function (val) { _this.onSizeChange.emit(val); });
        this.form = new forms.FormGroup({ size: size$$1 });
    };
    PagingListSizeComponent.prototype.ngOnDestroy = function () {
        this.changeSubscription.unsubscribe();
    };
    return PagingListSizeComponent;
}());
PagingListSizeComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-paging-list-size',
                template: "\n    <div *ngIf='form' [formGroup]=\"form\">\n        <select class=\"form-control\" formControlName=\"size\" [arBsSize]=\"contextualService.bsSize\">\n            <option *ngFor=\"let item of list\" [value]=\"item\">{{item}}</option>\n        </select>\n    </div>\n  ",
                styles: [":host{display:inline-block;margin:0 5px;padding-bottom:1px}"],
                changeDetection: core.ChangeDetectionStrategy.OnPush,
                providers: [ContextualService]
            },] },
];
PagingListSizeComponent.ctorParameters = function () { return [
    { type: ContextualService, },
]; };
PagingListSizeComponent.propDecorators = {
    "size": [{ type: core.Input },],
    "list": [{ type: core.Input },],
    "bsSize": [{ type: core.Input },],
    "onSizeChange": [{ type: core.Output },],
};
var PaginationComponent = /** @class */ (function () {
    function PaginationComponent(contextualService) {
        var _this = this;
        this.contextualService = contextualService;
        this.onPageSelect = new core.EventEmitter();
        this._total = 0;
        this.createPageItem = function (page, title, disabled, common$$1) {
            if (title === void 0) { title = null; }
            if (disabled === void 0) { disabled = false; }
            if (common$$1 === void 0) { common$$1 = false; }
            return {
                title: title || String(page),
                page: page,
                disabled: disabled || (common$$1 && page === _this.current) || page < 0 || page > _this.total,
                common: common$$1
            };
        };
    }
    Object.defineProperty(PaginationComponent.prototype, "total", {
        get: function () { return this._total; },
        set: function (val) {
            this._total = Number(val);
            this.pageLinks = this.createPages(this.total);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginationComponent.prototype, "current", {
        get: function () { return this._currentPage; },
        set: function (val) {
            this._currentPage = Number(val);
            this.pageLinks = this.createPages(this.total);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginationComponent.prototype, "isVisible", {
        get: function () {
            return this.total > 0 && this.current <= this.total && this.current > 0;
        },
        enumerable: true,
        configurable: true
    });
    PaginationComponent.prototype.ngOnInit = function () {
        this.pageLinks = this.createPages(this.total);
    };
    PaginationComponent.prototype.handlePageClick = function (page) {
        if (!page || page.disabled || this.isActivePage(page)) {
            return;
        }
        this.onPageSelect.emit(page.page);
    };
    PaginationComponent.prototype.isActivePage = function (page) {
        return !page.disabled && !page.common && this.current === page.page;
    };
    PaginationComponent.prototype.createPages = function (total) {
        var _this = this;
        if (total <= PaginationComponent.MAX_DISPLAY) {
            return lodash.map(lodash.range(1, total + 1), function (page) { return _this.createPageItem(page); });
        }
        if (this.current < PaginationComponent.EDGE_COUNT - 1) {
            return lodash.concat([this.createPageItem(this.current - 1, PaginationComponent.PREV_TITLE, this.current < 2, true)], lodash.map(lodash.range(1, PaginationComponent.EDGE_COUNT), function (page) { return _this.createPageItem(page); }), [this.createPageItem(0, PaginationComponent.EMPTY_TITLE, true, true)], lodash.map(lodash.range(total - PaginationComponent.MIN_COUNT + 1, total + 1), function (page) { return _this.createPageItem(page); }), [this.createPageItem(this.current + 1, PaginationComponent.NEXT_TITLE, false, true)]);
        }
        if (this.total - PaginationComponent.EDGE_COUNT + 2 < this.current) {
            return lodash.concat([this.createPageItem(this.current - 1, PaginationComponent.PREV_TITLE, false, true)], lodash.map(lodash.range(1, PaginationComponent.MIN_COUNT + 1), function (page) { return _this.createPageItem(page); }), [this.createPageItem(0, PaginationComponent.EMPTY_TITLE, true, true)], lodash.map(lodash.range(total - PaginationComponent.EDGE_COUNT + 2, total + 1), function (page) { return _this.createPageItem(page); }), [this.createPageItem(this.current + 1, PaginationComponent.NEXT_TITLE, false, true)]);
        }
        return lodash.concat([this.createPageItem(this.current - 1, PaginationComponent.PREV_TITLE, false, true)], lodash.map(lodash.range(1, PaginationComponent.MIN_COUNT), function (page) { return _this.createPageItem(page); }), [this.createPageItem(0, PaginationComponent.EMPTY_TITLE, true, true)], lodash.map(lodash.range(this.current - 1, this.current + 2), function (page) { return _this.createPageItem(page); }), [this.createPageItem(0, PaginationComponent.EMPTY_TITLE, true, true)], lodash.map(lodash.range(total - PaginationComponent.MIN_COUNT + 2, total + 1), function (page) { return _this.createPageItem(page); }), [this.createPageItem(this.current + 1, PaginationComponent.NEXT_TITLE, false, true)]);
    };
    return PaginationComponent;
}());
PaginationComponent.PREV_TITLE = '«';
PaginationComponent.NEXT_TITLE = '»';
PaginationComponent.EMPTY_TITLE = '...';
PaginationComponent.MAX_DISPLAY = 15;
PaginationComponent.EDGE_COUNT = 6;
PaginationComponent.MIN_COUNT = 3;
PaginationComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-pagination',
                template: "\n      <nav *ngIf=\"isVisible\" class=\"noselect\">\n          <ul class=\"pagination\" [arBsSize]=\"contextualService.bsSize\">\n              <li *ngFor=\"let item of pageLinks\"\n                  [class.active]=\"isActivePage(item)\"\n                  [class.disabled]=\"item.disabled\"\n                  class=\"page-item\"\n              >\n                  <a class=\"pointer page-link\" (click)=\"handlePageClick(item)\">\n                      {{item.title}}\n                  </a>\n              </li>\n          </ul>\n      </nav>\n  ",
                styles: [
                    ':host { display: inline-block; vertical-align: middle; }',
                    '.pagination { margin: 0; }'
                ],
                providers: [ContextualService]
            },] },
];
PaginationComponent.ctorParameters = function () { return [
    { type: ContextualService, },
]; };
PaginationComponent.propDecorators = {
    "total": [{ type: core.Input },],
    "current": [{ type: core.Input },],
    "onPageSelect": [{ type: core.Output },],
};
var PagingTextComponent = /** @class */ (function () {
    function PagingTextComponent() {
    }
    Object.defineProperty(PagingTextComponent.prototype, "shouldDisplay", {
        get: function () {
            return this.paging && this.paging.getTotalCount() > 0;
        },
        enumerable: true,
        configurable: true
    });
    return PagingTextComponent;
}());
PagingTextComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-paging-text',
                template: "\n    <small *ngIf=\"shouldDisplay\">\n      Showing {{ paging.getStartRecord() }} to {{ paging.getEndRecord() }} of {{ paging.getTotalCount() }} entries\n    </small>\n  ",
                styles: [
                    ":host {\n        display: inline-block;\n        margin: 0 1em;\n    }"
                ]
            },] },
];
PagingTextComponent.propDecorators = {
    "paging": [{ type: core.Input },],
};
var PagingControlComponent = /** @class */ (function () {
    function PagingControlComponent(contextualService) {
        this.contextualService = contextualService;
        this.bsSize = BsSizeEnum.SM;
        this.onChange = new core.EventEmitter();
    }
    return PagingControlComponent;
}());
PagingControlComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-paging-control',
                template: "\n    <ng-container *ngIf=\"paging\">\n      <small>Records per page</small>\n      <ar-paging-list-size\n        [size]=\"paging.getPageSize()\"\n        (onSizeChange)=\"onChange.emit({ pageSize: $event, page: paging.getPage() })\"\n        [arBsSize]=\"contextualService.bsSize\"\n      >\n      </ar-paging-list-size>\n      <ar-pagination\n        [total]=\"paging.getPagesCount()\"\n        [current]=\"paging.getPage()\"\n        (onPageSelect)=\"onChange.emit({ pageSize: paging.getPageSize(), page: $event })\"\n        [arBsSize]=\"contextualService.bsSize\"\n      >\n      </ar-pagination>\n      <ar-paging-text [paging]=\"paging\"></ar-paging-text>\n    </ng-container>\n  ",
                styles: [
                    ':host { display: flex; align-items: center; } '
                ],
                providers: [ContextualService]
            },] },
];
PagingControlComponent.ctorParameters = function () { return [
    { type: ContextualService, },
]; };
PagingControlComponent.propDecorators = {
    "paging": [{ type: core.Input },],
    "bsSize": [{ type: core.Input },],
    "onChange": [{ type: core.Output },],
};
var MailLinkComponent = /** @class */ (function () {
    function MailLinkComponent() {
    }
    return MailLinkComponent;
}());
MailLinkComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-mail-link',
                template: "\n    <a *ngIf=\"email\" href=\"mailto:{{email}}\">{{email}}</a>\n  "
            },] },
];
MailLinkComponent.propDecorators = {
    "email": [{ type: core.Input },],
};
var ModalLayoutViewModel = /** @class */ (function () {
    function ModalLayoutViewModel(viewContainer) {
        this.container = viewContainer;
    }
    return ModalLayoutViewModel;
}());
var ModalService = /** @class */ (function () {
    function ModalService(logService) {
        this.logService = logService;
    }
    ModalService.prototype.showModal = function (template, config, context) {
        this.checkLayout();
        if (this.layoutViewModel.template) {
            this.layoutViewModel.container.clear();
        }
        this.layoutViewModel.viewRef = this.layoutViewModel.container.createEmbeddedView(template, { $implicit: context });
        this.layoutViewModel.template = template;
        this.layoutViewModel.config = config;
        this.layoutViewModel.opened = true;
    };
    ModalService.prototype.hideModal = function () {
        this.checkLayout();
        this.layoutViewModel.opened = false;
        this.layoutViewModel.template = null;
        this.layoutViewModel.viewRef = null;
        this.layoutViewModel.config = null;
        this.layoutViewModel.container.clear();
    };
    ModalService.prototype.registerLayout = function (viewContainer) {
        if (this.layoutViewModel) {
            this.logService.log('ModalService: skip Modal Layout already registered error');
        }
        else if (this.layoutViewModel) {
            throw new Error('ModalService: another instance Modal Layout already registered');
        }
        this.layoutViewModel = this.createViewModel(viewContainer);
        return this.layoutViewModel;
    };
    ModalService.prototype.unregisterLayout = function () {
        this.checkLayout();
        this.layoutViewModel = null;
    };
    ModalService.prototype.createViewModel = function (viewContainer) {
        return new ModalLayoutViewModel(viewContainer);
    };
    ModalService.prototype.checkLayout = function () {
        if (!this.layoutViewModel) {
            throw new Error('ModalService: modal layout not registered');
        }
    };
    return ModalService;
}());
ModalService.decorators = [
    { type: core.Injectable },
];
ModalService.ctorParameters = function () { return [
    { type: LogService, },
]; };
var modalService;
var modalServiceFactory = function (logService) {
    if (!modalService) {
        modalService = new ModalService(logService);
    }
    return modalService;
};
var ɵ0$1 = modalServiceFactory;
var ModalLayoutComponent = /** @class */ (function () {
    function ModalLayoutComponent(modalService) {
        this.modalService = modalService;
    }
    ModalLayoutComponent.prototype.ngOnInit = function () {
        this.viewModel = this.modalService.registerLayout(this.modalContent);
    };
    ModalLayoutComponent.prototype.ngOnDestroy = function () {
        this.modalService.unregisterLayout();
    };
    ModalLayoutComponent.prototype.backdropClick = function (event) {
        if (!this.viewModel.config) {
            return;
        }
        if (!this.viewModel.config.backdropClickClose) {
            return;
        }
        if (this.dialog.nativeElement.contains(event.target)) {
            return;
        }
        this.modalService.hideModal();
    };
    ModalLayoutComponent.prototype.getDialogClassName = function () {
        var className = this.viewModel.config
            ? "modal-dialog " + this.viewModel.config.className
            : 'modal-dialog';
        className = this.viewModel.config && this.viewModel.config.centered
            ? className + " modal-dialog-centered"
            : className;
        return className;
    };
    return ModalLayoutComponent;
}());
ModalLayoutComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-modal-layout',
                template: "\n      <div class=\"modal fade\" [class.show]=\"viewModel.opened\" (click)=\"backdropClick($event)\">\n          <div [class]=\"getDialogClassName()\" #dialog>\n            <ng-container #modalContent></ng-container>\n          </div>\n      </div>\n      <div class=\"modal-backdrop fade show\" *ngIf=\"viewModel.opened\"></div>\n  ",
                styles: ['.show { display: block; }'],
                providers: [{
                        provide: ModalService,
                        useFactory: ɵ0$1,
                        deps: [LogService]
                    }]
            },] },
];
ModalLayoutComponent.ctorParameters = function () { return [
    { type: ModalService, },
]; };
ModalLayoutComponent.propDecorators = {
    "modalContent": [{ type: core.ViewChild, args: ['modalContent', { read: core.ViewContainerRef },] },],
    "dialog": [{ type: core.ViewChild, args: ['dialog', { read: core.ElementRef },] },],
};
var ModalDialogConfigModel = /** @class */ (function () {
    function ModalDialogConfigModel() {
        this.backdropClickClose = true;
        this.centered = false;
        this._classes = [];
    }
    Object.defineProperty(ModalDialogConfigModel.prototype, "large", {
        set: function (value) {
            this.toggleClass('modal-lg', value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ModalDialogConfigModel.prototype, "small", {
        set: function (value) {
            this.toggleClass('modal-sm', value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ModalDialogConfigModel.prototype, "customClass", {
        set: function (value) {
            this.toggleClass(value, true);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ModalDialogConfigModel.prototype, "className", {
        get: function () {
            return lodash.join(this._classes, ' ');
        },
        enumerable: true,
        configurable: true
    });
    ModalDialogConfigModel.prototype.toggleClass = function (className, state$$1) {
        if (lodash.indexOf(this._classes, className) >= 0 && state$$1) {
            return;
        }
        this._classes = state$$1
            ? lodash.concat(this._classes, className)
            : lodash.without(this._classes, className);
    };
    return ModalDialogConfigModel;
}());
var ɵ0$2 = modalServiceFactory;
var ModalComponent = /** @class */ (function () {
    function ModalComponent(modalService) {
        this.modalService = modalService;
        this.config = ModalComponent.createConfig();
        this.showHeader = true;
        this.showFooter = true;
    }
    ModalComponent.createConfig = function () { return new ModalDialogConfigModel(); };
    ModalComponent.prototype.show = function (context) {
        this.modalService.showModal(this.modalTemplate, this.config, context);
    };
    ModalComponent.prototype.hide = function () {
        this.modalService.hideModal();
    };
    return ModalComponent;
}());
ModalComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-modal',
                template: "\n    <ng-template #modalTemplate let-context>\n        <div class=\"modal-content\" [@toggleServiceState]>\n            <div class=\"modal-header\" *ngIf=\"showHeader\">\n                <ng-container *ngIf=\"headerTemplate\">\n                    <ng-container *ngTemplateOutlet=\"headerTemplate; context: { $implicit: context }\">\n                    </ng-container>\n                </ng-container>\n                <ng-content select=\"[header]\"></ng-content>\n                <button type=\"button\" class=\"close\" (click)=\"hide()\" >\n                  <span aria-hidden=\"true\">&times;</span>\n                </button>\n            </div>\n            <div class=\"modal-body\">\n                <ng-container *ngIf=\"bodyTemplate\">\n                    <ng-container *ngTemplateOutlet=\"bodyTemplate; context: { $implicit: context }\"></ng-container>\n                </ng-container>\n                <ng-content></ng-content>\n            </div>\n            <div class=\"modal-footer\" *ngIf=\"showFooter\">\n                <ng-container *ngIf=\"footerTemplate\">\n                    <ng-container *ngTemplateOutlet=\"footerTemplate; context: { $implicit: context }\"></ng-container>\n                </ng-container>\n                <ng-content select=\"[footer]\"></ng-content>\n            </div>\n        </div>\n    </ng-template>\n  ",
                styles: ["\n    .modal-content {\n        word-wrap: break-word;\n        overflow-wrap: break-word;\n    }\n  "],
                providers: [{
                        provide: ModalService,
                        useFactory: ɵ0$2,
                        deps: [LogService]
                    }],
                animations: [
                    ToggleService.ANIMATION_OPACITY
                ]
            },] },
];
ModalComponent.ctorParameters = function () { return [
    { type: ModalService, },
]; };
ModalComponent.propDecorators = {
    "modalTemplate": [{ type: core.ViewChild, args: ['modalTemplate', { read: core.TemplateRef },] },],
    "headerTemplate": [{ type: core.Input },],
    "bodyTemplate": [{ type: core.Input },],
    "footerTemplate": [{ type: core.Input },],
    "config": [{ type: core.Input },],
    "showHeader": [{ type: core.Input },],
    "showFooter": [{ type: core.Input },],
};
var ModalHeaderComponent = /** @class */ (function () {
    function ModalHeaderComponent() {
    }
    return ModalHeaderComponent;
}());
ModalHeaderComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-modal-header',
                template: "\n    <span class=\"modal-header-content\">\n      <ng-content></ng-content>\n    </span>\n  ",
                styles: ["\n    .modal-header-content {\n      font-weight: bold;\n      font-size: 110%;\n    }\n  "]
            },] },
];
var PanelComponent = /** @class */ (function () {
    function PanelComponent() {
        this.showHeader = false;
        this.showFooter = false;
        this.change = new core.EventEmitter();
    }
    return PanelComponent;
}());
PanelComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-panel',
                template: "\n    <ar-card\n      [arBsBgContext]=\"bsContext\"\n      [collapsible]=\"collapsible\"\n      [showHeader]=\"showHeader\"\n      [showFooter]=\"showFooter\"\n      [defaultCollapsed]=\"defaultCollapsed\"\n      [height]=\"height\"\n      (change)=\"change.emit($event)\"\n    >\n      <ng-container header>\n        <ng-content select=\"[header]\" #header></ng-content>\n      </ng-container>\n      <ng-content></ng-content>\n      <ng-container footer>\n        <ng-content select=\"[footer]\" #header></ng-content>\n      </ng-container>\n    </ar-card>\n  "
            },] },
];
PanelComponent.propDecorators = {
    "bsContext": [{ type: core.Input },],
    "collapsible": [{ type: core.Input },],
    "showHeader": [{ type: core.Input },],
    "showFooter": [{ type: core.Input },],
    "defaultCollapsed": [{ type: core.Input },],
    "height": [{ type: core.Input },],
    "change": [{ type: core.Output },],
};
var WidthDirective = /** @class */ (function () {
    function WidthDirective(renderer2, elementRef, logService) {
        this.renderer2 = renderer2;
        this.elementRef = elementRef;
        this.logService = logService;
    }
    Object.defineProperty(WidthDirective.prototype, "arWidth", {
        set: function (width) {
            if (width === this.currentWidth) {
                return;
            }
            this.checkWidthValue(width);
            this.decorate(width);
        },
        enumerable: true,
        configurable: true
    });
    WidthDirective.prototype.decorate = function (nextWidth) {
        this.renderer2.removeClass(this.elementRef.nativeElement, this.currentClass);
        this.currentWidth = nextWidth;
        this.currentClass = nextWidth ? WidthDirective.WIDTH_PREFIX + "-" + this.currentWidth : '';
        if (this.currentClass) {
            this.renderer2.addClass(this.elementRef.nativeElement, this.currentClass);
        }
    };
    WidthDirective.prototype.checkWidthValue = function (value) {
        var stringValue = value ? String(value) : '';
        if (lodash.indexOf(WidthDirective.ALLOWED_VALUES, stringValue) < 0) {
            this.logService.warn("WidthDirective: Only " + WidthDirective.ALLOWED_VALUES + " values could be accepted");
        }
    };
    return WidthDirective;
}());
WidthDirective.WIDTH_PREFIX = 'w';
WidthDirective.ALLOWED_VALUES = ['', '25', '50', '75', '100'];
WidthDirective.decorators = [
    { type: core.Directive, args: [{
                selector: '[arWidth]'
            },] },
];
WidthDirective.ctorParameters = function () { return [
    { type: core.Renderer2, },
    { type: core.ElementRef, },
    { type: LogService, },
]; };
WidthDirective.propDecorators = {
    "arWidth": [{ type: core.Input },],
};
var BorderDirective = /** @class */ (function () {
    function BorderDirective(renderer2, elementRef) {
        this.renderer2 = renderer2;
        this.elementRef = elementRef;
    }
    Object.defineProperty(BorderDirective.prototype, "arBorder", {
        set: function (context) {
            if (this.currentContext !== context) {
                this.decorate(context);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BorderDirective.prototype, "arBorderRounded", {
        set: function (isRounded) {
            if (this.rounded === isRounded) {
                return;
            }
            this.setRounded(isRounded);
        },
        enumerable: true,
        configurable: true
    });
    BorderDirective.prototype.ngOnInit = function () {
        this.renderer2.addClass(this.elementRef.nativeElement, BorderDirective.BORDER_PREFIX);
    };
    BorderDirective.prototype.decorate = function (context) {
        this.renderer2.removeClass(this.elementRef.nativeElement, this.currentContextClass);
        this.currentContext = context;
        this.currentContextClass = context ? BorderDirective.BORDER_PREFIX + "-" + this.currentContext : '';
        if (this.currentContextClass) {
            this.renderer2.addClass(this.elementRef.nativeElement, this.currentContextClass);
        }
    };
    BorderDirective.prototype.setRounded = function (isRounded) {
        if (isRounded) {
            this.renderer2.addClass(this.elementRef.nativeElement, BorderDirective.ROUNDED_PREFIX);
        }
        else {
            this.renderer2.removeClass(this.elementRef.nativeElement, BorderDirective.ROUNDED_PREFIX);
        }
        this.rounded = isRounded;
    };
    return BorderDirective;
}());
BorderDirective.BORDER_PREFIX = 'border';
BorderDirective.ROUNDED_PREFIX = 'rounded';
BorderDirective.decorators = [
    { type: core.Directive, args: [{
                selector: '[arBorder]'
            },] },
];
BorderDirective.ctorParameters = function () { return [
    { type: core.Renderer2, },
    { type: core.ElementRef, },
]; };
BorderDirective.propDecorators = {
    "arBorder": [{ type: core.Input },],
    "arBorderRounded": [{ type: core.Input },],
};
var CardGroupComponent = /** @class */ (function () {
    function CardGroupComponent() {
        this.isGroup = true;
        this.isDeck = false;
        this.isColumns = false;
    }
    Object.defineProperty(CardGroupComponent.prototype, "deck", {
        set: function (val) {
            this.isColumns = !val;
            this.isGroup = !val;
            this.isDeck = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CardGroupComponent.prototype, "columns", {
        set: function (val) {
            this.isColumns = val;
            this.isGroup = !val;
            this.isDeck = !val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CardGroupComponent.prototype, "group", {
        set: function (val) {
            this.isColumns = !val;
            this.isGroup = val;
            this.isDeck = !val;
        },
        enumerable: true,
        configurable: true
    });
    return CardGroupComponent;
}());
CardGroupComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-card-group, [arCardGroup]',
                template: "\n    <ng-content></ng-content>\n  ",
                styles: [":host.card-columns{display:block}"]
            },] },
];
CardGroupComponent.propDecorators = {
    "deck": [{ type: core.Input },],
    "columns": [{ type: core.Input },],
    "group": [{ type: core.Input },],
    "isGroup": [{ type: core.HostBinding, args: ['class.card-group',] },],
    "isDeck": [{ type: core.HostBinding, args: ['class.card-deck',] },],
    "isColumns": [{ type: core.HostBinding, args: ['class.card-columns',] },],
};
var DeprecatedDirective = /** @class */ (function () {
    function DeprecatedDirective(logService) {
        this.logService = logService;
    }
    DeprecatedDirective.prototype.ngOnInit = function () {
        this.logService.warn('bsContext, bsSize properties were deprecated. Use arBsContext, arBsSize directives instead');
    };
    return DeprecatedDirective;
}());
DeprecatedDirective.decorators = [
    { type: core.Directive, args: [{
                selector: '[bsSize]:not(ar-bs-group), [bsContext]:not(ar-bs-group)'
            },] },
];
DeprecatedDirective.ctorParameters = function () { return [
    { type: LogService, },
]; };
DeprecatedDirective.propDecorators = {
    "bsSize": [{ type: core.Input },],
    "bsContext": [{ type: core.Input },],
};
var ProgressBarComponent = /** @class */ (function () {
    function ProgressBarComponent(contextualService) {
        this.contextualService = contextualService;
        this.showValue = false;
        this.striped = false;
        this.active = false;
        this.baseClass = true;
        this.role = 'progressbar';
        this.ariaValueNow = '0';
        this.ariaValueMin = '0';
        this.ariaValueMax = '100';
        this._value = 0;
        contextualService.baseClass = ProgressBarComponent.BASE_CLASS;
    }
    Object.defineProperty(ProgressBarComponent.prototype, "height", {
        set: function (val) {
            this.hostStyle = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProgressBarComponent.prototype, "value", {
        get: function () { return this._value; },
        set: function (val) {
            this.ariaValueNow = String(val);
            this._value = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProgressBarComponent.prototype, "valueString", {
        get: function () {
            if (this.value < 0 || this.value > 100) {
                throw new Error('ProgressBarComponent: value should be a number between 0 and 100!');
            }
            return this.value + "%";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProgressBarComponent.prototype, "style", {
        get: function () {
            return { width: this.valueString };
        },
        enumerable: true,
        configurable: true
    });
    return ProgressBarComponent;
}());
ProgressBarComponent.BASE_CLASS = 'progress';
ProgressBarComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-progress-bar',
                template: "\n    <div class=\"progress-bar\"\n         [class.progress-bar-striped]=\"striped\"\n         [class.progress-bar-animated]=\"active\"\n         role=\"progressbar\"\n         [attr.aria-valuenow]=\"value\"\n         aria-valuemin=\"0\"\n         aria-valuemax=\"100\"\n         [ngStyle]=\"style\"\n         [arBsBgContext]=\"contextualService.bsContext\"\n    >\n      <ng-container *ngIf=\"showValue\">\n        {{ valueString }}\n      </ng-container>\n    </div>\n  ",
                providers: [ContextualService]
            },] },
];
ProgressBarComponent.ctorParameters = function () { return [
    { type: ContextualService, },
]; };
ProgressBarComponent.propDecorators = {
    "showValue": [{ type: core.Input },],
    "striped": [{ type: core.Input },],
    "active": [{ type: core.Input },],
    "height": [{ type: core.Input },],
    "value": [{ type: core.Input },],
    "baseClass": [{ type: core.HostBinding, args: ['class.progress',] },],
    "role": [{ type: core.HostBinding, args: ['attr.role',] },],
    "ariaValueNow": [{ type: core.HostBinding, args: ['attr.aria-valuenow',] },],
    "ariaValueMin": [{ type: core.HostBinding, args: ['attr.aria-valuemin',] },],
    "ariaValueMax": [{ type: core.HostBinding, args: ['attr.aria-valuemax',] },],
    "hostStyle": [{ type: core.HostBinding, args: ['style.height.px',] },],
};
var SpinnerComponent = /** @class */ (function () {
    function SpinnerComponent() {
    }
    Object.defineProperty(SpinnerComponent.prototype, "visible", {
        get: function () { return this.state; },
        set: function (state$$1) { this.state = state$$1; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SpinnerComponent.prototype, "invisible", {
        set: function (state$$1) {
            this.state = !state$$1;
        },
        enumerable: true,
        configurable: true
    });
    SpinnerComponent.prototype.show = function () {
        this.state = true;
    };
    SpinnerComponent.prototype.hide = function () {
        this.state = false;
    };
    return SpinnerComponent;
}());
SpinnerComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-spinner',
                template: "\n    <div class=\"spinner\" *ngIf=\"state\"></div>\n    <div [class.layout]=\"state\">\n      <ng-template *ngTemplateOutlet=\"templateRef\"></ng-template>\n      <ng-content></ng-content>\n    </div>\n  ",
                styles: ["@-webkit-keyframes spinner{to{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}@keyframes spinner{to{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}:host{position:relative;display:block;min-height:80px}.spinner{z-index:1;width:0;height:0;position:absolute;top:50%;left:50%}.spinner:before{content:'';-webkit-box-sizing:border-box;box-sizing:border-box;position:absolute;top:50%;left:50%;width:50px;height:50px;margin-top:-25px;margin-left:-25px;border-radius:50%;border:1px solid #f6f;border-top-color:#0e0;border-right-color:#0dd;border-bottom-color:#f90;-webkit-animation:.6s linear infinite spinner;animation:.6s linear infinite spinner}.layout{-webkit-filter:grayscale(100%);filter:grayscale(100%)}"]
            },] },
];
SpinnerComponent.propDecorators = {
    "visible": [{ type: core.Input },],
    "invisible": [{ type: core.Input },],
};
var SpinnerDirective = /** @class */ (function () {
    function SpinnerDirective(factoryResolver, templateRef, viewContainer) {
        this.templateRef = templateRef;
        this.viewContainer = viewContainer;
        var factory = factoryResolver.resolveComponentFactory(SpinnerComponent);
        this.component = factory.create(this.viewContainer.parentInjector);
        this.component.instance.templateRef = this.templateRef;
        this.viewContainer.insert(this.component.hostView);
    }
    Object.defineProperty(SpinnerDirective.prototype, "arSpinner", {
        set: function (condition) {
            this.component.instance.state = condition;
        },
        enumerable: true,
        configurable: true
    });
    return SpinnerDirective;
}());
SpinnerDirective.decorators = [
    { type: core.Directive, args: [{
                selector: '[arSpinner]'
            },] },
];
SpinnerDirective.ctorParameters = function () { return [
    { type: core.ComponentFactoryResolver, },
    { type: core.TemplateRef, },
    { type: core.ViewContainerRef, },
]; };
SpinnerDirective.propDecorators = {
    "arSpinner": [{ type: core.Input },],
};
var SwitchComponent = /** @class */ (function () {
    function SwitchComponent() {
        this.toggle = new core.EventEmitter();
    }
    SwitchComponent.prototype.clickHandler = function () {
        if (this.disabled) {
            return;
        }
        this.toggle.emit();
    };
    return SwitchComponent;
}());
SwitchComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-switch',
                template: "\n      <div class=\"onoffswitch\" [class.disabled]=\"disabled\" (click)=\"clickHandler()\">\n          <span class=\"onoffswitch-label\" [class.checked]=\"active\">\n              <span class=\"onoffswitch-inner\"></span>\n              <span class=\"onoffswitch-switch\"></span>\n          </span>\n      </div>\n  ",
                styles: [".onoffswitch{display:inline-block;position:relative;width:56px;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none}.onoffswitch-label{display:block;overflow:hidden;cursor:pointer;border:1px solid #fff;border-radius:10px;text-align:left}.onoffswitch-label.checked .onoffswitch-inner{margin-left:0}.onoffswitch-label.checked .onoffswitch-switch{right:0;background-color:#6bc95f}.onoffswitch-inner{display:block;width:200%;margin-left:-100%;-webkit-transition:margin .3s ease-in 0s;transition:margin .3s ease-in 0s}.onoffswitch-inner:after,.onoffswitch-inner:before{display:block;float:left;width:50%;height:19px;padding:0;line-height:19px;font-size:10px;font-family:Trebuchet,Arial,sans-serif;font-weight:700;-webkit-box-sizing:border-box;box-sizing:border-box}.onoffswitch-inner:before{content:\"ON\";padding-left:10px;background-color:#154975;color:#fff}.onoffswitch-inner:after{content:\"OFF\";padding-right:10px;background-color:#154975;color:#fff;text-align:right}.onoffswitch-switch{display:block;width:14px;margin:4px;background:#db3941;position:absolute;top:0;bottom:0;right:35px;border:1px solid #fff;border-radius:8px;-webkit-transition:all .3s ease-in 0s;transition:all .3s ease-in 0s}.onoffswitch.disabled .onoffswitch-switch{background-color:#cfcfcf}.onoffswitch.disabled .onoffswitch-inner:after,.onoffswitch.disabled .onoffswitch-inner:before{background-color:#cfcfcf;color:#fff}"]
            },] },
];
SwitchComponent.propDecorators = {
    "active": [{ type: core.Input },],
    "disabled": [{ type: core.Input },],
    "toggle": [{ type: core.Output },],
};
var FormService = /** @class */ (function () {
    function FormService() {
        this.id = 0;
    }
    FormService.prototype.generateControlId = function () {
        return this.generateId(this.formId + "_" + FormService.CONTROL_PREFIX);
    };
    FormService.prototype.generateSubControlId = function (control_id, value) {
        return control_id + "_" + value;
    };
    FormService.prototype.generateId = function (prefix) {
        this.id++;
        return prefix + "_" + this.id;
    };
    return FormService;
}());
FormService.FORM_ID_PREFIX = 'Form';
FormService.CONTROL_PREFIX = 'Input';
FormService.decorators = [
    { type: core.Injectable },
];
var FormComponent = /** @class */ (function () {
    function FormComponent(formService, logService) {
        this.formService = formService;
        this.logService = logService;
        this.isInline = false;
        this.autocomplete = 'off';
    }
    Object.defineProperty(FormComponent.prototype, "inline", {
        set: function (val) {
            this.isInline = Boolean(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FormComponent.prototype, "bgContext", {
        set: function (val) {
            this.formService.bsBgContext = val;
        },
        enumerable: true,
        configurable: true
    });
    FormComponent.prototype.ngOnInit = function () {
        if (!this.id) {
            this.logService.warn('Form id param required!');
        }
        this.formService.formId = this.id;
    };
    return FormComponent;
}());
FormComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-form, [arForm]',
                template: "\n    <ng-content></ng-content>\n  ",
                styles: ["[arForm].form-inline .form-group,ar-form.form-inline .form-group{margin-right:1rem!important}[arForm].form-inline .form-group:last-child,ar-form.form-inline .form-group:last-child{margin-right:0!important}"],
                providers: [FormService],
                encapsulation: core.ViewEncapsulation.None
            },] },
];
FormComponent.ctorParameters = function () { return [
    { type: FormService, },
    { type: LogService, },
]; };
FormComponent.propDecorators = {
    "inline": [{ type: core.Input },],
    "id": [{ type: core.Input },],
    "bgContext": [{ type: core.Input },],
    "isInline": [{ type: core.HostBinding, args: ['class.form-inline',] },],
    "autocomplete": [{ type: core.HostBinding, args: ['attr.autocomplete',] },],
};
var RowComponent = /** @class */ (function () {
    function RowComponent(formComponent) {
        this.formComponent = formComponent;
        this.baseClass = true;
        this.formRow = false;
    }
    RowComponent.prototype.ngOnInit = function () {
        this.formRow = Boolean(this.formComponent);
        this.baseClass = !this.formRow;
    };
    return RowComponent;
}());
RowComponent.ROW_CLASS = 'row';
RowComponent.FORM_ROW_CLASS = 'form-row';
RowComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-row, [arRow]',
                template: '<ng-content></ng-content>'
            },] },
];
RowComponent.ctorParameters = function () { return [
    { type: FormComponent, decorators: [{ type: core.Optional },] },
]; };
RowComponent.propDecorators = {
    "baseClass": [{ type: core.HostBinding, args: ['class.row',] },],
    "formRow": [{ type: core.HostBinding, args: ['class.form-row',] },],
};
var ColComponent = /** @class */ (function () {
    function ColComponent() {
        this.className = true;
    }
    return ColComponent;
}());
ColComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'ar-col',
                template: '<ng-content></ng-content>',
                styles: ["\n    :host { display: block; }\n  "]
            },] },
];
ColComponent.propDecorators = {
    "className": [{ type: core.HostBinding, args: ['class.col',] },],
};
var ScrollableDirective = /** @class */ (function () {
    function ScrollableDirective(renderer2, elementRef) {
        this.renderer2 = renderer2;
        this.elementRef = elementRef;
    }
    Object.defineProperty(ScrollableDirective.prototype, "arScrollable", {
        set: function (height) {
            this.currentHeight = height || 'auto';
            if (this.elementRef.nativeElement) {
                this.renderer2.setStyle(this.elementRef.nativeElement, 'height', this.currentHeight);
            }
        },
        enumerable: true,
        configurable: true
    });
    ScrollableDirective.prototype.ngOnInit = function () {
        this.renderer2.addClass(this.elementRef.nativeElement, ScrollableDirective.CLASS_NAME);
        this.renderer2.setStyle(this.elementRef.nativeElement, 'height', this.currentHeight);
        this.renderer2.setStyle(this.elementRef.nativeElement, 'display', 'block');
    };
    return ScrollableDirective;
}());
ScrollableDirective.CLASS_NAME = 'ar-ui-scrollable-block';
ScrollableDirective.decorators = [
    { type: core.Directive, args: [{
                selector: '[arScrollable]'
            },] },
];
ScrollableDirective.ctorParameters = function () { return [
    { type: core.Renderer2, },
    { type: core.ElementRef, },
]; };
ScrollableDirective.propDecorators = {
    "arScrollable": [{ type: core.Input },],
};
var UiModule = /** @class */ (function () {
    function UiModule() {
    }
    return UiModule;
}());
UiModule.decorators = [
    { type: core.NgModule, args: [{
                imports: [
                    common.CommonModule,
                    forms.ReactiveFormsModule,
                    router.RouterModule,
                    CoreModule
                ],
                declarations: [
                    AlertComponent,
                    AlertAreaComponent,
                    AlertHeadingComponent,
                    HelpBlockComponent,
                    LayoutComponent,
                    LoremIpsumComponent,
                    SimpleLayoutComponent,
                    ButtonComponent,
                    ButtonGroupComponent,
                    BsGroupComponent,
                    NavBarComponent,
                    NavBarNavComponent,
                    NavBarNavItemComponent,
                    NavBarNavDropdownItemComponent,
                    BsSizeDirective,
                    BsContextDirective,
                    BsBgContextDirective,
                    BsTextContextDirective,
                    TextAlignDirective,
                    WidthDirective,
                    BorderDirective,
                    DeprecatedDirective,
                    SpinnerDirective,
                    ScrollableDirective,
                    DropdownComponent,
                    DropdownMenuComponent,
                    DropdownMenuItemComponent,
                    DropdownDividerComponent,
                    IconComponent,
                    FigureComponent,
                    InnerLinkComponent,
                    OuterLinkComponent,
                    BlockComponent,
                    TabComponent,
                    TabLayoutComponent,
                    TabLinkComponent,
                    PageHeaderComponent,
                    PagingListSizeComponent,
                    PaginationComponent,
                    PagingTextComponent,
                    PagingControlComponent,
                    MailLinkComponent,
                    ModalLayoutComponent,
                    ModalComponent,
                    ModalHeaderComponent,
                    PanelComponent,
                    CardComponent,
                    CardGroupComponent,
                    ProgressBarComponent,
                    SpinnerComponent,
                    SwitchComponent,
                    RowComponent,
                    ColComponent
                ],
                exports: [
                    AlertComponent,
                    AlertHeadingComponent,
                    HelpBlockComponent,
                    LayoutComponent,
                    LoremIpsumComponent,
                    SimpleLayoutComponent,
                    ButtonComponent,
                    ButtonGroupComponent,
                    BsGroupComponent,
                    NavBarComponent,
                    NavBarNavComponent,
                    NavBarNavItemComponent,
                    NavBarNavDropdownItemComponent,
                    BsSizeDirective,
                    BsContextDirective,
                    BsBgContextDirective,
                    BsTextContextDirective,
                    TextAlignDirective,
                    WidthDirective,
                    BorderDirective,
                    DeprecatedDirective,
                    SpinnerDirective,
                    ScrollableDirective,
                    DropdownComponent,
                    DropdownMenuComponent,
                    DropdownMenuItemComponent,
                    DropdownDividerComponent,
                    IconComponent,
                    FigureComponent,
                    InnerLinkComponent,
                    OuterLinkComponent,
                    BlockComponent,
                    TabComponent,
                    TabLayoutComponent,
                    TabLinkComponent,
                    PageHeaderComponent,
                    PagingListSizeComponent,
                    PaginationComponent,
                    PagingControlComponent,
                    MailLinkComponent,
                    ModalComponent,
                    ModalHeaderComponent,
                    PanelComponent,
                    CardComponent,
                    CardGroupComponent,
                    ProgressBarComponent,
                    SpinnerComponent,
                    SwitchComponent,
                    RowComponent,
                    ColComponent
                ],
                providers: [
                    IconService
                ],
                entryComponents: [
                    SpinnerComponent
                ]
            },] },
];

exports.CoreModule = CoreModule;
exports.UiModule = UiModule;
exports.ɵi = DictionaryDirective;
exports.ɵf = InfinityScrollDirective;
exports.ɵg = ScrollContainerDirective;
exports.ɵl = dictionaryServiceFactory;
exports.ɵk = messageBusServiceFactory;
exports.ɵc = DictionaryPipe;
exports.ɵa = TrimPipe;
exports.ɵb = TruncatePipe;
exports.ɵm = AlertService;
exports.ɵd = DictionaryService;
exports.ɵe = LogService;
exports.ɵj = MessageBusService;
exports.ɵn = QueryOptionsBuilderService;
exports.ɵh = ScrollCollectionService;
exports.ɵcx = FormComponent;
exports.ɵcy = FormService;
exports.ɵo = AlertComponent;
exports.ɵu = AlertAreaComponent;
exports.ɵw = AlertHeadingComponent;
exports.ɵcb = BlockComponent;
exports.ɵs = BsGroupComponent;
exports.ɵbc = ButtonComponent;
exports.ɵbd = ButtonGroupComponent;
exports.ɵbz = CardComponent;
exports.ɵcs = CardGroupComponent;
exports.ɵcz = ColComponent;
exports.ɵbs = DropdownComponent;
exports.ɵbv = DropdownDividerComponent;
exports.ɵbt = DropdownMenuComponent;
exports.ɵbu = DropdownMenuItemComponent;
exports.ɵbx = FigureComponent;
exports.ɵx = HelpBlockComponent;
exports.ɵbw = IconComponent;
exports.ɵby = InnerLinkComponent;
exports.ɵy = LayoutComponent;
exports.ɵba = LoremIpsumComponent;
exports.ɵck = MailLinkComponent;
exports.ɵco = ModalComponent;
exports.ɵcq = ModalHeaderComponent;
exports.ɵcl = ModalLayoutComponent;
exports.ɵbe = NavBarComponent;
exports.ɵbg = NavBarNavComponent;
exports.ɵbi = NavBarNavDropdownItemComponent;
exports.ɵbh = NavBarNavItemComponent;
exports.ɵca = OuterLinkComponent;
exports.ɵcf = PageHeaderComponent;
exports.ɵch = PaginationComponent;
exports.ɵcj = PagingControlComponent;
exports.ɵcg = PagingListSizeComponent;
exports.ɵci = PagingTextComponent;
exports.ɵcr = PanelComponent;
exports.ɵct = ProgressBarComponent;
exports.ɵcw = RowComponent;
exports.ɵcu = SpinnerComponent;
exports.ɵcv = SwitchComponent;
exports.ɵcc = TabComponent;
exports.ɵcd = TabLayoutComponent;
exports.ɵce = TabLinkComponent;
exports.ɵbo = BorderDirective;
exports.ɵbj = BsBgContextDirective;
exports.ɵq = BsContextDirective;
exports.ɵt = BsSizeDirective;
exports.ɵbl = BsTextContextDirective;
exports.ɵr = ClassNameDirective;
exports.ɵbp = DeprecatedDirective;
exports.ɵbr = ScrollableDirective;
exports.ɵbq = SpinnerDirective;
exports.ɵbm = TextAlignDirective;
exports.ɵbn = WidthDirective;
exports.ɵcn = modalServiceFactory;
exports.ɵbb = SimpleLayoutComponent;
exports.ɵcp = ModalDialogConfigModel;
exports.ɵv = AlertsAreaService;
exports.ɵp = ContextualService;
exports.ɵz = IconService;
exports.ɵcm = ModalService;
exports.ɵbf = ToggleService;
exports.ɵbk = BsTextContextEnum;

Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=argon.umd.js.map
