export declare class FormService {
    static FORM_ID_PREFIX: string;
    static CONTROL_PREFIX: string;
    formId: string;
    bsBgContext: string;
    private id;
    generateControlId(): string;
    generateSubControlId(control_id: string, value: any): string;
    generateId(prefix: string): string;
}
